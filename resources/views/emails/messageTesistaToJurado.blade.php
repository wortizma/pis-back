<!DOCTYPE html>
<html>
<head>
<style>
table, th, td {
  border: 2px solid black;
  border-collapse: collapse;
}
th, td {
  padding: 5px;
  text-align: left;
}
</style>
</head>
<body>

<table style="width:100%">
  <tr>
    <td>Titulo de Tesis</td>
    <td colspan="3">{{$TituloTesis}}</td>
  </tr>
  <tr>
    <td>Tesista</td>
    <td colspan="3">{{$Tesista}}</td>
  </tr>
  <tr>
    <td>Jurado</td>
    <td colspan="3">{{$Jurado}}</td>
  </tr>
  <tr>
    <td>Fecha documento de Tesis</td>
    <td colspan="3">{{$Fecha}}</td>
  </tr>



</table>

@if (count($ObservacionesGenerales) != 0)
<br><br>
  <table style="width:100%">

    <tr>
      <td  colspan="2">Observaciones Generales</td>
      <td >Estado</td>
      <td >Consulta</td>

    </tr>

      @for ($i = 0; $i < count($ObservacionesGenerales); $i++)
        <tr>
        <td>{{$i+1}}</td>
        <td>{{$ObservacionesGenerales[$i]['Descripcion']}}</td>
        <td>{{$ObservacionesGenerales[$i]['Estado']}}</td>
        <td>{{$ObservacionesGenerales[$i]['Consulta']}}</td>
        </tr>
      @endfor



  </table>
@endif

@if (count($ObservacionesEspecificas) != 0)
<br><br>
  <table style="width:100%">
    <tr>
      <td  colspan="2">Observaciones Especificas</td>
      <td >Estado</td>
      <td >Consulta</td>

    </tr>


      @for ($i = 0; $i < count($ObservacionesEspecificas); $i++)
        <tr>
        <td>{{$i+1}}</td>
        <td>{{$ObservacionesEspecificas[$i]['Descripcion']}}</td>
        <td>{{$ObservacionesEspecificas[$i]['Estado']}}</td>
        <td>{{$ObservacionesEspecificas[$i]['Consulta']}}</td>
        </tr>
      @endfor

  </table>
@endif
  <p>

    </p>
    <p><strong>Portal de Tesis de la UNSA </strong></p>
    <p>Para acceder al sistema haga clic en: https://gestion-tesis-frontend.herokuapp.com/login </p>
    <img src="{{ $message->embed(public_path() . '/Banner/VG39Z3XwWzYYpHITRC1OWnMNoD5tgDzCVnDNGSIm.png') }}" />


</body>
</html>