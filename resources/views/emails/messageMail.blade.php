<!DOCTYPE html>
<html>
<head>
    <title>Mensaje del sistema</title>
</head>

<body>
    <h2>Tu usuario y contraseña para acceso al portal de tesis es: </h2>
    <p><strong>Usuario: </strong> {{$Usuario}} </p>
    <p><strong>Contraseña: </strong> {{$ClaveAutomatica}} </p>
    <p><strong>Anotela y evite perderla. </strong> </p>
    <p>

    </p>
    <p><strong>Portal de Tesis de la UNSA </strong></p>
    <p>Para acceder al sistema haga clic en: https://gestion-tesis-frontend.herokuapp.com/login </p>
    <img src="{{ $message->embed(public_path() . '/Banner/VG39Z3XwWzYYpHITRC1OWnMNoD5tgDzCVnDNGSIm.png') }}" />
</body>
</html>