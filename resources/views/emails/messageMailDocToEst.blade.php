<!DOCTYPE html>
<html>
<head>
    <title>Mensaje del sistema</title>
</head>

<body>
	<h2>Un jurado te ha hecho las siguientes observaciones: </h2>
	<table class="egt">
		<tr>
		  <td><strong>Título de Tesis : </strong></td>
		  <td>{{$Encabezados[0]}}</td>
		</tr>
		<tr>
		  <td><strong>Tesista         : </strong></td>
		  <td>{{$Encabezados[1]}}</td>
		</tr>
		  <td><strong>Jurado          : </strong></td>
		  <td>{{$Encabezados[2]}} </td>
		<tr>
		  	<td><strong>FECHA           : </strong></td>
		  	<td>{{$Encabezados[3]}} </td>
		</tr>
	</table>
	<h2>Observaciones Generales: </h2>

	<table class="egt" border="1" width="520">
		@foreach($Generales as $general)
            <tr>
                <td width="20">{{ $contadorGeneral++}}</td>
                <td width="520">{{ $general["Descripcion"]}}</td>
            </tr>
        @endforeach
	</table>

	<h2>Observaciones Específicas: </h2>

	<table class="egt" border="1" width="520">
		@foreach($Especificas as $especifico)
            <tr>
                <td width="20">{{ $contadorEspecifico++}}</td>
                <td width="520">{{ $especifico["Descripcion"]}}</td>
            </tr>
        @endforeach
	</table>
	<p>

    </p>
    <p><strong>Portal de Tesis de la UNSA </strong></p>
    <p>Para acceder al sistema haga clic en: https://gestion-tesis-frontend.herokuapp.com/login </p>
    <img src="{{ $message->embed(public_path() . '/Banner/VG39Z3XwWzYYpHITRC1OWnMNoD5tgDzCVnDNGSIm.png') }}" />
</body>
</html>