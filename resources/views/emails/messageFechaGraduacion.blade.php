<!DOCTYPE html>
<html>
<head>
    <title>Mensaje del sistema</title>
</head>

<body>
    <h2>Buen dia {{$NombreTesista}} </h2>
    <p><strong> El sistema de Posgrado le comunica que el día de su graduación será el dia {{$FechaGraduacion}} .</strong></p>
    <p><strong> Atte. Escuela de Posgrado.</strong></p>

    <p>

    </p>
    <p><strong>Portal de Tesis de la UNSA </strong></p>
    <p>Para acceder al sistema haga clic en: https://gestion-tesis-frontend.herokuapp.com/login </p>
    <img src="{{ $message->embed(public_path() . '/Banner/VG39Z3XwWzYYpHITRC1OWnMNoD5tgDzCVnDNGSIm.png') }}" />
</body>
</html>