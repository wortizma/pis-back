<!DOCTYPE html>
<html>
<head>
    <title>Mensaje del sistema</title>
</head>

<body>
    <h2>La siguiente tesis ha culminado su estado actual, requiere una actualizacion: </h2>
    <p><strong>Escuela: </strong> {{$Tesis["Escuela"]}} </p>
    <p><strong>Id de la tesis: </strong> {{$Tesis["IdTesis"]}} </p>
    <p><strong>Titulo: </strong> {{$Tesis["TituloTesis"]}} </p>
    <p><strong>Estado: </strong> {{$Tesis["Estado"]}} </p>
    <p><strong>Tesista: </strong> {{$Tesis["Nombre"]}} </p>
    <p>

    </p>
    <p><strong>Portal de Tesis de la UNSA </strong></p>
    <p>Para acceder al sistema haga clic en: https://gestion-tesis-frontend.herokuapp.com/login </p>
    <img src="{{ $message->embed(public_path() . '/Banner/VG39Z3XwWzYYpHITRC1OWnMNoD5tgDzCVnDNGSIm.png') }}" />
</body>
</html>