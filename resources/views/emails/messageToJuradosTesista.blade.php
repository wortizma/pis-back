<!DOCTYPE html>
<html>
<head>
    <title>Mensaje del sistema</title>
</head>

<body>
    <h2>Se ha puesto en revision una tesis </h2>
    <p><strong> “La tesis de {{$NombreEstudiante}} entro en FASE DE REVISION.</strong></p>
    <p>Para acceder al sistema haga clic en: https://gestion-tesis-frontend.herokuapp.com/login </p>
    <img src="{{ $message->embed(public_path() . '/Banner/VG39Z3XwWzYYpHITRC1OWnMNoD5tgDzCVnDNGSIm.png') }}" />
</body>
</html>