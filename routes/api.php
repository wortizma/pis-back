<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/
/*
Route::post('/Facultad/subirExcel', 'FacultadController@excelFacultad');
Route::post('/Escuela/subirExcel', 'EscuelaController@excelEscuela');
*/

//pruebas reporte
Route::post('/subirbanner', 'TesisController@subirBanner');
//pruebas estado
Route::post('/cambiarEstado','TesisController@pruebaestado');
//Pruebas sin login
Route::post('/login', 'AuthController@login');
Route::post('/crearJurado', 'JuradoController@store');
//rutas correspondientes a actividades que no requieren de un login
Route::post('/login', 'AuthController@login');
Route::post('/register', 'AuthController@register');
Route::post('/olvidoClave', 'EstudianteController@forwardingPass');
Route::post('/reiniciarPassword','PasswordResetRequestController@sendPasswordResetEmail','as','resetearPass');
Route::post('/change-password', 'ChangePasswordController@passwordResetProcess');
Route::get('/TipoDocumento/listar', 'TipodocumentoController@index');





//ruta para listar Roles
Route::post('/listarRolesP', 'EstudianteController@prueba');

//ruta para listar grado por escuela
Route::get('/grado/listarPorEscuela/{id}', 'GradoController@listarGradoPorEscuela');
//rutas correspondientes a actividades de usuarios que solo requieren estar logueados
Route::group(['middleware' => 'auth.jwt'], function () {
    Route::post('/logout', 'AuthController@logout');
    Route::post('/data', 'AuthController@getAuthUser');
    Route::post('/permission', 'AuthController@getPermission');
    //estudiante
    Route::get('/Estudiante/obtenerNombreEstudiante/{id}', 'EstudianteController@mostrarUnsoloEstudiante');
    //estudianteescuela
    Route::get('/EstudianteEscuela/listar/{id}', 'EstudianteEscuelaController@index');
    //reporte
    Route::post('/generarReporte','ReporteController@GenerarReporte');
    //finalizar observaciones
    Route::post('/Jurado/FinalizarObservacion', 'JuradoController@FinObservaciones');
    //rol
    Route::get('/Rol/listar', 'RolController@index');
    Route::get('/Rol/ver/{id}', 'RolController@show');
    //tesis
    Route::get('/Tesis/listarTesisEstudianteEscuela/{id}', 'TesisController@index');
    Route::get('/Tesis/verTesis/{id}', 'TesisController@verTesis');
    //Facultad
    Route::get('/Facultad/ListarEscuelasFacultad/{id}', 'FacultadController@ListarEscuelaPorFacultad');

    //observaciones
    Route::get('Observacion/listar/{id}', 'ObservacionController@index');
    Route::post('Observacion/EnviarObservacion', 'ObservacionController@enviarObservaciones');
    //TipoObservacio
    Route::get('TipoObservacion/listar', 'TipoObservacionController@index');
    //actualizar observacion
    Route::get('/Observacion/corregido/{id}', 'ObservacionController@corregido');

    Route::get('/Observacion/aprobado/{id}', 'ObservacionController@aprobado');
    Route::post('/Observacion/FinObservacion', 'ObservacionController@notificarOperador');

    //agregarconsulta a observacion
    Route::post('/Observacion/agregarConsultar/{id}', 'ObservacionController@agregarConsulta');

    //enviar correcciones
    Route::post('/Observacion/enviarCorrecciones', 'ObservacionController@enviarCorrecciones');


    /*Route::get('Operador/getAllOperador','OperadorController@index')->name('getAllOperador');
	Route::post('Operador/createOperador','OperadorController@store')->name('createOperador');
	Route::get('Operador/getOperador/{id}','OperadorController@show')->name('getOperador');
	Route::put('Operador/updateOperador/{id}','OperadorController@update')->name('updateOperador');
	Route::delete('Operador/delete/{id}','OperadorController@destroy')->name('deleteOperador');*/

    //Plantillas
    Route::get('/Plantilla/getPlantillaDocente', 'PlantillaController@getPlantillaDocente');
    Route::get('/Plantilla/getPlantillaEstudiante', 'PlantillaController@getPlantillaEstudiante');
    Route::get('/Plantilla/getPlantillaTipoJurado', 'PlantillaController@getPlantillaTipoJurado');
    Route::get('/Plantilla/getPlantillaDictamen', 'PlantillaController@getPlantillaDictamen');
    Route::get('/Plantilla/getPlantillaTipoDocumento', 'PlantillaController@getPlantillaTipoDocumento');
    Route::get('/Plantilla/getPlantillaFacultad', 'PlantillaController@getPlantillaFacultad');


    Route::get('/Plantilla/PlantillaEstadoExcel', 'DataExportController@exportExcelEstado');
    Route::get('/Plantilla/PlantillaTipoTesisExcel', 'DataExportController@exportExcelTipoTesis');
    Route::get('/Plantilla/PlantillaGradoExcel', 'DataExportController@exportExcelGrado');
    Route::get('/Plantilla/PlantillaEscuelaExcel', 'DataExportController@exportExcelEscuela');



    //HistoricoArchivo
    Route::get('/historicoArchivo', 'HistoricoArchivoController@store');

    //Plantilla TipoDocumento y Escuelas
    Route::get('/PlantillaEstudianteExcel', 'DataExportController@exportExcelEstudiante');
    Route::get('/PlantillaDocenteExcel', 'DataExportController@exportExcelDocente');




});
//rutas para el controlador estudiante
Route::get('Estudiante/listar', ['middleware' => 'auth.permission:bandejadeentrada.listar', 'uses' => 'EstudianteController@index', 'as' => 'EstudianteListar']);
Route::get('Estudiante/ver/{id}', ['middleware' => 'auth.permission:bandejadeentrada.ver', 'uses' => 'EstudianteController@show', 'as' => 'EstudianteVer']);
Route::delete('Estudiante/eliminar/{id}', ['middleware' => 'auth.permission:bandejadeentrada.eliminar', 'uses' => 'EstudianteController@destroy', 'as' => 'createOperador']);
Route::get('Estudiante/buscar', ['middleware' => 'auth.permission:bandejadeentrada.buscar', 'uses' => 'EstudianteController@getStudentPerName', 'as' => 'EstudianteBuscar']);
Route::put('Estudiante/editar/{id}', ['middleware' => 'auth.permission:bandejadeentrada.editar', 'uses' => 'EstudianteController@update', 'as' => 'EstudianteEditar']);
Route::post('Estudiante/crearEstudiante', ['middleware' => 'auth.permission:bandejadeentrada.crear', 'uses' => 'EstudianteController@store', 'as' => 'EstudianteCrear']);

Route::post('/Estudiante/subirExcelEstudiantes', ['middleware' => 'auth.permission:bandejadeentrada.crear', 'uses' => 'EstudianteController@excelEstudiantes', 'as' => 'EstudianteCrearArchivo']);

Route::post('/Estudiante/subirExcelDocentes', ['middleware' => 'auth.permission:bandejadeentrada.crear', 'uses' => 'EstudianteController@excelDocentes', 'as' => 'EstudianteCrearArchivo']);

Route::get('/Estudiante/listarDocentes', ['middleware' => 'auth.permission:mantenimientodetesis.crear', 'uses' => 'EstudianteController@listarDocentes', 'as' => 'EstudianteListarDocentes']);


//Rutas para  el controlador EstudianteEscuela
Route::post('EstudianteEscuela/crear', ['middleware' => 'auth.permission:mantenimientodeprogramas.crear', 'uses' => 'EstudianteEscuelaController@store', 'as' => 'EstudianteEscuelaCrear']);
Route::delete('EstudianteEscuela/eliminar/{id}', ['middleware' => 'auth.permission:mantenimientodeprogramas.eliminar', 'uses' => 'EstudianteEscuelaController@destroy', 'as' => 'EstudianteEscuelaEliminar']);
Route::put('EstudianteEscuela/editar/{id}', ['middleware' => 'auth.permission:mantenimientodeprogramas.editar', 'uses' => 'EstudianteEscuelaController@update', 'as' => 'EstudianteEscuelaEditar']);
Route::get('EstudianteEscuela/ver/{id}', ['middleware' => 'auth.permission:mantenimientodeprogramas.ver', 'uses' => 'EstudianteEscuelaController@show', 'as' => 'EstudianteEscuelaVer']);
//Rutas para  el controlador Roles
Route::post('Rol/crear', ['middleware' => 'auth.permission:mantenimientoderoles.crear', 'uses' => 'RolController@store', 'as' => 'RolCrear']);
Route::delete('Rol/eliminar/{id}', ['middleware' => 'auth.permission:mantenimientoderoles.eliminar', 'uses' => 'RolController@destroy', 'as' => 'RolEliminar']);

//Rutas para  el controlador Rol Proceso
Route::get('Proceso/verRolProceso/{id}', ['middleware' => 'auth.permission:mantenimientoderolesProceso.ver', 'uses' => 'ProcesoController@show', 'as' => 'RolProcesoVer']);

//Rutas para  el controlador Proceso
Route::get('Proceso/listarProceso', ['middleware' => 'auth.permission:gestordeprocesos.listar', 'uses' => 'ProcesoController@verProcesos', 'as' => 'ProcesoVer']);
Route::post('Proceso/crearProceso', ['middleware' => 'auth.permission:gestordeprocesos.crear', 'uses' => 'ProcesoController@store', 'as' => 'ProcesoCrear']);

//Rutas para  el controlador Actividad
Route::get('Proceso/verProcesoActividad/{idRolProceso}', ['middleware' => 'auth.permission:mantenimientoderolesProcesoActividad.ver', 'uses' => 'RolActividadController@show', 'as' => 'ProcesoActividadVer']);
Route::put('Proceso/editarProcesoActividad/{idRolProceso}', ['middleware' => 'auth.permission:mantenimientoderolesProcesoActividad.editar', 'uses' => 'RolActividadController@update', 'as' => 'ProcesoActividadEditar']);
Route::get('Actividad/listarActividad/{id}', ['middleware' => 'auth.permission:gestordeprocesosactividad.listar', 'uses' => 'ActividadController@index', 'as' => 'listarActividad']);
Route::post('Actividad/crearActividad', ['middleware' => 'auth.permission:gestordeprocesosactividad.crear', 'uses' => 'ActividadController@store', 'as' => 'crearActividad']);

//Rutas para  el controlador Tesis
Route::post('Tesis/crear', ['middleware' => 'auth.permission:mantenimientodetesis.crear', 'uses' => 'TesisController@store', 'as' => 'TesisCrear']);
Route::delete('Tesis/eliminar/{id}', ['middleware' => 'auth.permission:mantenimientodetesis.eliminar', 'uses' => 'TesisController@destroy', 'as' => 'TesisEliminar']);

Route::get('Tesis/obtenerarchivoTesis/{id}', 'TesisController@obtenerArchivoTesis');
Route::get('Tesis/obtenerarchivoResolucionAsesor/{id}', 'TesisController@obtenerArchivoResolucionAsesor');
Route::get('Tesis/obtenerarchivoResolucionJurado/{id}', 'TesisController@obtenerArchivoResolucionJurado');
Route::get('Tesis/obtenerarchivoDictamen/{id}', 'TesisController@obtenerArchivoDictamen');

Route::post('Tesis/modificarTesis/{id}','TesisController@update');
Route::post('Tesis/ActualizarArchivo/{id}','TesisController@updateFile');


//Rutas para  el controlador TipoTesis
Route::get('TipoTesis/listar', ['middleware' => 'auth.permission:gestordetipodetesis.listar', 'uses' => 'TipoTesisController@index', 'as' => 'TipoTesisListar']);
Route::get('TipoTesis/ver/{id}', ['middleware' => 'auth.permission:gestordetipodetesis.ver', 'uses' => 'TipoTesisController@show', 'as' => 'TipoTesisVer']);
Route::delete('TipoTesis/eliminar/{id}', ['middleware' => 'auth.permission:gestordetipodetesis.eliminar', 'uses' => 'TipoTesisController@destroy', 'as' => 'TipoTesisEliminar']);
Route::get('TipoTesis/buscar', ['middleware' => 'auth.permission:gestordetipodetesis.buscar', 'uses' => 'TipoTesisController@getTipoTesisPerName', 'as' => 'TipoTesisBuscar']);
Route::put('TipoTesis/editar/{id}', ['middleware' => 'auth.permission:gestordetipodetesis.editar', 'uses' => 'TipoTesisController@update', 'as' => 'TipoTesisEditar']);
Route::post('TipoTesis/crear', ['middleware' => 'auth.permission:gestordetipodetesis.crear', 'uses' => 'TipoTesisController@store', 'as' => 'TipoTesisCrear']);
Route::post('/TipoTesis/subirExcel', ['uses' => 'TipoTesisController@subirExcelTipoTesis', 'as' => 'TipoTesisSubirExcel']);



//Rutas para  el controlador Dictamen

Route::get('Dictamen/listar', ['middleware' => 'auth.permission:mantenimientodedictamenes.listar', 'uses' => 'DictamenController@index', 'as' => 'DictamenListar']);
Route::get('Dictamen/ver/{id}', ['middleware' => 'auth.permission:mantenimientodedictamenes.ver', 'uses' => 'DictamenController@show', 'as' => 'DictamenVer']);
Route::delete('Dictamen/eliminar/{id}', ['middleware' => 'auth.permission:mantenimientodedictamenes.eliminar', 'uses' => 'DictamenController@destroy', 'as' => 'DictamenEliminar']);
Route::get('Dictamen/buscar', ['middleware' => 'auth.permission:mantenimientodedictamenes.buscar', 'uses' => 'DictamenController@getDictamenPerName', 'as' => 'DictamenBuscar']);
Route::put('Dictamen/editar/{id}', ['middleware' => 'auth.permission:mantenimientodedictamenes.editar', 'uses' => 'DictamenController@update', 'as' => 'DictamenEditar']);
Route::post('Dictamen/crear', ['middleware' => 'auth.permission:mantenimientodedictamenes.crear', 'uses' => 'DictamenController@store', 'as' => 'DictamenCrear']);
Route::post('Dictamen/subirExcelDictamen', ['middleware' => 'auth.permission:mantenimientodedictamenes.crear', 'uses' => 'DictamenController@excelDitamen', 'as' => 'DictamenCargarArchivo']);

//Rutas para  el controlador Jurado
Route::get('Jurado/listar', ['middleware' => 'auth.permission:mantenimientodetesis.crear', 'uses' => 'JuradoController@index', 'as' => 'JuradoListar']);


//Rutas para  el controlador Observacion
Route::post('Observacion/crear', ['middleware' => 'auth.permission:mantenimientodeobservacion.crear', 'uses' => 'ObservacionController@store', 'as' => 'ObservacionCrear']);
Route::put('Observacion/editar/{id}', ['middleware' => 'auth.permission:mantenimientodeobservacion.editar', 'uses' => 'ObservacionController@update', 'as' => 'ObservacionEditar']);
Route::delete('Observacion/eliminar/{id}', ['middleware' => 'auth.permission:mantenimientodeobservacion.eliminar', 'uses' => 'ObservacionController@destroy', 'as' => 'ObservacionEliminar']);
Route::get('Observacion/ver/{id}', ['middleware' => 'auth.permission:mantenimientodeobservacion.ver', 'uses' => 'ObservacionController@show', 'as' => 'ObservacionVer']);


//Rutas para  el controlador Facultad
Route::get('Facultad/listar', ['middleware' => 'auth.permission:gestordefacultades.listar', 'uses' => 'FacultadController@index', 'as' => 'FacultadListar']);
Route::get('Facultad/ver/{id}', ['middleware' => 'auth.permission:gestordefacultades.ver', 'uses' => 'FacultadController@show', 'as' => 'FacultadVer']);
Route::delete('Facultad/eliminar/{id}', ['middleware' => 'auth.permission:gestordefacultades.eliminar', 'uses' => 'FacultadController@destroy', 'as' => 'FacultadEliminar']);
Route::get('Facultad/buscar', ['middleware' => 'auth.permission:gestordefacultades.buscar', 'uses' => 'FacultadController@getFacultadPerName', 'as' => 'FacultadBuscar']);
Route::put('Facultad/editar/{id}', ['middleware' => 'auth.permission:gestordefacultades.editar', 'uses' => 'FacultadController@update', 'as' => 'FacultadEditar']);
Route::post('Facultad/crear', ['middleware' => 'auth.permission:gestordefacultades.crear', 'uses' => 'FacultadController@store', 'as' => 'FacultadCrear']);
Route::post('/Facultad/subirExcel', ['middleware' => 'auth.permission:gestordefacultades.crear', 'uses' => 'FacultadController@excelFacultad', 'as' => 'FacultadCrearArchivo']);

//Rutas para  el controlador Escuela
Route::get('Escuela/listar', ['middleware' => 'auth.permission:gestordeescuelas.listar', 'uses' => 'EscuelaController@index', 'as' => 'EscuelaListar']);
Route::get('Escuela/ver/{id}', ['middleware' => 'auth.permission:gestordeescuelas.ver', 'uses' => 'EscuelaController@show', 'as' => 'EscuelaVer']);
Route::delete('Escuela/eliminar/{id}', ['middleware' => 'auth.permission:gestordeescuelas.eliminar', 'uses' => 'EscuelaController@destroy', 'as' => 'EscuelaEliminar']);
Route::get('Escuela/buscar', ['middleware' => 'auth.permission:gestordeescuelas.buscar', 'uses' => 'EscuelaController@getEscuelaPerName', 'as' => 'EscuelaBuscar']);
Route::put('Escuela/editar/{id}', ['middleware' => 'auth.permission:gestordeescuelas.editar', 'uses' => 'EscuelaController@update', 'as' => 'EscuelaEditar']);
Route::post('Escuela/crear', ['middleware' => 'auth.permission:gestordeescuelas.crear', 'uses' => 'EscuelaController@store', 'as' => 'EscuelaCrear']);
Route::post('/Escuela/subirExcel', ['middleware' => 'auth.permission:gestordeescuelas.crear', 'uses' => 'EscuelaController@ExcelEscuela', 'as' => 'EscuelaCrearArchivo']);

//Rutas para  el controlador Estado
Route::get('Estado/listar', ['middleware' => 'auth.permission:gestordeestadodetesis.listar', 'uses' => 'EstadoController@index', 'as' => 'EstadoListar']);
Route::get('Estado/ver/{id}', ['middleware' => 'auth.permission:gestordeestadodetesis.ver', 'uses' => 'EstadoController@show', 'as' => 'EstadoVer']);
Route::delete('Estado/eliminar/{id}', ['middleware' => 'auth.permission:gestordeestadodetesis.eliminar', 'uses' => 'EstadoController@destroy', 'as' => 'EstadoEliminar']);
Route::get('Estado/buscar', ['middleware' => 'auth.permission:gestordeestadodetesis.buscar', 'uses' => 'EstadoController@getEstadoPerName', 'as' => 'EstadoBuscar']);
Route::put('Estado/editar/{id}', ['middleware' => 'auth.permission:gestordeestadodetesis.editar', 'uses' => 'EstadoController@update', 'as' => 'EstadoEditar']);
Route::post('Estado/crear', ['middleware' => 'auth.permission:gestordeestadodetesis.crear', 'uses' => 'EstadoController@store', 'as' => 'EstadoCrear']);
Route::post('/Estado/subirExcel', ['uses' => 'EstadoController@subirExcelEstado', 'as' => 'EstadoSubirExcel']);

// Rutas para  el controlador tipo de documento
Route::post('TipoDocumento/crear', ['middleware' => 'auth.permission:gestordetipodedocumento.crear', 'uses' => 'TipodocumentoController@store', 'as' => 'TipoDocumentoCrear']);
Route::delete('TipoDocumento/eliminar/{id}', ['middleware' => 'auth.permission:gestordetipodedocumento.eliminar', 'uses' => 'TipodocumentoController@destroy', 'as' => 'TipoDocumentoEliminar']);
Route::get('TipoDocumento/ver/{id}', ['middleware' => 'auth.permission:gestordetipodedocumento.ver', 'uses' => 'TipodocumentoController@show', 'as' => 'TipoDocumentoVer']);
Route::put('TipoDocumento/editar/{id}', ['middleware' => 'auth.permission:gestordetipodedocumento.editar', 'uses' => 'TipodocumentoController@update', 'as' => 'TipoDocumentoEditar']);
Route::post('TipoDocumento/subirExcelTipoDocumento', ['middleware' => 'auth.permission:gestordetipodedocumento.crear', 'uses' => 'TipodocumentoController@excelTipoDocumento', 'as' => 'TipoDocumentoCargarArchivo']);

// Rutas para  el controlador tipo de jurado
Route::post('TipoJurado/crear', ['middleware' => 'auth.permission:gestordetipodejurado.crear', 'uses' => 'TipoJuradoController@store', 'as' => 'TipoDocumentoCrear']);
Route::delete('TipoJurado/eliminar/{id}', ['middleware' => 'auth.permission:gestordetipodejurado.eliminar', 'uses' => 'TipoJuradoController@destroy', 'as' => 'TipoDocumentoEliminar']);
Route::get('TipoJurado/ver/{id}', ['middleware' => 'auth.permission:gestordetipodejurado.ver', 'uses' => 'TipoJuradoController@show', 'as' => 'TipoDocumentoVer']);
Route::get('TipoJurado/listar', ['middleware' => 'auth.permission:gestordetipodejurado.listar', 'uses' => 'TipoJuradoController@index', 'as' => 'TipoDocumentoVer']);
Route::post('TipoJurado/subirExcelTipoJurado', ['middleware' => 'auth.permission:gestordetipodejurado.crear', 'uses' => 'TipoJuradoController@excelTipoJurado', 'as' => 'TipoJuradoCargarArchivo']);
Route::post('TipoJurado/crear', ['middleware' => 'auth.permission:gestordetipodejurado.crear', 'uses' => 'TipoJuradoController@store', 'as' => 'TipoJuradoCrear']);
Route::delete('TipoJurado/eliminar/{id}', ['middleware' => 'auth.permission:gestordetipodejurado.eliminar', 'uses' => 'TipoJuradoController@destroy', 'as' => 'TipoJuradoEliminar']);
Route::get('TipoJurado/ver/{id}', ['middleware' => 'auth.permission:gestordetipodejurado.ver', 'uses' => 'TipoJuradoController@show', 'as' => 'TipoJuradoVer']);
Route::get('TipoJurado/listar', ['middleware' => 'auth.permission:gestordetipodejurado.listar', 'uses' => 'TipoJuradoController@index', 'as' => 'TipoJuradoListar']);
Route::put('TipoJurado/editar/{id}', ['uses' => 'TipoJuradoController@update', 'as' => 'TipoJuradoEditar']);


//rutas para el controlador de grado
Route::get('Grado/listar', ['middleware' => 'auth.permission:gestordegrado.listar', 'uses' => 'GradoController@index', 'as' => 'GradoListar']);
Route::post('Grado/crear', ['middleware' => 'auth.permission:gestordegrado.crear', 'uses' => 'GradoController@store', 'as' => 'GradoCrear']);
Route::delete('Grado/eliminar/{id}', ['middleware' => 'auth.permission:gestordegrado.eliminar', 'uses' => 'GradoController@destroy', 'as' => 'GradoEliminar']);
Route::put('Grado/editar/{id}', ['middleware' => 'auth.permission:gestordegrado.editar', 'uses' => 'GradoController@update', 'as' => 'GradoEditar']);
Route::get('Grado/ver/{id}', ['middleware' => 'auth.permission:gestordegrado.ver', 'uses' => 'GradoController@show', 'as' => 'GradoVer']);
Route::post('Grado/subirExcel', ['middleware' => 'auth.permission:gestordegrado.crear', 'uses' => 'GradoController@excelGrado', 'as' => 'GradoExcel']);


