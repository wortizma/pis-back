<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ExampleTest extends TestCase
{

    public function signIn($data=['DNI'=>'11223344', 'password'=>'123456789'])
    {
        $response = $this->post('api/login', $data);
        $content = json_decode($response->getContent());

        $this->assertObjectHasAttribute('token', $content, 'message');
        $this->token = $content->token;

        return $this->token;
    }
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
       //$this->signIn();
       $response = $this->get('api/TipoDocumento/listar');
       $response->assertStatus(200);
    }

    public function testCrearFacltad(){
        $token = $this->signIn();
        //$headers = ['token' => "Bearer $token"];
        $payload = [
            'Facultad' => 'P',
            'token' => $token,
        ];
        $response = $this->post('api/Facultad/crear', $payload);
        $response->assertStatus(302);
        //$response->assertJson(['Mensaje' => 'Se ha registrado la facultad de manera correcta']);
    }

    public function testListarFacltad(){
        $token = $this->signIn();
        $response = $this->get('api/Facultad/ver/1?token='.$token);
        $response->assertStatus(200);
    }
    public function testEditarFacltad(){
        $token = $this->signIn();
        $payload = [
            'Facultad' => 'Esto se edito',
            'token' => $token,
        ];
        $response = $this->put('api/Facultad/editar/58', $payload);
        $response->assertStatus(200);
        $response->assertJson(['Mensaje' => 'Actualizacion exitosa.']);
    }

}
