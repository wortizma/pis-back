<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dictamen extends Model
{
    // Datos correspondiente a la tabla Dictamen
    protected $table = "Dictamen";
    protected $primaryKey = "IdDictamen";
    protected $fillable = array('Dictamen');
    public $timestamps = false;
}
