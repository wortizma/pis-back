<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Observacion extends Model
{
	// Datos correspondiente a la tabla Observacion
    protected $table = "Observacion";
    protected $primaryKey = "IdObservacion";
    protected $fillable = array('IdTesis','IdJurado','IdTipoObservacion','Descripcion','Fecha','Correccion','Consulta');
    public $timestamps = false;
}
