<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Escuela extends Model
{
    // Datos correspondiente a la tabla Escuela
     protected $table = "Escuela";
    protected $primaryKey = "idEscuela";
    protected $fillable = array('IdFacultad','Escuela','Directorio');
    public $timestamps = false;
}
