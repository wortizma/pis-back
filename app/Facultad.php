<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Facultad extends Model
{
    // Datos correspondiente a la tabla Facultad
    protected $table = "Facultad";
    protected $primaryKey = "IdFacultad";
    protected $fillable = array('Facultad');
    public $timestamps = false;
}
