<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoTesis extends Model
{
    // Datos correspondiente a la tabla TipoTesis
    protected $table = "TipoTesis";
    protected $primaryKey = "IdTipoTesis";
    protected $fillable = array('TipoTesis');
    public $timestamps = false;
}
