<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\MessageReceived;
use App\Estudiante;
use App\User;
use App\Rol;
use App\EstudianteRol;
use App\RolActividad;
use App\EstudianteEscuela;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;
use App\EstudianteProceso;
use Excel;
use App\Imports\EstudianteImport;
use App\Http\Traits\CrearEstudianteEscuela;
class EstudianteController extends Controller
{
    use CrearEstudianteEscuela;

// funcion encargada de listar a todos los estudiantes en la base de datos
// Primero verifica si la sesion fue iniciada
//Obtiene el token del estudiante para obtener sus permisos
//realiza el filtrado de acuerdoa  sus permisos, si tiene operador solo filtrara a los operadores
//Si tiene operador docente filtrara a operadores y docentes
    public function index(){

        try {
            //Access token from the request
            $token = JWTAuth::parseToken();
            //Try authenticating user
            $user = $token->authenticate();
        } catch (TokenExpiredException $e) {
            //Thrown if token has expired
            return $this->unauthorized('Tu sesion ha expirado, por favor realiza el loguin nuevamente.');
        } catch (TokenInvalidException $e) {
            //Thrown if token invalid
            return $this->unauthorized('Tu credenciales son invalidad, por favor vuelve a loguearte.');
        }catch (JWTException $e) {
            //Thrown if token was not found in the request.
            return $this->unauthorized('Por favor, inicia sesion para continuar.');
        }

        $data = EstudianteProceso::distinct()->select('ProcesoActividad.Nombre','EstudianteProceso.Permiso')->join('ProcesoActividad','ProcesoActividad.IdProcesoActividad' ,'=','EstudianteProceso.IdProcesoActividad')->where([['EstudianteProceso.IdEstudiante',$user->id],['EstudianteProceso.Permiso','1']])->get();
        $Resultado = array();

        $obtenerEscuela = EstudianteEscuela::distinct()->select('IdEscuela')->where([['IdEstudiante',$user->id]])->get();
        $IdEstEsc = [];
        foreach ($obtenerEscuela as $EstEsc) {
             $IdEstEsc[] = $EstEsc->IdEscuela;
        }
        if(strcmp($user->Operador, "1") === 0){
            foreach ($data as $dat) {

                 if (strcmp($dat->Nombre, "bandejadeentrada.Operador") === 0) {
                    $Operador = User::select('Estudiante.id','Estudiante.Nombre','Estudiante.idDNITipo','Estudiante.DNI','Estudiante.Correo','Estudiante.Telefono','Estudiante.Direccion','Estudiante.password','Estudiante.Operador','Estudiante.Estudiante','Estudiante.Docente')->join('EstudianteEscuela','EstudianteEscuela.IdEstudiante','=','Estudiante.id')->whereIn('EstudianteEscuela.IdEscuela', $IdEstEsc)->where([['Operador',"1"]])->get()->toArray();
                    $Resultado = array_merge($Resultado, $Operador);

                 }
                 if (strcmp($dat->Nombre, "bandejadeentrada.Estudiante") === 0) {
                    $Estudiante = User::select('Estudiante.id','Estudiante.Nombre','Estudiante.idDNITipo','Estudiante.DNI','Estudiante.Correo','Estudiante.Telefono','Estudiante.Direccion','Estudiante.password','Estudiante.Operador','Estudiante.Estudiante','Estudiante.Docente')->join('EstudianteEscuela','EstudianteEscuela.IdEstudiante','=','Estudiante.id')->whereIn('EstudianteEscuela.IdEscuela', $IdEstEsc)->where([['Estudiante',"1"]])->get()->toArray();
                    $Resultado = array_merge($Resultado, $Estudiante);

                 }

                 if (strcmp($dat->Nombre, "bandejadeentrada.Docente") === 0) {
                    $Docente = User::select('Estudiante.id','Estudiante.Nombre','Estudiante.idDNITipo','Estudiante.DNI','Estudiante.Correo','Estudiante.Telefono','Estudiante.Direccion','Estudiante.password','Estudiante.Operador','Estudiante.Estudiante','Estudiante.Docente')->join('EstudianteEscuela','EstudianteEscuela.IdEstudiante','=','Estudiante.id')->whereIn('EstudianteEscuela.IdEscuela', $IdEstEsc)->where([['Docente',"1"]])->get()->toArray();
                    $Resultado = array_merge($Resultado, $Docente);

                 }

            }
        }
             //$Operador->union($Estudiante)->get();
            /*$User = User::select('id','Nombre','idDNITipo','DNI','Correo','password','Operador','Estudiante','Docente')->get();
                return response()->json($User,200);*/

        $MisDatos = User::select('Estudiante.id','Estudiante.Nombre','Estudiante.idDNITipo','Estudiante.DNI','Estudiante.Correo','Estudiante.Telefono','Estudiante.Direccion','Estudiante.password','Estudiante.Operador','Estudiante.Estudiante','Estudiante.Docente')->join('EstudianteEscuela','EstudianteEscuela.IdEstudiante','=','Estudiante.id')->where([['id',$user->id]])->get()->toArray();
        $Resultado = array_merge($Resultado, $MisDatos);

        if(EstudianteRol::where('IdRol', '1')->where("IdEstudiante","=",$user->id)->exists()){
            $Administrador = User::select('Estudiante.id','Estudiante.Nombre','Estudiante.idDNITipo','Estudiante.DNI','Estudiante.Correo','Estudiante.Telefono','Estudiante.Direccion','Estudiante.password','Estudiante.Operador','Estudiante.Estudiante','Estudiante.Docente')->get()->toArray();
                $Resultado = array_merge($Resultado, $Administrador);
        }
        if(strcmp($user->Docente, "1") === 0){
            $EstudiantesDocente = User::select('id','Nombre','idDNITipo','DNI','Correo','Telefono','Direccion','password','Operador','Estudiante','Docente')->join('EstudianteEscuela','EstudianteEscuela.IdEstudiante','=','Estudiante.id')->join('Tesis','Tesis.IdEstudianteEscuela','=','EstudianteEscuela.IdEstudianteEscuela')->join('Jurado','Jurado.IdTesis','=','Tesis.IdTesis')->where([['Jurado.IdEstudiante',$user->id]])->get()->toArray();
        $Resultado = array_merge($Resultado, $EstudiantesDocente);
        }

        $arraySinDuplicados = [];
        foreach($Resultado as $indice => $elemento) {
            if (!in_array($elemento, $arraySinDuplicados)) {
                $arraySinDuplicados[] = $elemento;
            }
        }


            return response()->json($arraySinDuplicados);

    }

    //Este metodo es usado para un estudiante y sus roles asosciales, se requiere de IdEstudiante
    public function show($id){
        $User = User::select('id','Nombre','idDNITipo','DNI','Correo','Telefono','Direccion','password','Operador','Estudiante','Docente')->find($id);
        $Roles = EstudianteRol::distinct()->select('Rol.IdRol','Rol.NombreRol')->join('Rol','EstudianteRol.IdRol' ,'=','Rol.IdRol')->where([['EstudianteRol.IdEstudiante',$id]])->get()->toArray();
        $RolesRespuesta = [];

        foreach ($Roles as $Rol) {
            $RolesRespuesta [] = $Rol['IdRol'];
        }

        return  response()->json(['id' => $User->id, 'Nombre' => $User->Nombre, 'idDNITipo' =>$User->idDNITipo, 'DNI' =>$User->DNI, 'Correo' =>$User->Correo, 'Telefono' =>$User->Telefono, 'Direccion' =>$User->Direccion, 'password' =>$User->password, 'Operador' =>$User->Operador, 'Estudiante' =>$User->Estudiante, 'Docente' =>$User->Docente, 'ListaRoles' => $RolesRespuesta]);
        return response()->json($User,200);

    }

    public function excelEstudiantes(Request $request){
        $import = new EstudianteImport();
        $data = Excel::toArray($import, $request->file('files'));
        $end = $data[0];
        $contador = 2;
        $resultadoInsert = "";
        $actualizados = "";
        $contadorActualizados = 0;
        $contadorError = 0;
        $contadorDatosErroneos= 0;
        $contadorVacios=0;
        $vaciosNoValidos = "";
        $contadorVaciosNoValidos = 0;
        $DatosErroneos = "";
        $datosVacios="";
        $noRegistrados = "";
        $MensajeFinal = "";
        foreach ($end as $Estudiante) {
            if($Estudiante['nombre'] !== '' &&  $Estudiante['nombre'] !== NULL && $Estudiante['idtipodocumento'] !== '' &&  $Estudiante['idtipodocumento'] !== NULL && $Estudiante['dni'] !== '' &&  $Estudiante['dni'] !== NULL && $Estudiante['correo'] !== '' &&  $Estudiante['correo'] !== NULL && $Estudiante['telefono'] !== '' &&  $Estudiante['telefono'] !== NULL && $Estudiante['direccion'] !== '' &&  $Estudiante['direccion'] !== NULL && $Estudiante['idescuela'] !== '' &&  $Estudiante['idescuela'] !== NULL && $Estudiante['escuela'] !== '' &&  $Estudiante['escuela'] !== NULL){
                $ListaRoles = [];

                if(($this->consultarCrear("DNI", $Estudiante['dni'])) ){
                    //if(($this->consultarCrear("Correo",  $Estudiante['correo'])) ){
                        $ListaRoles[] = 4;
                        $estudianteActualizado = Estudiante::select("id")->where([['DNI',$Estudiante['dni']],['Correo', $Estudiante['correo']]])->get();
                        if(count($estudianteActualizado) > 0){

                        if(strlen ($Estudiante['telefono']) !== 9){

                            $DatosErroneos = $DatosErroneos."- El registro No. ".$contador." tiene un telefono invalido";
                            $contadorDatosErroneos++;
                            $contador++;
                            continue;
                        }
                        $request->replace(['Nombre' => $Estudiante['nombre'], 'idDNITipo' => $Estudiante['idtipodocumento'], 'DNI' => $Estudiante['dni'], 'Correo' => $Estudiante['correo'], 'Telefono' => $Estudiante['telefono'], 'Direccion' => $Estudiante['direccion'], 'Operador' => '0', 'Estudiante' => '1', 'Docente' => '0', 'ListaRoles' => $ListaRoles]);

                        if( $this->verificarDatosRequest($request) > 0){
                            $vaciosNoValidos = $vaciosNoValidos."- El registro No. ".$contador." tiene datos no validos.\n";
                            $contadorVaciosNoValidos++;
                            $contador++;
                            continue;
                        }
                        $this->update($request, $estudianteActualizado[0]->id);

                        $estescuelaResultado = EstudianteEscuela::where([['IdEscuela',$Estudiante['idescuela']],['IdEstudiante',$estudianteActualizado[0]->id]])->get();

                        //$estescuelaResultado = $this->StoreEstudianteEscuela($request);

                        if (Count($estescuelaResultado) >= 2){
                            $contadorActualizados++;
                            $actualizados = $actualizados."- El registro No. ".$contador." ya existe en el sistema, se han actualizado sus datos pero ya existe registro de la escuela.\n";
                            $contador++;
                            continue;
                        }
                        EstudianteEscuela::insert(array('IdEscuela' => $Estudiante['idescuela'],'IdEstudiante' => $estudianteActualizado[0]->id));

                        $contadorActualizados++;
                        $actualizados = $actualizados."El registro No. ".$contador." ya existe en el sistema, se han actualizado sus datos y agregado la nueva escuela y/o grado.\n";
                        $contador++;
                        continue;
                        //le mando a update;
                    }else{
                        $contadorError++;
                        $noRegistrados = $noRegistrados."- El registro No. ".$contador." No se ha registrado porque el estudiante ya esta registrado con otro Correo.\n";
                        $contador++;
                        continue;
                    }
                }
                if(($this->consultarCrear("Correo", $Estudiante['correo'])) ){

                        $ListaRoles[] = 4;
                        $estudianteActualizado = Estudiante::select("id")->where([['DNI',$Estudiante['dni']],['Correo', $Estudiante['correo']]])->get();
                    if(count($estudianteActualizado) > 0){


                        if(strlen ($Estudiante['telefono']) !== 9){

                            $DatosErroneos = $DatosErroneos."- El registro No. ".$contador." tiene un telefono invalido";
                            $contadorDatosErroneos++;
                            $contador++;
                            continue;
                        }
                         $request->replace(['Nombre' => $Estudiante['nombre'], 'idDNITipo' => Estudiante['idtipodocumento'], 'DNI' => $Estudiante['dni'], 'Correo' => $Estudiante['correo'], 'Telefono' => $Estudiante['telefono'], 'Direccion' => $Estudiante['direccion'], 'Operador' => '0', 'Estudiante' => '1', 'Docente' => '0', 'ListaRoles' => $ListaRoles]);

                         if( $this->verificarDatosRequest($request) > 0){
                            $vaciosNoValidos = $vaciosNoValidos."- El registro No. ".$contador." tiene datos no validos.\n";
                            $contadorVaciosNoValidos++;
                            $contador++;
                            continue;
                        }

                        $this->update($request, $estudianteActualizado[0]->id);
                        $estescuelaResultado = EstudianteEscuela::where([['IdEscuela',$Estudiante['idescuela']],['IdEstudiante',$estudianteActualizado[0]->id]])->get();



                        if (Count($estescuelaResultado) >= 2){
                            $contadorActualizados++;
                            $actualizados = $actualizados."- El registro No. ".$contador." ya existe en el sistema, se han actualizado sus datos pero ya existe registro de la escuela.\n";
                            $contador++;
                            continue;
                        }
                        EstudianteEscuela::insert(array('IdEscuela' => $Estudiante['idescuela'],'IdEstudiante' => $estudianteActualizado[0]->id));

                        $contadorActualizados++;
                        $actualizados = $actualizados."- El registro No. ".$contador." ya existe en el sistema, se han actualizado sus datos y agregado la nueva escuela.\n";
                        $contador++;
                        continue;
                        //le mando a update;
                    }else{
                        $contadorError++;
                        $noRegistrados = $noRegistrados."- El registro No. ".$contador." No se ha registrado porque el estudiante ya esta registrado con otro DNI.\n";
                        $contador++;
                        continue;
                    }

                }
                $ListaRoles[] = 4;

                $request->replace(['Nombre' => $Estudiante['nombre'], 'idDNITipo' => $Estudiante['idtipodocumento'], 'DNI' => $Estudiante['dni'], 'Correo' => $Estudiante['correo'], 'Telefono' => $Estudiante['telefono'], 'Direccion' => $Estudiante['direccion'], 'Operador' => '0', 'Estudiante' => '1', 'Docente' => '0', 'ListaRoles' => $ListaRoles]);

                if( $Estudiante['idtipodocumento'] == '1' && strlen ($Estudiante['dni']) !== 8){

                            $DatosErroneos = $DatosErroneos."- El registro No. ".$contador." tiene un DNI invalido. \n";
                            $contadorDatosErroneos++;
                            $contador++;
                            continue;
                        }
                if(strlen ($Estudiante['telefono']) !== 9){

                            $DatosErroneos = $DatosErroneos."- El registro No. ".$contador." tiene un telefono invalido. \n";
                            $contadorDatosErroneos++;
                            $contador++;
                            continue;
                        }
                if( $this->verificarDatosRequest($request) > 0){
                            $vaciosNoValidos = $vaciosNoValidos."- El registro No. ".$contador." tiene datos no validos.\n";
                            $contadorVaciosNoValidos++;
                            $contador++;
                            continue;
                        }
                $resultado = $this->store($request);
                EstudianteEscuela::insert(array('IdEscuela' => $Estudiante['idescuela'],'IdEstudiante' => $resultado->id));
                $contador++;
            }

            }

            $MensajeFinal = $MensajeFinal."¡Se han leido ".($contador-2)." filas de forma correcta! \n";

            if($contadorActualizados == 0 && $contadorError ==0 && $contadorDatosErroneos == 0 && $contadorVacios == 0){
                $MensajeFinal = $MensajeFinal."Todos los registros se han realizado de forma correcta. \n";
            }
            if($contadorVaciosNoValidos > 0){
                $MensajeFinal = $MensajeFinal."No se registraron los siguientes registros: \n".$vaciosNoValidos;
                //return response()->json(['Eliminacion' => true]);
            }
            if($contadorActualizados > 0){
                $MensajeFinal = $MensajeFinal."Se han actualizado los siguientes registros: \n".$actualizados;
                //return response()->json(['Eliminacion' => true]);
            }

            if($contadorError > 0){
                $MensajeFinal = $MensajeFinal."Los siguientes registros no se han realizado: \n".$noRegistrados;
                //return response()->json(['Eliminacion' => true]);
            }

            if($contadorDatosErroneos > 0){
                $MensajeFinal = $MensajeFinal."Los siguientes registros no se han realizado: \n".$DatosErroneos;
                //return response()->json(['Eliminacion' => true]);
            }
            if($contadorVacios > 0){
                $MensajeFinal = $MensajeFinal."Los siguientes registros no se han realizado porque tienen datos vacios: \n".$datosVacios;
                //return response()->json(['Eliminacion' => true]);
            }
            return  response()->json(['Mensaje' => $MensajeFinal]);
            //return $MensajeFinal;
    }

    public function excelDocentes(Request $request){
        $import = new EstudianteImport();
        $data = Excel::toArray($import, $request->file('files'));
        $end = $data[0];
        $contador = 2;
        $resultadoInsert = "";
        $actualizados = "";
        $contadorActualizados = 0;
        $contadorError = 0;
        $contadorDatosErroneos= 0;
        $contadorVacios=0;
        $vaciosNoValidos = "";
        $contadorVaciosNoValidos = 0;
        $DatosErroneos = "";
        $datosVacios="";
        $noRegistrados = "";
        $MensajeFinal = "";
        foreach ($end as $Estudiante) {
            if($Estudiante['nombre'] !== '' &&  $Estudiante['nombre'] !== NULL && $Estudiante['idtipodocumento'] !== '' &&  $Estudiante['idtipodocumento'] !== NULL && $Estudiante['dni'] !== '' &&  $Estudiante['dni'] !== NULL && $Estudiante['correo'] !== '' &&  $Estudiante['correo'] !== NULL && $Estudiante['telefono'] !== '' &&  $Estudiante['telefono'] !== NULL && $Estudiante['direccion'] !== '' &&  $Estudiante['direccion'] !== NULL){
                $ListaRoles = [];

                if(($this->consultarCrear("DNI", $Estudiante['dni'])) ){
                    //if(($this->consultarCrear("Correo",  $Estudiante['correo'])) ){
                        $ListaRoles[] = 3;
                        $estudianteActualizado = Estudiante::select("id")->where([['DNI',$Estudiante['dni']],['Correo', $Estudiante['correo']]])->get();
                        if( count($estudianteActualizado) > 0){

                        if(strlen ($Estudiante['telefono']) !== 9){

                            $DatosErroneos = $DatosErroneos."- El registro No. ".$contador." tiene un telefono invalido";
                            $contadorDatosErroneos++;
                            $contador++;
                            continue;
                        }
                        $request->replace(['Nombre' => $Estudiante['nombre'], 'idDNITipo' => $Estudiante['idtipodocumento'], 'DNI' => $Estudiante['dni'], 'Correo' => $Estudiante['correo'], 'Telefono' => $Estudiante['telefono'], 'Direccion' => $Estudiante['direccion'], 'Operador' => '0', 'Estudiante' => '0', 'Docente' => '1', 'ListaRoles' => $ListaRoles]);
                        if( $this->verificarDatosRequest($request) > 0){
                            $vaciosNoValidos = $vaciosNoValidos."- El registro No. ".$contador." tiene datos no validos.\n";
                            $contadorVaciosNoValidos++;
                            $contador++;
                            continue;
                        }
                        $this->update($request, $estudianteActualizado[0]->id);

                        $contadorActualizados++;
                        $actualizados = $actualizados."El registro No. ".$contador." ya existe en el sistema, se han actualizado sus datos y agregado la nueva escuela.\n";
                        $contador++;
                        continue;
                        //le mando a update;
                    }else{
                        $contadorError++;
                        $noRegistrados = $noRegistrados."- El registro No. ".$contador." No se ha registrado porque el estudiante ya esta registrado con otro Correo.\n";
                        $contador++;
                        continue;
                    }
                }
                if(($this->consultarCrear("Correo", $Estudiante['correo'])) ){

                        $ListaRoles[] = 3;
                        $estudianteActualizado = Estudiante::select("id")->where([['DNI',$Estudiante['dni']],['Correo', $Estudiante['correo']]])->get();
                        if(count($estudianteActualizado)>0){

                        if(strlen ($Estudiante['telefono']) !== 9){

                            $DatosErroneos = $DatosErroneos."- El registro No. ".$contador." tiene un telefono invalido";
                            $contadorDatosErroneos++;
                            $contador++;
                            continue;
                        }
                         $request->replace(['Nombre' => $Estudiante['nombre'], 'idDNITipo' => Estudiante['idtipodocumento'], 'DNI' => $Estudiante['dni'], 'Correo' => $Estudiante['correo'], 'Telefono' => $Estudiante['telefono'], 'Direccion' => $Estudiante['direccion'], 'Operador' => '0', 'Estudiante' => '0', 'Docente' => '1', 'ListaRoles' => $ListaRoles]);
                         if( $this->verificarDatosRequest($request) > 0){
                            $vaciosNoValidos = $vaciosNoValidos."- El registro No. ".$contador." tiene datos no validos.\n";
                            $contadorVaciosNoValidos++;
                            $contador++;
                            continue;
                        }
                        $this->update($request, $estudianteActualizado[0]->id);

                        $contadorActualizados++;
                        $actualizados = $actualizados."- El registro No. ".$contador." ya existe en el sistema, se han actualizado sus datos.\n";
                        $contador++;
                        continue;
                        //le mando a update;
                    }else{
                        $contadorError++;
                        $noRegistrados = $noRegistrados."- El registro No. ".$contador." No se ha registrado porque el estudiante ya esta registrado con otro DNI.\n";
                        $contador++;
                        continue;
                    }
                }
                $ListaRoles[] = 3;

                $request->replace(['Nombre' => $Estudiante['nombre'], 'idDNITipo' => $Estudiante['idtipodocumento'], 'DNI' => $Estudiante['dni'], 'Correo' => $Estudiante['correo'], 'Telefono' => $Estudiante['telefono'], 'Direccion' => $Estudiante['direccion'], 'Operador' => '0', 'Estudiante' => '0', 'Docente' => '1', 'ListaRoles' => $ListaRoles]);

                if( $Estudiante['idtipodocumento'] == '1' && strlen ($Estudiante['dni']) !== 8){

                            $DatosErroneos = $DatosErroneos."- El registro No. ".$contador." tiene un DNI invalido. \n";
                            $contadorDatosErroneos++;
                            $contador++;
                            continue;
                        }
                if(strlen ($Estudiante['telefono']) !== 9){

                            $DatosErroneos = $DatosErroneos."- El registro No. ".$contador." tiene un telefono invalido. \n";
                            $contadorDatosErroneos++;
                            $contador++;
                            continue;
                        }
                if( $this->verificarDatosRequest($request) > 0){
                            $vaciosNoValidos = $vaciosNoValidos."- El registro No. ".$contador." tiene datos no validos.\n";
                            $contadorVaciosNoValidos++;
                            $contador++;
                            continue;
                        }
                $resultado = $this->store($request);
                $contador++;
            }

            }

            $MensajeFinal = $MensajeFinal."¡Se han leído ".($contador-2)." filas de forma correcta! \n";
            if($contadorActualizados == 0 && $contadorError ==0 && $contadorDatosErroneos == 0 && $contadorVacios == 0){
                $MensajeFinal = $MensajeFinal."Todos los registros se han realizado de forma correcta. \n";
            }
            if($contadorVaciosNoValidos > 0){
                $MensajeFinal = $MensajeFinal."No se registraron los siguientes registros: \n".$vaciosNoValidos;
                //return response()->json(['Eliminacion' => true]);
            }

            if($contadorActualizados > 0){
                $MensajeFinal = $MensajeFinal."Se han actualizado los siguientes registros: \n".$actualizados;
                //return response()->json(['Eliminacion' => true]);
            }

            if($contadorError > 0){
                $MensajeFinal = $MensajeFinal."Los siguientes registros no se han realizado: \n".$noRegistrados;
                //return response()->json(['Eliminacion' => true]);
            }

            if($contadorDatosErroneos > 0){
                $MensajeFinal = $MensajeFinal."Los siguientes registros no se han realizado: \n".$DatosErroneos;
                //return response()->json(['Eliminacion' => true]);
            }
            if($contadorVacios > 0){
                $MensajeFinal = $MensajeFinal."Los siguientes registros no se han realizado porque tienen datos vacios: \n".$datosVacios;
                //return response()->json(['Eliminacion' => true]);
            }
            return  response()->json(['Mensaje' => $MensajeFinal]);
            //return $MensajeFinal;
    }
    public function verificarDatosRequest(Request $request){
        $verificar = $request->all();
        $contador = 0;

        if($request->Nombre == NULL || $request->Nombre == ''){
            $contador++;
        }
        if($request->idDNITipo == NULL || $request->idDNITipo == ''){
            $contador++;
        }
        if($request->DNI == NULL || $request->DNI == ''){
            $contador++;
        }
        if($request->Correo == NULL || $request->Correo == ''){
            $contador++;
        }
        if(!filter_var($request->Correo, FILTER_VALIDATE_EMAIL)){
            $contador++;
        }
        if($request->Direccion == NULL || $request->Direccion == '' || strlen($request->Direccion) < 10){
            $contador++;
        }
        if($request->Operador == NULL || $request->Operador == ''){
            $contador++;
        }
        if($request->Operador == NULL || $request->Operador == ''){
            $contador++;
        }
        if($request->Estudiante == NULL || $request->Estudiante == ''){
            $contador++;
        }
        if($request->Docente == NULL || $request->Docente == ''){
            $contador++;
        }
        return $contador;
    }

    //Este metodo es usado para  enviar correo electronico a un estudiante su usuario (DNI y contraseña)
    public function sendEmail($Estudiante,$ClaveAutomatica,$Correo){
        Mail::to($Correo)->send(new MessageReceived($Estudiante, $ClaveAutomatica));
    }

    public function consultarCrear($comparar, $dato){
       return User::where($comparar, $dato)->exists();;
    }

    //Este metodo es usado para guardar datos correspondiente a un estudiante en la base de datos
    public function store(Request $request)
    {

         request()->validate([
            'Nombre' => 'required|min:6|max:40',
            'DNI' => 'required|numeric',
            'Correo' => 'required|email:rfc,dns',
            'Telefono' => 'required|numeric|digits:9',
            'Direccion' => 'required|min:10|max:50',
            'Operador' => 'required',
            'Estudiante' => 'required',
            'Docente' => 'required'
        ]);

        if(($this->consultarCrear("DNI",$request->DNI)) ){
            return  response()->json([
            'Campo' => 'DNI',
            'Existe' => 'true'
            ], 200);
        }

        if(($this->consultarCrear("Correo", $request->Correo)) ){
            return  response()->json([
            'Campo' => 'Correo',
            'Existe' => 'true'
            ], 200);
        }

        $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $ClaveAutomatica = substr(str_shuffle($permitted_chars), 0, 10);
        $Hash = password_hash($ClaveAutomatica,PASSWORD_DEFAULT);

        $user = new User;
        $user->Nombre = $request->Nombre;
        $user->idDNITipo = $request->idDNITipo;
        $user->DNI = $request->DNI;
        $user->Correo = $request->Correo;
        $user->Telefono = $request->Telefono;
        $user->Direccion = $request->Direccion;
        $user->password = $Hash;
        $user->Operador = $request->Operador;
        $user->Estudiante = $request->Estudiante;
        $user->Docente = $request->Docente;
        $user->save();
        $this->sendEmail($request->DNI,$ClaveAutomatica,$request->Correo);

       // $ListaRoles = $request->ListaRoles;

        /*$ListaRoles = Rol::select('IdRol','NombreRol')->whereIn('IdRol', $request->ListaRoles)->get();
        foreach ($ListaRoles as $rol) {

            $UsuarioAutomatico = substr(str_shuffle($permitted_chars), 0, 8);
            $ClaveAutomatica = substr(str_shuffle($permitted_chars), 0, 8);
            $Jugador = Jugador::find($dat);
            $Jugador->update(['Notificado' => 1, 'Usuario' => $UsuarioAutomatico, 'password' => $ClaveAutomatica]);
            $this->sendEmail($Jugador,$UsuarioAutomatico,$ClaveAutomatica);
        }*/
        $returnList = [];
        if(!(is_array($request->ListaRoles))){
            array_push($returnList, $request->ListaRoles);
        }else{
            $returnList = $request->ListaRoles;
        }
        $shares = RolActividad::distinct()->select('IdProcesoActividad','Permiso')->join('RolProceso', 'RolProceso.IdRolProceso', '=', 'RolActividad.IdRolProceso')->join('Rol', 'RolProceso.IdRol', '=', 'Rol.IdRol')->whereIn('Rol.IdRol', $returnList)->where('Permiso','1')->get();
        foreach ($returnList as $InsertarRol)
        {
            $EstudianteRol = new EstudianteRol();
            $EstudianteRol->IdEstudiante = $user->id;
            $EstudianteRol->IdRol = $InsertarRol;
            $EstudianteRol->save();
        }
        foreach ($shares as $nuevo)
        {
            $EstudianteProceso = new EstudianteProceso();
            $EstudianteProceso->IdEstudiante = $user->id;
            $EstudianteProceso->IdProcesoActividad = $nuevo->IdProcesoActividad;
            $EstudianteProceso->Permiso = $nuevo->Permiso;
            $EstudianteProceso->save();
        }

        //$ListaRoles = Rol::select('IdRol','NombreRol')->whereIn('IdRol', $request->ListaRoles)->get();
        //return response()->json($shares, 200);

        return $user;
    }
    public function prueba(Request $request){

    }
//permite actualizas a un estudiante
//si el correo actualizado existe no dejara que el registro se actualice
//si el DNI ya fue registrado, no dejara que el registro de actualice
    public function update(Request $request, $id)
    {

         request()->validate([
            'Nombre' => 'required|min:6|max:40',
            'DNI' => 'required|numeric',
            'Correo' => 'required|email:rfc,dns',
            'Telefono' => 'required|numeric|digits:9',
            'Direccion' => 'required|min:10|max:50'
        ]);

        $User = User::find($id);
        if(($this->consultar("DNI",$request->DNI,$User->id)) ){
            return  response()->json([
            'Campo' => 'DNI',
            'Existe' => 'true'
            ], 200);
        }

        if(($this->consultar("Correo", $request->Correo,$User->id)) ){
            return  response()->json([
            'Campo' => 'Correo',
            'Existe' => 'true'
            ], 200);
        }


        $User->Nombre = $request->Nombre;
        $User->idDNITipo = $request->idDNITipo;
        $User->DNI = $request->DNI;
        $User->Correo = $request->Correo;
        $User->Telefono = $request->Telefono;
        $User->Direccion = $request->Direccion;
        $User->Operador = $request->Operador;
        $User->Estudiante = $request->Estudiante;
        $User->Docente = $request->Docente;
        $User->save();

        $returnList = [];
        if(!(is_array($request->ListaRoles))){
            array_push($returnList, $request->ListaRoles);
        }else{
            $returnList = $request->ListaRoles;
        }

        $shares = RolActividad::distinct()->select('IdProcesoActividad','Permiso')->join('RolProceso', 'RolProceso.IdRolProceso', '=', 'RolActividad.IdRolProceso')->join('Rol', 'RolProceso.IdRol', '=', 'Rol.IdRol')->whereIn('Rol.IdRol', $returnList)->where('Permiso','1')->get();

        EstudianteProceso::where('IdEstudiante', $User->id)->delete();
        EstudianteRol::where('IdEstudiante', $User->id)->delete();

        foreach ($returnList as $InsertarRol)
        {
            $EstudianteRol = new EstudianteRol();
            $EstudianteRol->IdEstudiante = $User->id;
            $EstudianteRol->IdRol = $InsertarRol;
            $EstudianteRol->save();
        }

        foreach ($shares as $nuevo)
        {
            $EstudianteProceso = new EstudianteProceso();
            $EstudianteProceso->IdEstudiante = $User->id;
            $EstudianteProceso->IdProcesoActividad = $nuevo->IdProcesoActividad;
            $EstudianteProceso->Permiso = $nuevo->Permiso;
            $EstudianteProceso->save();
        }


        return $this->show($id);
    }

//Permite verificar un registro, es utilizado apra actualizar, buscara todos los registros distintos
//al id enviado
    public function consultar($comparar, $dato, $id){
       return User::where($comparar, $dato)->where("id","!=",$id)->exists();;
    }

    public function getStudentPerName(Request $request){

        if ($request->input('Nombre')) {
            $campoBusqueda = 'Nombre';
            $datoEnviado = $request->input('Nombre');
        }else if ($request->input('DNI')) {
            $campoBusqueda = 'DNI';
            $datoEnviado = $request->input('DNI');
        }else if ($request->input('Correo')) {
            $campoBusqueda = 'Correo';
            $datoEnviado = $request->input('Correo');
        }else
            return response()->json(['message' => 'Debe Llena el Campo']);

        $User = User::where($campoBusqueda,'like','%'.$datoEnviado.'%')->select('id','Nombre','idDNITipo','DNI','Correo','Telefono','Direccion','password','Operador','Estudiante','Docente')->get();
                return response()->json($User,200);
    }

    public function forwardingPass(Request $request)
    {
        $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $ClaveAutomatica = substr(str_shuffle($permitted_chars), 0, 10);
        $Hash = password_hash($ClaveAutomatica,PASSWORD_DEFAULT);
        try {
        $Estudiante = Estudiante::where([['DNI',$request->DNI],['Correo',$request->Correo]])->update([
           'password' => $Hash
        ]);
        if($Estudiante==0){
            return response()->json(['Actualizacion de clave' => false]);
        }
        $this->sendEmail($request->DNI,$ClaveAutomatica,$request->Correo);
        } catch(\Illuminate\Database\QueryException $ex){
            return response()->json(['Actualizacion de clave' => false]);
         // Note any method of class PDOException can be called on $ex.
        }
        return response()->json(['Actualizacion de clave' => true]);
    }

//Elimina un estudiante, la eliminacion se hara en cascada si no se tiene un registro asociado en las tesis.
    public function destroy($id)
    {
        $User = User::find($id);
        $User->delete();

        return response()->json(['Eliminacion' => true]);
    }

//Este metodo es usado para  listar todos los estudiantes que son docentes en la base de datos
    public function listarDocentes(){
        $Docentes = Estudiante::select('id','Nombre')->where('Docente',1)->get();
        return response()->json($Docentes,200);
    }

//Este metodo es usado para mostrar un solo estudiante, se requiere IdEstudiantre
    public function mostrarUnsoloEstudiante($id)
    {

        $Estudiante = Estudiante::select('Nombre')->where('id','=',$id)->get();
        return  response()->json(['Nombre' => $Estudiante[0]->Nombre]);

    }

}
