<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Rol;
class RolController extends Controller
{

    //Este metodo es usado para listar todos los Roles de la base de datos
    public function index(){
    	$Rol = Rol::select('IdRol','NombreRol')->get();
        return response()->json($Rol, 200);
    //
    }

    //Este metodo es usado para ver un Rol, se requiere del idRol
    public function show($id){

        $Rol = Rol::select('IdRol','NombreRol')->find($id);
        return response()->json($Rol,200);

    //
    }

//Este metodo es usado para guardar datos correspondientes a un Rol en la base de datos
    public function store(Request $request){

        request()->validate([
            'NombreRol' => 'required|max:50',
        ]);

    	$Rol = new Rol;
        $Rol->NombreRol = $request->NombreRol;
    	$Rol->save();

    	return response()->json($Rol, 201);

    }
    //Este metodo es usado para  eliminar un Rol  de la base de datos, se requiere de IdRol
    public function destroy($id){
        $Rol = Rol::find($id);
        $Rol->delete();

        return response()->json(['Eliminacion' => true]);
    }


}
