<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Imports\ExcelImport;
use Excel;
use App\TipoTesis;
class TipoTesisController extends Controller
{
    //Este metodo es usado para listar todos los TipoTesis existentes en la base de datos
    public function index(){
    	$TipoTesis = TipoTesis::select('IdTipoTesis','TipoTesis')->get();
        return response()->json($TipoTesis, 200);
    }
    //Este metodo es usado para guardar datos correspondientes a un TipoTesis de la base de datos
    public function store(Request $request){
    	request()->validate([
            'TipoTesis' => 'required|min:0|max:45|regex:/^[a-zA-ZáéíóúüñÁÉÍÓÚÜÑ.,;:[:space:]]*$/',

          ],[
            'TipoTesis.required' => 'El campo tiene caracteres no validos.'
          ]);
         if($this->duplicidadRegistro($request->TipoTesis)){
            return response()->json(['Mensaje' => 'Los datos ingresados ya existen en el sistema.']);
         }
    	$TipoTesis = new TipoTesis;
        $TipoTesis->TipoTesis = $request->TipoTesis;
  		$TipoTesis->save();
  		//return response()->json($TipoTesis, 200);
        return response()->json(['Mensaje' => 'Se ha registrado el Tipo de Tesis de manera correcta.']);
    }

    //Este metodo es usado para eliminar un TipoTesis de la base de datos, se requiere de IdTipoTesis
	public function destroy($id){
    	$TipoTesis = TipoTesis::find($id);
        $TipoTesis->delete();

        //return response()->json(['Eliminacion' => true]);
        return response()->json(['Mensaje' => 'Se eliminó de manera correcta.']);
    }

//Este metodo es usado para actualizar datos correspondientes a TipoTesis, se requiere de IdTipoTesis
    public function update(Request $request, $id)
    {
         request()->validate([
            'TipoTesis' => 'required|min:0|max:45|regex:/^[a-zA-ZáéíóúüñÁÉÍÓÚÜÑ.,;:[:space:]]*$/',

          ],[
            'TipoTesis.required' => 'El campo tiene caracteres no validos.'
          ]);
         if($this->duplicidad($request->TipoTesis,$id)){
            return response()->json(['Mensaje' => 'Los datos ingresados ya existen en el sistema.']);
         }
    	$TipoTesis = TipoTesis::find($id);
        $TipoTesis->TipoTesis = $request->TipoTesis;
        $TipoTesis->save();
    	//return $this->show($id);
        return response()->json(['Mensaje' => 'Actualizacion existosa.']);
    }

//Este metodo es usado para mostrar datos correspondientes a un TipoTesis, se requyiere de IdTipoTesis
    public function show($id)
    {

        $TipoTesis = TipoTesis::select('IdTipoTesis','TipoTesis')->find($id);
    	return response()->json($TipoTesis,200);

    }

//Este metodo es usado para realizar una busqueda por filtrado de TipoTesis
     public function getTipoTesisPerName(Request $request){

        request()->validate([
            'TipoTesis' => 'required|max:100',
        ]);
        $datoEnviado=$request->TipoTesis;
        $campoBusqueda='TipoTesis';
        $TipoTesis = TipoTesis::where($campoBusqueda,'like','%'.$datoEnviado.'%')->select('IdTipoTesis','TipoTesis')->get();
                return response()->json($TipoTesis,200);
    }

    //Esta funcion se encarga de verificar la duplicidad para un registro nuevo
    public function duplicidadRegistro($dato){
        return TipoTesis::where('TipoTesis', $dato)->exists();;
    }

    //Este metodo es usado para verificar que el dato no exista en la BD en un registro nuevo
    public function duplicidad($TipoTesis,$id){
        return TipoTesis::where('TipoTesis', $TipoTesis)->where('IdTipoTesis','!=',$id)->exists();;
    }

    //Funcion que verifica que la cadena tenga datos validos
    public function verificarCadena($dato){
        if (preg_match("/^[a-zA-Z0-9áéíóúüñÁÉÍÓÚÜÑ \-_]{3,45}+$/", $dato)) {
            return true;
        } else {
            return false;
           }

    }

    //Este metodo es usado para  cargar un excel con los datos correspondiente a un estado
    public function subirExcelTipoTesis(Request $request){
        $import = new ExcelImport();
        $data = Excel::toArray($import, $request->file('file'));
        $end = $data[0];
        $NoGuardados = "";
        $DatosInvalidos = "";
        $MensajeFinal="";
        $contador = 2;
        $contadorNoGuardado = 0;
        foreach ($end as $TipoTesis) {
            if($TipoTesis['tipotesis'] !== '' &&  $TipoTesis['tipotesis'] !== NULL){
                if($this->duplicidadRegistro($TipoTesis['tipotesis'])){
                    $NoGuardados = $NoGuardados."El registro: ".$contador." ya existe en la base de datos.\n";
                    $contadorNoGuardado++;
                    $contador++;
                    continue;
                }else{
                    if(!$this->verificarCadena($TipoTesis['tipotesis'])){
                        $DatosInvalidos = $DatosInvalidos."El registro: ".$contador." tiene caracteres no válidos.\n";
                        $contador++;
                        continue;
                    }
                    $request->replace(['TipoTesis' => $TipoTesis['tipotesis']]);
                    $this->store($request);
                    $contador++;
                }
            }
        }
        if($contadorNoGuardado == 0){
                $MensajeFinal = $MensajeFinal."Todos los registros se han guardado con éxito!\n";
            }else{
                $MensajeFinal = $MensajeFinal."Los siguientes registros no se han guardado:\n".$NoGuardados.$DatosInvalidos;
            }
      return response()->json(['Mensaje' => $MensajeFinal]);
    }





}
