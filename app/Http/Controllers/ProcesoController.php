<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Proceso;
use App\RolProceso;

class ProcesoController extends Controller
{
    //
    //Este metodo es usado para  listar todos los procesos correspondiente a un rol, se requiere IdRol
    public function show($id){
		$data = RolProceso::select('RolProceso.IdRolProceso as IdProceso','Proceso.NombreProceso')->join('Proceso','Proceso.IdProceso' ,'=','RolProceso.IdProceso')->where('RolProceso.IdRol','=',$id)->get();
        return response()->json($data,200);

    }

//Este metodo es usado para guardar datos correspondientes de un proceso
    public function store(Request $request){

        request()->validate([
            'NombreProceso' => 'required|min:1|max:100',
        ]);

    	$Proceso = new Proceso;
        $Proceso->NombreProceso = $request->NombreProceso;
    	$Proceso->save();

    	return response()->json($Proceso, 201);

    }


//Este metodo es usado para  list5ar todos los procesos exigentes
    public function verProcesos(){
    	$Proceso = Proceso::select('IdProceso','NombreProceso')->get();
        return response()->json($Proceso, 200);
    }

}
