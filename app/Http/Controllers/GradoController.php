<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Grado;
use Excel;
use App\Imports\GradoImport;
class GradoController extends Controller
{
    //Funcion encargada de listar los grados existentes
    public function index(){
    	$grado = Grado::select("Grado.IdGrado","Grado.Grado","Escuela.Escuela")->join("Escuela","Grado.IdEscuela","=","Escuela.idEscuela")->get();
    	return response()->json($grado, 200);
    }
    //Funcion encargada de listar los grados por escuela.
    public function listarGradoPorEscuela($id){
        $lista = Grado::select('IdGrado','IdEscuela','Grado')->where('IdEscuela','=',$id)->get();
        return response()->json($lista,201);
    }
    //Funcion encargada de registrar los grados.
    public function store(Request $request){
        request()->validate([
            'IdEscuela' => 'required',
            'Grado' => 'required|min:5|max:30|regex:/^[a-zA-ZáéíóúüñÁÉÍÓÚÜÑ():;.,[:space:]]*$/'
        ],[
            'Grado.required' => 'El campo tiene caracteres no validos.'
          ]);
    	if($this->duplicidadRegistro($request->IdEscuela, $request->Grado)){
	    	return response()->json(['Mensaje' => "Los datos ingresados ya existen en el sistema."]);
	    }
	    $Grado = new Grado();
	    $Grado->IdEscuela = $request->IdEscuela;
	    $Grado->Grado = $request->Grado;
	    $Grado->save();
    	return response()->json(['Mensaje' => "Se ha registrado el Grado de manera correcta."]);
    }
    //Funcion encargada de mostrar los datos de un determinado grado
    public function show($id){
    	$Grado = Grado::select('Grado.IdGrado','Grado.IdEscuela','Escuela.Escuela','Grado.Grado')->join('Escuela','Escuela.idEscuela','=','Grado.IdEscuela')->where('Grado.IdGrado','=',$id)->get();
        return  response()->json(['IdGrado' => $Grado[0]->IdGrado, 'IdEscuela' => $Grado[0]->IdEscuela, 'Escuela' =>$Grado[0]->Escuela, 'Grado' =>$Grado[0]->Grado]);
    }
    //Funcion encargada de actualizar un grado.
    public function update(Request $request, $id){
        request()->validate([
            'IdEscuela' => 'required',
            'Grado' => 'required|min:5|max:30|regex:/^[a-zA-Z0-9áéíóúüñÁÉÍÓÚÜÑ:;.,[:space:]]*$/'
        ],[
            'Grado.required' => 'El campo tiene caracteres no validos.'
          ]);

        if($this->duplicidad($request->IdEscuela, $request->Grado, $id)){
            return response()->json(['Mensaje' => "Los datos ingresados ya existen en el sistema"]);
        }

    	$Grado = Grado::find($id);
    	$Grado->IdEscuela = $request->IdEscuela;
    	$Grado->Grado = $request->Grado;
    	$Grado->save();
        return response()->json(['Mensaje' => "Actualizacion exitosa."]);
    }
    //Funcion encargada de eliminar un grado.
    public function destroy($id){
    	$Grado = Grado::find($id);
        $Grado->delete();
        return response()->json(['Mensaje' => 'Se eliminó de manera correcta.']);
    }
    //Este metodo es usado para verificar que el dato no exista en la BD
    public function duplicidad($idEscuela, $Grado, $id){
        return Grado::where('IdEscuela', $idEscuela)->where("Grado","=", $Grado)->where('IdGrado','!=',$id)->exists();
    }
    //Este metodo es usado para verificar que el dato no exista en la BD para un registro nuevo
    public function duplicidadRegistro($idEscuela, $Grado){
        return Grado::where('IdEscuela', $idEscuela)->where("Grado","=", $Grado)->exists();
    }
    //Funcion que verifica que la cadena tenga datos validos
    public function verificarCadena($dato){
        if (preg_match("/^[a-zA-ZáéíóúüñÁÉÍÓÚÜÑ :;.,]{5,30}+$/", $dato)) {
            return true;
        } else {
            return false;
           }

    }
    //Funcion encargada de registrar un excel de grados.
    public function excelGrado(Request $request){
        $import = new GradoImport();
        $data = Excel::toArray($import, $request->file('file'));
        $end = $data[0];
        $NoGuardados = "";
        $DatosInvalidos = "";
        $MensajeFinal="";
        $contador = 2;
        $contadorNoGuardado = 0;
        foreach ($end as $Grado) {
        	if($Grado['idescuela'] !== '' &&  $Grado['idescuela'] !== NULL && $Grado['grado'] !== '' &&  $Grado['grado'] !== NULL){
        		if($this->duplicidadRegistro($Grado['idescuela'], $Grado['grado'])){
		            $NoGuardados = $NoGuardados."El registro: ".$contador." ya existe en la base de datos.\n";
		            $contadorNoGuardado++;
		            $contador++;
		            continue;
	        	}else{
                    if(!$this->verificarCadena($Grado['grado'])){
                        $DatosInvalidos = $DatosInvalidos."El registro: ".$contador." tiene caracteres no válidos.\n";
                        $contador++;
                        continue;
                    }
	        		$request->replace(['IdEscuela' => $Grado['idescuela'], 'Grado' => $Grado['grado']]);
		            $this->store($request);
		            $contador++;
	        	}
        	}
        }
    	if($contadorNoGuardado == 0){
        		$MensajeFinal = $MensajeFinal."Todos los registros se han guardado con exito\n";
        	}else{
        		$MensajeFinal = $MensajeFinal."Los siguientes registros no se han guardado:\n".$NoGuardados.$DatosInvalidos;
        	}
      return response()->json(['Mensaje' => $MensajeFinal]);
    }

}
