<?php

namespace App\Http\Controllers;
use App\TipoJurado;
use Excel;
use Illuminate\Http\Request;
use App\Imports\ExcelImport;
class TipoJuradoController extends Controller
{
    //Este metodo es usado para listar todos los jurados de la base de datos
    public function index(){
    	$TipoJurado = TipoJurado::select('IdTipoJurado','TipoJurado')->get();
        return response()->json($TipoJurado, 200);
    }
//Este metodo es usado para guardar datos correspondientes a un jurado en la base de datos
    public function store(Request $request){
    	request()->validate([
            'TipoJurado' => 'required|min:6|max:50|regex:/^[a-zA-ZáéíóúüñÁÉÍÓÚÜÑ.;,[:space:]]*$/',
          ],[
            'TipoJurado.required' => 'El campo tiene muchos o pocos caracteres.'
          ]);
        if(TipoJurado::where('TipoJurado', $request->TipoJurado)->exists()){
            return response()->json(['Mensaje' => 'El Tipo de Jurado '.$request->TipoJurado.' ya existe']);
         }
    	$TipoJurado = new TipoJurado;
        $TipoJurado->TipoJurado = $request->TipoJurado;
  		$TipoJurado->save();
  		return response()->json($TipoJurado, 200);
    }

//Este metodo es usado para eliminar un jurado de la base de datos, se requiere de IdJurado
	public function destroy($id){
    	$TipoJurado = TipoJurado::find($id);
        $TipoJurado->delete();

        return response()->json(['Eliminacion' => true]);
    }

//Este metodo es usado para mostrar datos correspondientes a un jurado, se requiere de IdJurado
    public function show($id)
    {

        $TipoJurado = TipoJurado::select('IdTipoJurado','TipoJurado')->find($id);
    	return response()->json($TipoJurado,200);

    }


    //Funcion encargada de verificar que no haya duplicidad en la base de datos
    public function duplicidad($dato){
        return TipoJurado::where('TipoJurado', $dato)->exists();
    }
    //Funcion que verifica que la cadena tenga datos validos
    public function verificarCadena($dato){
        if (preg_match("/^[a-zA-Z0-9 \-_]{6,50}+$/", $dato)) {
            return true;
        } else {
            return false;
           }

    }
    //Funcion encargada de subir datos desde un excel
    public function excelTipoJurado(Request $request){
        $import = new ExcelImport();
        $data = Excel::toArray($import, $request->file('file'));
        $end = $data[0];
        $NoGuardados = "";
        $DatosInvalidos = "";
        $MensajeFinal="";
        $contador = 2;
        $contadorNoGuardado = 0;
        foreach ($end as $Jurado) {
            if($Jurado['tipojurado'] !== '' &&  $Jurado['tipojurado'] !== NULL){
                if($this->duplicidad($Jurado['tipojurado'])){
                    $NoGuardados = $NoGuardados."El registro: ".$contador." ya existe en la base de datos.\n";
                    $contadorNoGuardado++;
                    $contador++;
                    continue;
                }else{
                    if(!$this->verificarCadena($Jurado['tipojurado'])){
                        $DatosInvalidos = $DatosInvalidos."El registro: ".$contador." tiene caracteres no válidos.\n";
                        $contador++;
                        continue;
                    }
                    $request->replace(['TipoJurado' => $Jurado['tipojurado']]);
                    $this->store($request);
                    $contador++;
                }
            }
        }
        if($contadorNoGuardado == 0){
                $MensajeFinal = $MensajeFinal."Todos los registros se han guardado con exito\n";
            }else{
                $MensajeFinal = $MensajeFinal."Los siguientes registros no se han guardado:\n".$NoGuardados.$DatosInvalidos;
            }
      return response()->json(['Mensaje' => $MensajeFinal]);
    }

    public function update(Request $request, $id)
    {
         request()->validate([
            'TipoJurado' => 'required|min:3|max:45|regex:/^[a-zA-Z0-9áéíóúüñÁÉÍÓÚÜÑ.;,[:space:]]*$/',
        ],[
            'TipoJurado.required' => 'El campo tiene muchos o pocos caracteres.'
          ]);
         if(TipoJurado::where('TipoJurado', $request->TipoJurado)->exists()){
            return response()->json(['Mensaje' => "El Tipo de Jurado '".$request->TipoJurado."' ya existe"]);
         }
        $TipoJurado = TipoJurado::find($id);
        $TipoJurado->TipoJurado = $request->TipoJurado;
        $TipoJurado->save();
        return $this->show($id);
    }


}
