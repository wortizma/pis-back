<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Imports\ExcelImport;
use App\Dictamen;
use Excel;
class DictamenController extends Controller
{
    //Este metodo es usado para  listar todos los datos de la tabla Dictamen de la base de datos
    public function index(){
    	$Dictamen = Dictamen::select('IdDictamen','Dictamen')->get();
        return response()->json($Dictamen, 200);
    }

    //Este metodo es usado para  guardar los datos correspondiente a un Dictamen en la base de datos
    public function store(Request $request){
    	request()->validate([
            'Dictamen' => 'required|min:6|max:40',
          ]);
         if(Dictamen::where('Dictamen', $request->Dictamen)->exists()){
            return response()->json(['Mensaje' => 'El Dictamen '.$request->Dictamen.' ya existe']);
         }
    	$Dictamen = new Dictamen;
        $Dictamen->Dictamen = $request->Dictamen;
  		$Dictamen->save();
  		return response()->json($Dictamen, 200);
    }

    //Este metodo es usado para  eliminar un Dictamen de la base de datos, requiere del idDictamen
	public function destroy($id){
    	$Dictamen = Dictamen::find($id);
        $Dictamen->delete();

        return response()->json(['Eliminacion' => true]);
    }

    //Este metodo es usado para actualizar un dictamen en la base de datos, requiere del idDictamen
    public function update(Request $request, $id)
    {
         request()->validate([
            'Dictamen' => 'required|min:6|max:40',
        ]);
         if(Dictamen::where('Dictamen', $request->Dictamen)->exists()){
            return response()->json(['Mensaje' => "El Dictamen '".$request->Dictamen."' ya existe"]);
         }
    	$Dictamen = Dictamen::find($id);
        $Dictamen->Dictamen = $request->Dictamen;
        $Dictamen->save();
    	return $this->show($id);
    }

    //Este metodo es usado para mostrar un dictamen, se requiere de IdDicamen
    public function show($id)
    {

        $Dictamen = Dictamen::select('IdDictamen','Dictamen')->find($id);
    	return response()->json($Dictamen,200);

    }

        //Este metodo es usado para  realizar una busqueda por filtrado de un Dictamen
       public function getDictamenPerName(Request $request){

        request()->validate([
            'Dictamen' => 'required|max:100',
        ]);
        $campoBusqueda='Dictamen';
        $datoEnviado=$request->Dictamen;
        $Dictamen = Dictamen::where($campoBusqueda,'like','%'.$datoEnviado.'%')->select('IdDictamen','Dictamen')->get();
                return response()->json($Dictamen,200);
    }

    //Funcion encargada de verificar que no haya duplicidad en la base de datos
    public function duplicidad($dato){
        return Dictamen::where('Dictamen', $dato)->exists();
    }
    //Funcion que verifica que la cadena tenga datos validos
    public function verificarCadena($dato){
        if (preg_match("/^[a-zA-Z0-9áéíóúüñÁÉÍÓÚÜÑ \-_]{6,40}+$/", $dato)) {
            return true;
        } else {
            return false;
           }

    }
    //Funcion encargada de subir datos desde un excel
    public function excelDitamen(Request $request){
        $import = new ExcelImport();
        $data = Excel::toArray($import, $request->file('file'));
        $end = $data[0];
        $NoGuardados = "";
        $DatosInvalidos = "";
        $MensajeFinal="";
        $contador = 2;
        $contadorNoGuardado = 0;
        foreach ($end as $Dictamen) {
            if($Dictamen['dictamen'] !== '' &&  $Dictamen['dictamen'] !== NULL){
                if($this->duplicidad($Dictamen['dictamen'])){
                    $NoGuardados = $NoGuardados."El registro: ".$contador." ya existe en la base de datos.\n";
                    $contadorNoGuardado++;
                    $contador++;
                    continue;
                }else{
                    if(!$this->verificarCadena($Dictamen['dictamen'])){
                        $DatosInvalidos = $DatosInvalidos."El registro: ".$contador." tiene caracteres no validos.\n";
                        $contador++;
                        continue;
                    }
                    $request->replace(['Dictamen' => $Dictamen['dictamen']]);
                    $this->store($request);
                    $contador++;
                }
            }
        }
        if($contadorNoGuardado == 0){
                $MensajeFinal = $MensajeFinal."Todos los registros se han guardado con exito\n";
            }else{
                $MensajeFinal = $MensajeFinal."Los siguientes registros no se han guardado:\n".$NoGuardados.$DatosInvalidos;
            }
      return response()->json(['Mensaje' => $MensajeFinal]);
    }

}
