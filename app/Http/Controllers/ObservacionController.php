<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Mail;
use App\Mail\MessageDocToStud;
use App\Mail\MessageToOperator;
use JWTAuth;
use DateTime;
use App\Observacion;
use App\EstudianteRol;
use App\Tesis;
use App\Jurado;
use App\HistoricoArchivo;
use App\Mail\SendMailTesistaToJurado;
class ObservacionController extends Controller
{

    //Este metodo es usado para listar observaciones, se requiere el idTesis
	public function index($id){

        try {
        //Access token from the request
        $token = JWTAuth::parseToken();
        //Try authenticating user
        $user = $token->authenticate();
    } catch (TokenExpiredException $e) {
        //Thrown if token has expired
        return $this->unauthorized('Tu sesion ha expirado, por favor realiza el loguin nuevamente.');
    } catch (TokenInvalidException $e) {
        //Thrown if token invalid
        return $this->unauthorized('Tu credenciales son invalidad, por favor vuelve a loguearte.');
    }catch (JWTException $e) {
        //Thrown if token was not found in the request.
        return $this->unauthorized('Por favor, inicia sesion para continuar.');
    }

    $Resultado = array();

         if (strcmp($user->Operador, "1") === 0) {
            $ObservacionOperador = Observacion::select('Estudiante.id','Estudiante.Nombre','Observacion.IdObservacion','Observacion.IdTesis','Observacion.IdJurado','Observacion.IdTipoObservacion','Observacion.Descripcion','Observacion.Fecha','Observacion.Correccion','Estudiante.Nombre','TipoObservacion.TipoObservacion','Observacion.Aprobado','Observacion.Consulta')->join("Jurado","Observacion.IdJurado","=","Jurado.IdJurado")->join("Estudiante","Estudiante.id","=","Jurado.IdEstudiante")->join("TipoObservacion","TipoObservacion.IdTipoObservacion","=","Observacion.IdTipoObservacion")->where([['Observacion.IdTesis',$id]])->get()->toArray();
            $Resultado = array_merge($Resultado, $ObservacionOperador);

         }
         if (strcmp($user->Docente, "1") === 0) {
            $ObservacionDocente = Observacion::select('Estudiante.id','Estudiante.Nombre','Observacion.IdObservacion','Observacion.IdTesis','Observacion.IdJurado','Observacion.IdTipoObservacion','Observacion.Descripcion','Observacion.Fecha','Observacion.Correccion','Estudiante.Nombre','TipoObservacion.TipoObservacion','Observacion.Aprobado','Observacion.Consulta')->join("Jurado","Observacion.IdJurado","=","Jurado.IdJurado")->join("Estudiante","Estudiante.id","=","Jurado.IdEstudiante")->join("TipoObservacion","TipoObservacion.IdTipoObservacion","=","Observacion.IdTipoObservacion")->where([['Observacion.IdTesis',$id]])->get()->toArray();
            $Resultado = array_merge($Resultado, $ObservacionDocente);

         }

         if (strcmp($user->Estudiante, "1") === 0) {
            $ObservacionEstudiante = Observacion::select('Estudiante.id','Estudiante.Nombre','Observacion.IdObservacion','Observacion.IdTesis','Observacion.IdJurado','Observacion.IdTipoObservacion','Observacion.Descripcion','Observacion.Fecha','Observacion.Correccion','Estudiante.Nombre','TipoObservacion.TipoObservacion','Observacion.Aprobado','Observacion.Consulta')->join("Jurado","Jurado.IdJurado","=","Observacion.IdJurado")->join("Estudiante","Estudiante.id","=","Jurado.IdEstudiante")->join("TipoObservacion","TipoObservacion.IdTipoObservacion","=","Observacion.IdTipoObservacion")->where([['Observacion.IdTesis',$id]])->get()->toArray();
            $Resultado = array_merge($Resultado, $ObservacionEstudiante);

         }

        return response()->json(array_unique($Resultado, SORT_REGULAR));
    }

    public function show($id){
        $Observacion = Observacion::select('Observacion.IdObservacion','Observacion.IdTipoObservacion','Observacion.Descripcion','Observacion.Fecha','TipoObservacion.TipoObservacion')->join('TipoObservacion','TipoObservacion.IdTipoObservacion','=','Observacion.IdTipoObservacion')->where('Observacion.IdObservacion','=',$id)->get();

        return response()->json(['IdObservacion' => $Observacion[0]->IdObservacion, 'IdTipoObservacion' => $Observacion[0]->IdTipoObservacion, 'Descripcion' => $Observacion[0]->Descripcion, 'Fecha' => $Observacion[0]->Fecha, 'TipoObservacion' => $Observacion[0]->TipoObservacion]);
    }
    //Este metodo es usado para guardar datos correspondiente a una observacion en la base de datos
    public function store(Request $request){
    	request()->validate([
            'IdTesis' => 'required',
            'IdJurado' => 'required',
            'IdTipoObservacion' => 'required|numeric|digits:1',
            'Descripcion' => 'required|min:1|max:499',
            'Fecha' => 'required'
        ]);

        $PuedeObservar = Jurado::select("IdJurado")->where([['IdTesis',$request->IdTesis],['IdJurado',$request->IdJurado],['FinObservaciones','0']])->get();

        if(count($PuedeObservar)>0){
            $Observacion = new Observacion;
            $Observacion->IdTesis = $request->IdTesis;
            $Observacion->IdJurado = $request->IdJurado;
            $Observacion->IdTipoObservacion = $request->IdTipoObservacion;
            $Observacion->Descripcion = $request->Descripcion;
            $Observacion->Fecha = $request->Fecha;
            $Observacion->save();
            return response()->json($Observacion, 200);
        }else{
            return response()->json(["Mensaje" => "Usted culminó sus observaciones, no puede registrar más."]);
        }



    }

//Este metodo es usado para actualizar una observacion de de la base de datos, se requiere de IdObservacion
    public function corregido($id){


        try {
        //Access token from the request
        $token = JWTAuth::parseToken();
        //Try authenticating user
        $user = $token->authenticate();
    } catch (TokenExpiredException $e) {
        //Thrown if token has expired
        return $this->unauthorized('Tu sesion ha expirado, por favor realiza el loguin nuevamente.');
    } catch (TokenInvalidException $e) {
        //Thrown if token invalid
        return $this->unauthorized('Tu credenciales son invalidad, por favor vuelve a loguearte.');
    }catch (JWTException $e) {
        //Thrown if token was not found in the request.
        return $this->unauthorized('Por favor, inicia sesion para continuar.');
    }

        if(strcmp($user->Estudiante, "1") === 0){
            $Observacion = Observacion::find($id);
            if(strcmp($Observacion->Correccion, "0") === 0){
                $Observacion->Correccion = "1";
            }else{
                $Observacion->Correccion = "0";
            }
            $Observacion->save();
        }

        return response()->json(['Actualizacion de correccion' => true]);
    }

    public function aprobado($id){
        $user = $this->ObtenerToken();      //Obtener el token del usuario logueado.
        //verificamos que el estudiante logueado es un administrador del sistema o si es un operador
        $isAdmi = EstudianteRol::where('IdRol', '1')->where("IdEstudiante","=",$user->id)->exists();
        if(EstudianteRol::where('IdRol', '3')->where("IdEstudiante","=",$user->id)->exists() || $isAdmi == true ){
            //verificamos si la observacion le pertenece al usuario que realizo la peticion o es un administrador.
            $verificar = Observacion::select("Estudiante.id")->join("Jurado","Jurado.IdJurado","=","Observacion.IdJurado")->join("Estudiante","Estudiante.id","=","Jurado.IdEstudiante")->where([["Observacion.IdObservacion",$id]])->get();
            if(strcmp($verificar[0]->id, $user->id) === 0 || $isAdmi == true ){
                //Se realiza la modificacion del campo 'Aprobado'
                $Observacion = Observacion::find($id);
                if(strcmp($Observacion->Aprobado, "0") === 0){
                    $Observacion->Aprobado = "1";
                }else{
                    $Observacion->Aprobado = "0";
                }
                $Observacion->save();
                //Envia un mensaje al front
                return response()->json(['Actualizacion' => true]);
            }else{
                return response()->json(['Actualizacion' => false]);
            }
        }
        return response()->json(['Actualizacion' => "no entre"]);
    }
    public function ObtenerToken(){
        try {
            $token = JWTAuth::parseToken();
            $user = $token->authenticate();
            } catch (TokenExpiredException $e) {
                return $this->unauthorized('Tu sesion ha expirado, por favor realiza el loguin nuevamente.');
            } catch (TokenInvalidException $e) {
                return $this->unauthorized('Tu credenciales son invalidad, por favor vuelve a loguearte.');
            }catch (JWTException $e) {
                return $this->unauthorized('Por favor, inicia sesion para continuar.');
            }
        return $user;
    }
//enviar observaciones al correo del estudiante
    public function enviarObservaciones(Request $request){
        $datosEncabezadoCorreo = [];
        $user = $this->ObtenerToken();      //Obtener el token del usuario logueado.
        //Verificamos si es un operador o un docente.
        if (strcmp($user->Operador, "1") === 0 || strcmp($user->Docente, "1") === 0) {
            //Contamos cuantas observaciones ha realizado el usuario y cuantas fueron aprobadas.
            $verificar = Observacion::where([["IdJurado",$request->IdJurado],["Aprobado","0"],["IdTesis",$request->IdTesis]])->count();
            //Si no hay observaciones sin el check de aprobado, devolvemos un mensaje informandolo.
            if($verificar <= 0){
                return response()->json(['Mensaje' =>"No hay observaciones nuevas que notificar."]);
            }
            //Obtenemos los datos del estudiante al cual pertenece la tesis.
            $Tesis = Tesis::select("Tesis.TituloTesis","Estudiante.Nombre","Estudiante.Correo")->join("EstudianteEscuela","EstudianteEscuela.IdEstudianteEscuela","=","Tesis.IdEstudianteEscuela")->join("Estudiante","Estudiante.id","=","EstudianteEscuela.IdEstudiante")->where([["Tesis.IdTesis",$request->IdTesis]])->get();
            date_default_timezone_set("America/Lima");
            $fecha = new DateTime();
            //Obtenemos las observaciones generales y especificas que no esten aprobadas.
            $Generales = Observacion::select("IdObservacion","Descripcion")->where([["IdJurado",$request->IdJurado],["IdTipoObservacion","1"], ["Aprobado","0"],["IdTesis",$request->IdTesis]])->get();
            $Especificas = Observacion::select("IdObservacion","Descripcion")->where([["IdJurado",$request->IdJurado],["IdTipoObservacion","2"], ["Aprobado","0"],["IdTesis",$request->IdTesis]])->get();
            //En cuyo caso tengan el check de corregido, las cambiamos a no corregidas.
            $this->cambiarCorregido($Generales);
            $this->cambiarCorregido($Especificas);
            //Construimos el encabezado con los datos encesarios para el envio del correo (Titulo de tesis, Nombre del dueno de la tesis, nombre del jurado que enviara observaciones y fecha de envio de correo.)
            $datosEncabezadoCorreo [] = $Tesis[0]->TituloTesis;
            $datosEncabezadoCorreo [] = $Tesis[0]->Nombre;
            $datosEncabezadoCorreo [] = $user->Nombre;
            $datosEncabezadoCorreo [] = $fecha->format('Y-m-d H:i:s');
            $destino = $Tesis[0]->Correo;
            //Se envia los datos del encabezado, las observaciones y el correo destino(del estudiante).
            $this->sendEmail($datosEncabezadoCorreo, $Generales,$Especificas, $destino);
            //notificamos el envio de correo al front.
            return response()->json(['Mensaje' =>"Se ha notificado al estudiante de forma correcta."]);
        }
        return response()->json(['Mensaje' =>"No autorizado."]);    //Si no es el docente el que se ha logueado, informamos al front.
    }
    public function sendEmail($datosEncabezadoCorreo, $Generales,$Especificas, $Correo){
        Mail::to($Correo)->send(new MessageDocToStud($datosEncabezadoCorreo, $Generales,$Especificas));
    }
    public function sendEmailToOperador($Tesis, $Correo){
        Mail::to($Correo)->send(new MessageToOperator($Tesis[0], $Correo));
    }
    public function sendEmailFechaGraduacion($Correo,$NombreTesista,$FechaGraduacion){
        Mail::to($Correo)->send(new MessageFechaGraduacion($NombreTesista, $FechaGraduacion));
    }


    public function notificarOperador(Request $request){

        $verificarA = Observacion::where([["Aprobado","0"],["IdTesis",$request->IdTesis]])->count();
        $verificarC = Observacion::where([["Correccion","0"],["IdTesis",$request->IdTesis]])->count();
        if($verificarA == 0 && $verificarC == 0){
            $estado = Tesis::select("Tesis.IdTesis","Escuela.Escuela","Estudiante.Nombre","Tesis.TituloTesis","Estado.Estado")->join("Estado","Tesis.IdEstado","=","Estado.IdEstado")->join("EstudianteEscuela","EstudianteEscuela.IdEstudianteEscuela","=","Tesis.IdEstudianteEscuela")->join("Escuela","Escuela.idEscuela","=","EstudianteEscuela.IdEscuela")->join("Estudiante","Estudiante.id","=","EstudianteEscuela.IdEstudiante")->where([["Tesis.IdTesis",$request->IdTesis]])->get();
            $correo = "doviedoy@unsa.edu.pe";
            $this->sendEmailToOperador($estado, $correo);
            return response()->json(['Mensaje' => 'Se ha notificado al operador a cargo']);
        }else{
            return response()->json(['Mensaje' => 'Aun faltan corregir y aprobar algunas observaciones de esta tesis']);
        }
        return response()->json(['Mensaje' => 'Aun faltan corregir y aprobar algunas observaciones de esta tesis']);
    }

    public function cambiarCorregido($ListaObservacion){
        $Datos = [];
        foreach ($ListaObservacion as $observacion) {
            $Datos[] = $observacion->IdObservacion;
        }

        Observacion::whereIn('IdObservacion',$Datos)->update(['Correccion' => 0]);
    }

    //funcion para editar una observacion, solo se puede editar las observaciones que el usuario haya hecho
    public function update($id, Request $request){
        $user = $this->ObtenerToken();      //Obtener el token del usuario logueado.
        //Verificamos si el usuario logueado es un administrador.
        if(EstudianteRol::where('IdRol', '1')->where("IdEstudiante","=",$user->id)->exists()){
            $Observacion = Observacion::find($id);
            $Observacion->IdTipoObservacion = $request->IdTipoObservacion;
            $Observacion->Fecha = $request->Fecha;
            $Observacion->Descripcion = $request->Descripcion;
            $Observacion->save();
            return response()->json(['Actualizacion' => true]);
        }
        //Verificamos si el usuario logueado es un Operador.
        if(strcmp($user->Operador, "1") === 0){
            $Observacion = Observacion::find($id);
            $Observacion->IdTipoObservacion = $request->IdTipoObservacion;
            $Observacion->Fecha = $request->Fecha;
            $Observacion->Descripcion = $request->Descripcion;
            $Observacion->save();
            return response()->json(['Actualizacion' => true]);
        }
        //Verificamos si la observacion le pertenece al doente logueado
            $verificar = Observacion::select("Estudiante.id")->join("Jurado","Jurado.IdJurado","=","Observacion.IdJurado")->join("Estudiante","Estudiante.id","=","Jurado.IdEstudiante")->where([["Observacion.IdObservacion",$id]])->get();
                if(strcmp($verificar[0]->id, $user->id) === 0){
                    $Observacion = Observacion::find($id);
                    //Una vez verificado, se consulta si aun puede realizar modificaciones o registrar en la tabla observaciones.
                    $PuedeObservar = Jurado::where([['IdTesis',$Observacion->IdTesis],['IdEstudiante',$user->id],['FinObservaciones','0']])->get();
                    if(count($PuedeObservar)>0){
                        //continuamos con la ejecucion del metodo
                    }else{
                        return response()->json(["Actualizacion" => false]);
                    }
                    //Si aun no presiono el boton 'Fin observaciones' realiza la actualizacion
                    $Observacion->IdTipoObservacion = $request->IdTipoObservacion;
                    $Observacion->Fecha = $request->Fecha;
                    $Observacion->Descripcion = $request->Descripcion;
                    $Observacion->save();
                    return response()->json(['Actualizacion' => true]);
                    }else{
                        return response()->json(['Actualizacion' => false]);
                    }
        return response()->json(['Actualizacion' => false]);
    }
//Este metodo es usado para eliminar una observacion de la base de datos, se requiere IdObservacion
    public function destroy(Request $request, $id)
    {
        try {
            //Access token from the request
            $token = JWTAuth::parseToken();
            //Try authenticating user
            $user = $token->authenticate();
        } catch (TokenExpiredException $e) {
            //Thrown if token has expired
            return $this->unauthorized('Tu sesion ha expirado, por favor realiza el loguin nuevamente.');
        } catch (TokenInvalidException $e) {
            //Thrown if token invalid
            return $this->unauthorized('Tu credenciales son invalidad, por favor vuelve a loguearte.');
        }catch (JWTException $e) {
            //Thrown if token was not found in the request.
            return $this->unauthorized('Por favor, inicia sesion para continuar.');
        }

            if(EstudianteRol::where('IdRol', '1')->where("IdEstudiante","=",$user->id)->exists()){
                $Observacion = Observacion::find($id);
                $Observacion->delete();
                return response()->json(['Eliminacion' => true]);
            }

            if(strcmp($user->Operador, "1") === 0){

                $Observacion = Observacion::find($id);
                $Observacion->delete();
                return response()->json(['Eliminacion' => true]);

            }

                $verificar = Observacion::select("Estudiante.id")->join("Jurado","Jurado.IdJurado","=","Observacion.IdJurado")->join("Estudiante","Estudiante.id","=","Jurado.IdEstudiante")->where([["Observacion.IdObservacion",$id]])->get();
                if(strcmp($verificar[0]->id, $user->id) === 0){

                    $Observacion = Observacion::find($id);
                    $Observacion->delete();
                    return response()->json(['Eliminacion' => true]);
                    }else{
                        return response()->json(['Eliminacion' => false]);
                    }

        return response()->json(['Eliminacion' => false]);
    }

    public function agregarConsulta(Request $request, $id){
         $Observacion = Observacion::find($id);
         $Observacion->Consulta = $request->Consulta;
         $Observacion->save();
          return response()->json(['Agregar consulta ' => true]);
    }

    public function enviarCorrecciones(Request $request){
        $arrayJurados=[];
        $arrayObservaciones = [];
        $arrayIdObservaciones = $request->idsObservaciones;
        //return response()->json(['Agregar consulta ' => $arrayIdObservaciones]);
        $idTesis = Observacion::distinct()->select('IdTesis')->where([['IdObservacion',$arrayIdObservaciones[0]]])->get();
        $Tesista = Tesis::select('Estudiante.Nombre','Tesis.TituloTesis')->join('EstudianteEscuela','EstudianteEscuela.IdEstudianteEscuela','=','Tesis.IdEstudianteEscuela')->join('Estudiante','Estudiante.id','=','EstudianteEscuela.IdEstudiante')->where('Tesis.IdTesis',$idTesis[0]->IdTesis)->get();

        $NombreTesista = $Tesista[0]->Nombre;
        $TituloTesis = $Tesista[0]->TituloTesis;
        $HistoricoArchivo = HistoricoArchivo::select('IdTesis','Fecha','Numero')->where('IdTesis',$idTesis[0]->IdTesis)->get();
        $Fecha = $HistoricoArchivo[count($HistoricoArchivo)-1]->Fecha;
         $idsJurados = Observacion::distinct()->select('IdJurado')->whereIn('IdObservacion',$arrayIdObservaciones)->get();
           $arrayObsPorJurado = [];
           $arp=array();
         $arrayPlantilla=array();
        // return response()->json(['Agregar consulta ' => $arrayIdObservaciones]);
         foreach ($idsJurados as $idJurado){
            $arrayName =  array('TituloTesis'=>$TituloTesis,'Tesista'=>$NombreTesista,"IdJurado"=>$idJurado['IdJurado'],"NombreJurado" => "","Correo" => "",'Fecha'=>$Fecha,"ObsGenerales"=>array(),"ObsEspecificos"=>array());
            array_push($arrayPlantilla, $arrayName);
         }

         $Observaciones = Observacion::select('Observacion.IdObservacion','Observacion.IdJurado','Observacion.IdTipoObservacion','Observacion.Descripcion','Observacion.Consulta','Observacion.Correccion','Estudiante.Nombre','Estudiante.Correo')->whereIn('IdObservacion',$arrayIdObservaciones)->join('Jurado','Jurado.IdJurado','=','Observacion.IdJurado')->join('Estudiante','Estudiante.id','=','Jurado.IdEstudiante')->get()->toArray();

         $asd=array();
        for ($i=0;$i<count($idsJurados);$i++) {
            $arrayObsGenerales = array();
            foreach ($Observaciones as $Observacion) {
                if(strcmp($Observacion['IdJurado'], $arrayPlantilla[$i]['IdJurado']) === 0){
                    $arrayPlantilla[$i]['NombreJurado']=$Observacion['Nombre'];
                    $arrayPlantilla[$i]['Correo']=$Observacion['Correo'];
                    $estado="Consulta";
                    $consulta = $Observacion['Consulta'];
                    if(strcmp($Observacion['Correccion'], 1) === 0){
                        $estado="Corregido";
                        $consulta ="";
                    }

                    if(strcmp($Observacion['IdTipoObservacion'], 1) === 0){
                        array_push($arrayPlantilla[$i]['ObsGenerales'], array('Descripcion' =>$Observacion['Descripcion'],'Estado'=>$estado,'Consulta'=>$consulta));
                    }
                    if(strcmp($Observacion['IdTipoObservacion'], 2) === 0){
                        array_push($arrayPlantilla[$i]['ObsEspecificos'], array('Descripcion' =>$Observacion['Descripcion'],'Estado'=>$estado,'Consulta'=>$consulta));
                    }
                }
             }
         }
        foreach ($arrayPlantilla as $arrayP) {
             $this->sendEmailJuradoToTesista($arrayP);
        }
        return response()->json($arrayPlantilla, 200);
    }



    public function sendEmailJuradoToTesista($arrayDatos){

        Mail::to($arrayDatos['Correo'])->send(new SendMailTesistaToJurado($arrayDatos['TituloTesis'],$arrayDatos['Tesista'],$arrayDatos['NombreJurado'],$arrayDatos['Fecha'],$arrayDatos['ObsGenerales'],$arrayDatos['ObsEspecificos']));
    }

}
