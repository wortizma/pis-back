<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EstudianteEscuela;
use App\EstudianteRol;
use App\Http\Traits\CrearEstudianteEscuela;
use App\User;
class EstudianteEscuelaController extends Controller
{
    use CrearEstudianteEscuela;
    //Este metodo es usado para listar a los estudiantes que corresponden a una determinada escuela, se requiere idEstudiante de  la tabla Estudiante escuela
	public function index($id)
    {
        $Estudiante = User::find($id);
        //Si es un estudiante, debe listar su grado y promocion.
        if(strcmp($Estudiante->Estudiante, "1") === 0){

            $EstudianteEscuelaConGrado = EstudianteEscuela::select('EstudianteEscuela.IdEstudianteEscuela','Escuela.idEscuela', 'Escuela.Escuela', 'Facultad.Facultad','EstudianteEscuela.IdGrado','Grado.Grado','EstudianteEscuela.Promocion')->join('Escuela', 'Escuela.idEscuela', '=', 'EstudianteEscuela.IdEscuela')->join('Facultad','Escuela.IdFacultad','=','Facultad.IdFacultad')->join('Grado','Grado.IdGrado','=','EstudianteEscuela.IdGrado')->where('EstudianteEscuela.IdEstudiante', $id)->get()->toArray();
            $EstudianteEscuelaSinGrado = EstudianteEscuela::select('EstudianteEscuela.IdEstudianteEscuela','Escuela.idEscuela', 'Escuela.Escuela', 'Facultad.Facultad','EstudianteEscuela.IdGrado','EstudianteEscuela.Promocion')->join('Escuela', 'Escuela.idEscuela', '=', 'EstudianteEscuela.IdEscuela')->join('Facultad','Escuela.IdFacultad','=','Facultad.IdFacultad')->where('EstudianteEscuela.IdEstudiante', $id)->where('EstudianteEscuela.IdGrado','=',null)->get()->toArray();
            $EstudianteEscuela = array();
            $EstudianteEscuela = array_merge($EstudianteEscuelaConGrado, $EstudianteEscuelaSinGrado);

        }else{
            //Si es un cualquier otro usuario, estos no tienen grado ni promocion.
            $EstudianteEscuela = EstudianteEscuela::select('EstudianteEscuela.IdEstudianteEscuela','Escuela.idEscuela', 'Escuela.Escuela', 'Facultad.Facultad')->join('Escuela', 'Escuela.idEscuela', '=', 'EstudianteEscuela.IdEscuela')->join('Facultad','Escuela.IdFacultad','=','Facultad.IdFacultad')->where('EstudianteEscuela.IdEstudiante', $id)->get();
        }

        return response()->json($EstudianteEscuela, 200);

    }

//Este metodo es usado para guardar datos correspondientes a la tabla EstudianteEscuela
    public function store(Request $request){
       // User::where('IdEscuela', $request->IdEscuela)->where("IdEstudiante","=", $request->IdEstudiante)->exists();
        $respuesta = $this->StoreEstudianteEscuela($request);
        return response()->json(['Mensaje' => $respuesta]);

    }
    public function show($id){
        //Verificamos el dato del estudiante escuela consultado.
        $datos = EstudianteEscuela::select('EstudianteEscuela.IdEstudianteEscuela','EstudianteEscuela.IdEscuela','Escuela.Escuela','EstudianteEscuela.IdEstudiante','EstudianteEscuela.IdGrado','EstudianteEscuela.Promocion')->join('Escuela','Escuela.idEscuela','=','EstudianteEscuela.IdEscuela')->where('EstudianteEscuela.IdEstudianteEscuela','=', $id)->get();
        //En caso de no ser estudiante, los campos grado y promocion son nulos.
        if($datos[0]->IdGrado == NULL){
            return response()->json(['IdEstudianteEscuela' => $datos[0]->IdEstudianteEscuela, 'IdEscuela' => $datos[0]->IdEscuela, 'Escuela' => $datos[0]->Escuela, 'IdEstudiante' => $datos[0]->IdEstudiante, 'IdGrado' => $datos[0]->IdGrado, 'Grado' => 'null', 'Promocion' => 'null']);
        }
        //En caso de ser un estudiante, se consulta su grado y promocion.
        $datos = EstudianteEscuela::select('EstudianteEscuela.IdEstudianteEscuela','EstudianteEscuela.IdEscuela','Escuela.Escuela','EstudianteEscuela.IdEstudiante','EstudianteEscuela.IdGrado','Grado.Grado','EstudianteEscuela.Promocion')->join('Escuela','Escuela.idEscuela','=','EstudianteEscuela.IdEscuela')->join('Grado','Grado.IdGrado','=','EstudianteEscuela.IdGrado')->where('EstudianteEscuela.IdEstudianteEscuela','=',$id)->get();
        return response()->json($datos[0],201);
    }

    public function update(Request $request, $id){
        $idEstEsc = $id;
        $id = $request->IdEstudiante;
        $atributos = User::find($id);

        if(strcmp($atributos->Operador, "1") === 0 && strcmp($atributos->Estudiante, "1") === 0){


            if($request->input('IdGrado')){
                if($request->IdGrado !== NULL && $request->IdGrado !== ''){
                    if(EstudianteEscuela::where('IdEscuela', $request->IdEscuela)->where([["IdEstudiante", $request->IdEstudiante],["IdGrado",  $request->IdGrado],['IdEstudianteEscuela','!=',$idEstEsc]])->exists()){
                        return response()->json(['Mensaje' => "Este registro ya existe"]);

                    }
                    $dato = EstudianteEscuela::find($idEstEsc);
                    $dato->IdEscuela = $request->IdEscuela;
                    $dato->IdEstudiante = $request->IdEstudiante;
                    $dato->IdGrado = $request->IdGrado;
                    $dato->Promocion = $request->Promocion;
                    $dato->save();
                    return response()->json(['Mensaje' => "Registro Actualizado."]);

                }

            }
            if(EstudianteEscuela::where('IdEscuela', $request->IdEscuela)->where([["IdEstudiante", $request->IdEstudiante],['IdEstudianteEscuela','!=',$idEstEsc]])->exists()){
                return response()->json(['Mensaje' => "Este registro ya existe"]);
                }

                    $dato = EstudianteEscuela::find($idEstEsc);
                    $dato->IdEscuela = $request->IdEscuela;
                    $dato->IdEstudiante = $request->IdEstudiante;
                    $dato->save();
            return response()->json(['Mensaje' => "Registro Actualizado."]);

        }

        if(strcmp($atributos->Estudiante, "1") === 0){
            if(EstudianteEscuela::where('IdEscuela', $request->IdEscuela)->where([["IdEstudiante", $request->IdEstudiante],["IdGrado",  $request->IdGrado],['IdEstudianteEscuela','!=',$idEstEsc]])->exists()){
                    return response()->json(['Mensaje' => "Este registro ya existe"]);
                }
            $dato = EstudianteEscuela::find($idEstEsc);
            $dato->IdEscuela = $request->IdEscuela;
            $dato->IdEstudiante = $request->IdEstudiante;
            $dato->IdGrado = $request->IdGrado;
            $dato->Promocion = $request->Promocion;
            $dato->save();
            return response()->json(['Mensaje' => "Registro Actualizado."]);
        }

        if(strcmp($atributos->Operador, "1") === 0){
             if(EstudianteEscuela::where('IdEscuela', $request->IdEscuela)->where([["IdEstudiante", $request->IdEstudiante],['IdEstudianteEscuela','!=',$idEstEsc]])->exists()){
                return response()->json(['Mensaje' => "Este registro ya existe"]);
                }
            $dato = EstudianteEscuela::find($idEstEsc);
            $dato->IdEscuela = $request->IdEscuela;
            $dato->IdEstudiante = $request->IdEstudiante;
            $dato->save();
            return response()->json(['Mensaje' => "Registro Actualizado."]);
        }
    }

//Este metodo es usado para  eliminar uan fila de la tabla EstudianteEscuela, se requierer idEstudianteEscuela
    public function destroy($id){
    	$EstudianteEscuela = EstudianteEscuela::find($id);
        $EstudianteEscuela->delete();

        return response()->json(['Eliminacion' => true]);
    }


}
