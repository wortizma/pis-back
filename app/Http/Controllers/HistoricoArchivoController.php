<?php

namespace App\Http\Controllers;
use DateTime;
use Illuminate\Http\Request;
use App\HistoricoArchivo;
class HistoricoArchivoController extends Controller
{
	  public function store(Request $request){

        $HistoricoArchivo = new HistoricoArchivo;
        $HistoricoArchivo->IdTesis = $request->IdTesis;
        $HistoricoArchivo->NombreArchivo = $request->NombreArchivo;
        date_default_timezone_set("America/Lima");
        $fecha = new DateTime();
        $HistoricoArchivo->Fecha = $fecha;


        $HistoricoArchivo->Tamano = $request->Tamano;

        $historicos = HistoricoArchivo::select('NombreArchivo')->where('IdTesis', $request->IdTesis)->get();
        $numero = count($historicos)+1;
        $HistoricoArchivo->Numero = $numero;

        $HistoricoArchivo->save();
        return response()->json($HistoricoArchivo, 200);
    }
}