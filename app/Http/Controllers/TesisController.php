<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Mail\MessageEstadoRevision;
use App\Mail\MessageDictamen;
use App\Mail\MessageFaseAsesoria;
use App\Mail\MessageToJuradosTesista;
use App\Mail\MessageFechaGraduacion;

use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;
use App\Tesis;
use DateTime;
use App\Jurado;
use App\Estudiante;
use App\Dictamen;
use App\EstudianteEscuela;
use App\Http\Controllers\JuradoController;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailUploadTesis;
class TesisController extends Controller
{
    //Este metodo es usado para  listar todas tesis asociadas a un asesor , se requiere de IdEstudianteEscuela
    public function index($id)
    {
        try {
        //Access token from the request
        $token = JWTAuth::parseToken();
        //Try authenticating user
        $user = $token->authenticate();
    } catch (TokenExpiredException $e) {
        //Thrown if token has expired
        return $this->unauthorized('Tu sesion ha expirado, por favor realiza el loguin nuevamente.');
    } catch (TokenInvalidException $e) {
        //Thrown if token invalid
        return $this->unauthorized('Tu credenciales son invalidad, por favor vuelve a loguearte.');
    }catch (JWTException $e) {
        //Thrown if token was not found in the request.
        return $this->unauthorized('Por favor, inicia sesion para continuar.');
    }

        if(strcmp($user->Operador, "1") === 0){
            $Tesis = Tesis::distinct()->select('HistoricoArchivo.Fecha','Tesis.IdTesis','Jurado.IdJurado','TipoTesis.TipoTesis','Tesis.TituloTesis','Estudiante.id', 'Estudiante.Nombre','Estado.Estado','Dictamen.Dictamen','Tesis.RutaArchivo')->join('TipoTesis', 'TipoTesis.IdTipoTesis', '=', 'Tesis.IdTipoTesis')->join('Jurado', 'Jurado.IdTesis', '=', 'Tesis.IdTesis')->join('TipoJurado', 'TipoJurado.IdTipoJurado', '=', 'Jurado.IdTipoJurado')->join('Estudiante', 'Estudiante.id', '=', 'Jurado.IdEstudiante')->join('Estado','Tesis.IdEstado','=','Estado.IdEstado')->join('Dictamen','Dictamen.IdDictamen','=','Tesis.IdDictamen')->leftJoin('HistoricoArchivo', function($query) {
    $query->on('HistoricoArchivo.IdTesis','=','Tesis.IdTesis')
    ->whereRaw('HistoricoArchivo.IdHistoricoArchivo IN (select MAX(a2.IdHistoricoArchivo) from HistoricoArchivo as a2 join Tesis as u2 on u2.IdTesis = a2.IdTesis group by u2.IdTesis)');
    })->where([['Tesis.IdEstudianteEscuela',$id],['TipoJurado.TipoJurado','Asesor']])->get();
             return response()->json($Tesis, 200);
        }

        $IdsEstudianteEscuela = EstudianteEscuela::distinct()->select('IdEstudianteEscuela')->where('EstudianteEscuela.IdEstudiante','=',$user->id)->get();

        $datosEstudianteEscuela = [];

        foreach ($IdsEstudianteEscuela as $EstEsc)
            {
                $datosEstudianteEscuela[] = $EstEsc->IdEstudianteEscuela;
             }

        if (in_array($id, $datosEstudianteEscuela)) {
            $Tesis = Tesis::distinct()->select('HistoricoArchivo.Fecha','Tesis.IdTesis','Jurado.IdJurado','TipoTesis.TipoTesis','Tesis.TituloTesis','Estudiante.id', 'Estudiante.Nombre','Estado.Estado','Dictamen.Dictamen','Tesis.RutaArchivo')->join('TipoTesis', 'TipoTesis.IdTipoTesis', '=', 'Tesis.IdTipoTesis')->join('Jurado', 'Jurado.IdTesis', '=', 'Tesis.IdTesis')->join('TipoJurado', 'TipoJurado.IdTipoJurado', '=', 'Jurado.IdTipoJurado')->join('Estudiante', 'Estudiante.id', '=', 'Jurado.IdEstudiante')->join('Estado','Tesis.IdEstado','=','Estado.IdEstado')->join('Dictamen','Dictamen.IdDictamen','=','Tesis.IdDictamen')->leftJoin('HistoricoArchivo', function($query) {
    $query->on('HistoricoArchivo.IdTesis','=','Tesis.IdTesis')
    ->whereRaw('HistoricoArchivo.IdHistoricoArchivo IN (select MAX(a2.IdHistoricoArchivo) from HistoricoArchivo as a2 join Tesis as u2 on u2.IdTesis = a2.IdTesis group by u2.IdTesis)');
    })->where([['Tesis.IdEstudianteEscuela',$id],['TipoJurado.TipoJurado','Asesor']])->get();

        return response()->json($Tesis, 200);
         }

        if(strcmp($user->Docente, "1") === 0){
            $Tesis = Tesis::distinct()->select('HistoricoArchivo.Fecha','Tesis.IdTesis','Jurado.IdJurado','TipoTesis.TipoTesis','Tesis.TituloTesis','Estudiante.id', 'Estudiante.Nombre','Estado.Estado','Dictamen.Dictamen','Tesis.RutaArchivo')->join('TipoTesis', 'TipoTesis.IdTipoTesis', '=', 'Tesis.IdTipoTesis')->join('Jurado', 'Jurado.IdTesis', '=', 'Tesis.IdTesis')->join('TipoJurado', 'TipoJurado.IdTipoJurado', '=', 'Jurado.IdTipoJurado')->join('Estudiante', 'Estudiante.id', '=', 'Jurado.IdEstudiante')->join('Estado','Tesis.IdEstado','=','Estado.IdEstado')->join('Dictamen','Dictamen.IdDictamen','=','Tesis.IdDictamen')->leftJoin('HistoricoArchivo', function($query) {
    $query->on('HistoricoArchivo.IdTesis','=','Tesis.IdTesis')
    ->whereRaw('HistoricoArchivo.IdHistoricoArchivo IN (select MAX(a2.IdHistoricoArchivo) from HistoricoArchivo as a2 join Tesis as u2 on u2.IdTesis = a2.IdTesis group by u2.IdTesis)');
    })->where([['Jurado.IdEstudiante',$user->id],['Tesis.IdEstudianteEscuela',$id]])->get();
            foreach ($Tesis as $individual)
            {
                $Tesis2 = Tesis::distinct()->select('Estudiante.id','Estudiante.Nombre')->join('TipoTesis', 'TipoTesis.IdTipoTesis', '=', 'Tesis.IdTipoTesis')->join('Jurado', 'Jurado.IdTesis', '=', 'Tesis.IdTesis')->join('TipoJurado', 'TipoJurado.IdTipoJurado', '=', 'Jurado.IdTipoJurado')->join('Estudiante', 'Estudiante.id', '=', 'Jurado.IdEstudiante')->join('Estado','Tesis.IdEstado','=','Estado.IdEstado')->where([['Tesis.IdEstudianteEscuela',$id],['TipoJurado.TipoJurado','Asesor']])->get();
               $individual->Nombre = $Tesis2[0]->Nombre;
               $individual->id = $Tesis2[0]->id;
             }

            return response()->json($Tesis, 200);
        }

        $Tesis = Tesis::distinct()->select('HistoricoArchivo.Fecha','Tesis.IdTesis','Jurado.IdJurado','TipoTesis.TipoTesis','Tesis.TituloTesis', 'Estudiante.id','Estudiante.Nombre','Estado.Estado','Dictamen.Dictamen','Tesis.RutaArchivo')->join('TipoTesis', 'TipoTesis.IdTipoTesis', '=', 'Tesis.IdTipoTesis')->join('Jurado', 'Jurado.IdTesis', '=', 'Tesis.IdTesis')->join('TipoJurado', 'TipoJurado.IdTipoJurado', '=', 'Jurado.IdTipoJurado')->join('Estudiante', 'Estudiante.id', '=', 'Jurado.IdEstudiante')->join('Estado','Tesis.IdEstado','=','Estado.IdEstado')->join('Dictamen','Dictamen.IdDictamen','=','Tesis.IdDictamen')->leftJoin('HistoricoArchivo', function($query) {
    $query->on('HistoricoArchivo.IdTesis','=','Tesis.IdTesis')
    ->whereRaw('HistoricoArchivo.IdHistoricoArchivo IN (select MAX(a2.IdHistoricoArchivo) from HistoricoArchivo as a2 join Tesis as u2 on u2.IdTesis = a2.IdTesis group by u2.IdTesis)');
    })->where([['Tesis.IdEstudianteEscuela',$id],['TipoJurado.TipoJurado','Asesor']])->get();

        return response()->json($Tesis, 200);

    }

    //Este metodo es usado para guardar datos correspondiente a una tesis
    public function store(Request $request){
        request()->validate([
        
            'TituloTesis' => 'required|min:3|max:250',

          //  'TituloTesis' => 'required|min:3|max:250|regex:/^[a-zA-ZäÄëËïÏöÖüÜáéíóúáéíóúÁÉÍÓÚÂÊÎÔÛâêîôûàèìòùÀÈÌÒÙÑñ0-9- \"\#\$\%\&\?\¿\*\+\/\=\º\(\)\,\.\:\;\’\°\”\“[:space:]]*$/',
        //    'Resumen' => 'required|min:3|max:99|regex:/^[a-zA-ZáéíóúüñÁÉÍÓÚÜÑ.,;:[:space:]]*$/',
           // 'PalabrasClave' => 'required|min:3|max:99|regex:/^[a-zA-ZáéíóúüñÁÉÍÓÚÜÑ.,;:[:space:]]*$/',

          ],[
            'TituloTesis.required' => 'El Titulo de la Tesis tiene caracteres no validos.',
        //    'Resumen.required' => 'El Resumen tiene caracteres no validos.',
         //  'PalabrasClave.required' => 'Las palabras claves tiene caracteres no validos.',
          ]);
    	$Tesis = new Tesis;
        $Tesis->IdEstudianteEscuela = $request->IdEstudianteEscuela;
        $Tesis->IdTipoTesis = $request->IdTipoTesis;
        $Tesis->IdEstado = $request->IdEstado;
        $Tesis->IdDictamen = $request->IdDictamen;
        $Tesis->TituloTesis = $request->TituloTesis;

        $Tesis->Resumen = null;
        $Tesis->PalabrasClave = null;

        if (isset($request->Resumen))
            $Tesis->Resumen = $request->Resumen;
        if (isset($request->PalabrasClave))
            $Tesis->PalabrasClave = $request->PalabrasClave;


        /*
        return response()->json(['mensaje'=>$Tesis->Resumen]);
        if (isset($request->Resumen)) {
            return response()->json(['mensaje'=>'resumen tiene valor']);
        }else{
            return response()->json(['mensaje'=>'resumen es null']);
        }*/
        // return response()->json(['mensaje'=>$Tesis->Resumen,'palabras clvae'=>$Tesis->PalabrasClave]);

        $Tesis->RutaArchivo = null;
        date_default_timezone_set("America/Lima");
        $fecha = new DateTime();
        $Tesis->FechaRegistro = $fecha;
        $Tesis->FechaActualizacion = $fecha;

        $idEstEsc=$request->IdEstudianteEscuela;

        $correoTesista = EstudianteEscuela::select('Estudiante.Correo','Estudiante.Nombre')->join('Estudiante','Estudiante.id','=','EstudianteEscuela.IdEstudiante')->where('EstudianteEscuela.IdEstudianteEscuela',$Tesis->IdEstudianteEscuela)->get();

        $correo =$correoTesista[0]->Correo;
        $nombreTesista=$correoTesista[0]->Nombre;
        if ($request->hasFile('file')) {
            $Escuela = EstudianteEscuela::distinct()->select('Escuela.Directorio','Estudiante.DNI','Grado.Grado')->join('Escuela','Escuela.IdEscuela','=','EstudianteEscuela.IdEscuela')->join('Grado','Grado.IdGrado','=','EstudianteEscuela.IdGrado')->where('EstudianteEscuela.IdEstudianteEscuela',$idEstEsc)->join('Estudiante','Estudiante.id','=','EstudianteEscuela.IdEstudiante')->get();

            $file = $request->File('file');
            $file->move(public_path().'/'.$Escuela[0]->Directorio.'/'.$Escuela[0]->DNI.'/Resolucion Asesor/',$Escuela[0]->DNI.'_'.$Escuela[0]->Grado.'_resolucionAsesor.pdf');
            $Tesis->ResolucionAsesor = $Escuela[0]->Directorio.'/'.$Escuela[0]->DNI.'/Resolucion Asesor/'.$Escuela[0]->DNI.'_'.$Escuela[0]->Grado.'_resolucionAsesor.pdf';
        }


        $Tesis->save();
        if($request->input('IdEstudiante')){
            $currentIdTesis = Tesis::distinct()->select('IdTesis')->where([['IdEstudianteEscuela',$request->IdEstudianteEscuela],['IdTipoTesis',$request->IdTipoTesis],['IdEstado',$request->IdEstado],['IdDictamen',$request->IdDictamen],['TituloTesis',$request->TituloTesis]])->get();
            $correoAsesor = Estudiante::select('Correo')->where('id',$request->IdEstudiante)->get();
            $request->replace(['IdTesis' => $Tesis->IdTesis,'IdEstudiante' => $request->IdEstudiante,'IdTipoJurado' => 1]);
            $resultado = app('App\Http\Controllers\JuradoController')->store($request);
        }

        $this->sendEmailFaseAsesoria($correo,$nombreTesista,$Tesis->ResolucionAsesor);
        $this->sendEmailFaseAsesoria($correoAsesor[0]->Correo,$nombreTesista,$Tesis->ResolucionAsesor);

           return $Tesis;
    }

     public function sendEmailFaseAsesoria($Correo,$NombreEstudiante,$file){
        Mail::to($Correo)->send(new MessageFaseAsesoria($NombreEstudiante,$file));
    }

    public function sendEmailJuradosTesista($Correo,$NombreEstudiante,$file){
        Mail::to($Correo)->send(new MessageToJuradosTesista($NombreEstudiante,$file));
    }

    public function sendEmailUploadTesis($correo){

        Mail::to($correo)->send(new SendMailUploadTesis());
    }


    //Este metodo es usado para eliminar una tesis de la base de datos, se requiere de IdTesis
    public function destroy($id){

        $Tesis = Tesis::find($id);
        $Tesis->delete();

        return response()->json(['Eliminacion' => true]);
    }

//Este metodo es usado para obtener el archivo PDF de la tesis, se requiere IdTesis
    public function obtenerArchivoTesis(Request $request,$id){
         //Obtengo el nombre del achivo (ruta)
         $nombrePDF = Tesis::distinct()->select('RutaArchivo')->where('IdTesis',$id)->get();

         //Obteniendo el archivo solicitado
         $file = File($nombrePDF[0]->RutaArchivo);
         $archivo=file_get_contents($nombrePDF[0]->RutaArchivo);

         //Codificando en base64
         $arc=base64_encode($archivo);
         return response()->json($arc, 200);
    }

//Este metodo es usado para obtener el archivo PDF de ResolucionAsesor, se requiere IdTesis
    public function obtenerArchivoResolucionAsesor(Request $request,$id){
        //Obtengo el nombre del achivo (ruta)
        $nombrePDF = Tesis::distinct()->select('ResolucionAsesor')->where('IdTesis',$id)->get();

         //Obteniendo el archivo solicitado
        $file = File($nombrePDF[0]->ResolucionAsesor);
        $archivo=file_get_contents($nombrePDF[0]->ResolucionAsesor);

        //Codificando en base64
        $arc=base64_encode($archivo);
        return response()->json($arc, 200);
    }

//Este metodo es usado para obtener el archivo PDF de la ResolucionJurado, se requiere IdTesis
    public function obtenerArchivoResolucionJurado(Request $request,$id){
        //Obtengo el nombre del achivo (ruta)
         $nombrePDF = Tesis::distinct()->select('ResolucionJurado')->where('IdTesis',$id)->get();

         //Obteniendo el archivo solicitado
         $file = File($nombrePDF[0]->ResolucionJurado);
         $archivo=file_get_contents($nombrePDF[0]->ResolucionJurado);

          //Codificando en base64
         $arc=base64_encode($archivo);
         return response()->json($arc, 200);
    }

//Este metodo es usado para obtener el archivo PDF de ActaDictamen, se requiere IdTesis
    public function obtenerArchivoDictamen(Request $request,$id){
        //Obtengo el nombre del achivo (ruta)
         $nombrePDF = Tesis::distinct()->select('ActaDictamen')->where('IdTesis',$id)->get();

         //Obteniendo el archivo solicitado
         $file = File($nombrePDF[0]->ActaDictamen);
         $archivo=file_get_contents($nombrePDF[0]->ActaDictamen);

          //Codificando en base64
         $arc=base64_encode($archivo);
         return response()->json($arc, 200);
    }

    public function sendEmailFechaGraduacion($Correo,$NombreTesista,$FechaGraduacion){
        Mail::to($Correo)->send(new MessageFechaGraduacion($NombreTesista, $FechaGraduacion));
    }


//Este metodo es usado para actualizar datos correspondientes a una Tesis
    public function update(Request $request, $id)
    {
        /*
        if (isset($request->FechaGraduacion)) {
            return response()->json(['mensaje'=>'Si hay fecha de graduacion']);
        }else{
            return response()->json(['mensaje'=>'No hay fecha de graduacion']);
        }
*/


        //Obteniendo el dictamen actual de la tesis
        $Tesis = Tesis::find($id);
        $IdDictamen = $Tesis->IdDictamen;

        //obtengo el estado actual de la tesis
        $EstadoActual = $Tesis->IdEstado;

        //Actualizando campos de la tesis que son textos
        $Tesis->TituloTesis = $request->TituloTesis;



        if (isset($request->Resumen))
            $Tesis->Resumen = $request->Resumen;
        if (isset($request->PalabrasClave))
            $Tesis->PalabrasClave = $request->PalabrasClave;

        $Tesis->FechaFinAsesoria = null;
        $Tesis->FechaFinRevision = null;

        if (isset($request->FechaFinAsesoria))
            $Tesis->FechaFinAsesoria = $request->FechaFinAsesoria;
        if (isset($request->FechaFinRevision))
            $Tesis->FechaFinRevision = $request->FechaFinRevision;




        $Tesis->IdEstudianteEscuela = $request->IdEstudianteEscuela;
        $Tesis->IdTipoTesis = $request->IdTipoTesis;
        $Tesis->IdEstado = $request->IdEstado;
        $Tesis->IdDictamen = $request->IdDictamen;
       // $Tesis->FechaGraduacion = $request->FechaGraduacion;
        $arrayIdsJurados = $request->idsJurados;

        $correoTesista = EstudianteEscuela::select('Estudiante.Correo','Estudiante.Nombre')->join('Estudiante','Estudiante.id','=','EstudianteEscuela.IdEstudiante')->where('EstudianteEscuela.IdEstudianteEscuela',$Tesis->IdEstudianteEscuela)->get();
            $Correo =$correoTesista[0]->Correo;
            $NombreTesista = $correoTesista[0]->Nombre;

            //verifica Fecha de graduacion
        if (isset($request->FechaGraduacion)) {
            //Si existe fecha de graduacion
            $Tesis->FechaGraduacion=$request->FechaGraduacion;
            $this->sendEmailFechaGraduacion($Correo, $NombreTesista,$request->FechaGraduacion);
        }else{
            //Si no existe fecha de graduacion no hace nada
            //return response()->json(['Correo'=>'asd']);
        }

        //Obteniendo fecha actual
        date_default_timezone_set("America/Lima");
        $fecha = new DateTime();





        //Verifico si el IdDictamen actual de la tesis es diferente al ingresado
        if(strcmp($request->IdDictamen, $IdDictamen) === 0){
            //sson iguales no se envia mensaje
        }else{
            //Obteniendo Correo del Tesista
            $Tesis->FechaDictamen = $fecha;


            //Obteniendo el nombre del dictamen actualizado
            $dictamenTesis = Dictamen::select('Dictamen')->where('IdDictamen',$Tesis->IdDictamen)->get();
            $Dictamen =$dictamenTesis[0]->Dictamen;

            //Enviando Correo al Tesista sobre el cambio de dictamen
            $this->sendEmailDictamen($Correo, $Tesis->TituloTesis,$Dictamen);
        }

        //Obteniendo fecha actual
        date_default_timezone_set("America/Lima");
        $fecha = new DateTime();

        //Actualianzo la Fecha de Actualizacion
        $Tesis->FechaActualizacion = $fecha;

        //Obteniendo datos del tesista
        $Escuela = EstudianteEscuela::distinct()->select('Escuela.Directorio','Estudiante.DNI','Grado.Grado','Estudiante.Nombre')->join('Escuela','Escuela.IdEscuela','=','EstudianteEscuela.IdEscuela')->join('Grado','Grado.IdGrado','=','EstudianteEscuela.IdGrado')->where('EstudianteEscuela.IdEstudianteEscuela',$request->IdEstudianteEscuela)->join('Estudiante','Estudiante.id','=','EstudianteEscuela.IdEstudiante')->get();


        //Archivo resolucion de Asesor
        if ($request->hasFile('fileAsesor')) {
            $file = $request->File('fileAsesor');
            $name = $file->getClientOriginalName();

            //Moviendo el archivo a la carpeta publica
            $file->move(public_path().'/'.$Escuela[0]->Directorio.'/'.$Escuela[0]->DNI.'/Nombramiento_del_Asesor/',$Escuela[0]->DNI.'_'.$Escuela[0]->Grado.'_resolucionAsesor.pdf');

            //Actualizando el campo de ResolucionAsesor en la tesis
            $Tesis->ResolucionAsesor=$Escuela[0]->Directorio.'/'.$Escuela[0]->DNI.'/Nombramiento_del_Asesor/'.$Escuela[0]->DNI.'_'.$Escuela[0]->Grado.'_resolucionAsesor.pdf';
        }

        //Archivo resolucion de Jurado
        if ($request->hasFile('fileJurado')) {
            $file = $request->File('fileJurado');
            $name = $file->getClientOriginalName();

            //Moviendo el archivo a la carpeta publica
            $file->move(public_path().'/'.$Escuela[0]->Directorio.'/'.$Escuela[0]->DNI.'/Nombramiento_de_Jurado/',$Escuela[0]->DNI.'_'.$Escuela[0]->Grado.'_resolucionJurado.pdf');

            //Actualizando el campo de ResolucionJurado en la tesis
            $Tesis->ResolucionJurado=$Escuela[0]->Directorio.'/'.$Escuela[0]->DNI.'/Nombramiento_de_Jurado/'.$Escuela[0]->DNI.'_'.$Escuela[0]->Grado.'_resolucionJurado.pdf';
        }

        //Archivo Tesis
        if ($request->hasFile('fileTesis')) {
            $Tamano=$request->Tamano;
            $file = $request->File('fileTesis');
            $name = $file->getClientOriginalName();

            //Moviendo el archivo a la carpeta publica
            $file->move(public_path().'/'.$Escuela[0]->Directorio.'/'.$Escuela[0]->DNI.'/Documento_de_Tesis/',$Escuela[0]->DNI.'_'.$Escuela[0]->Grado.'_tesis.pdf');

            //Actualizando el campo de Tesis en la tesis
            $Tesis->RutaArchivo=$Escuela[0]->Directorio.'/'.$Escuela[0]->DNI.'/Documento_de_Tesis/'.$Escuela[0]->DNI.'_'.$Escuela[0]->Grado.'_tesis.pdf';

            //Guardando en el registro de ArchivoHistorico
            $request->replace(['IdTesis' =>$id,'NombreArchivo' => $Escuela[0]->DNI.'_'.$Escuela[0]->Grado.'_tesis.pdf', 'Tamano' => $Tamano ]);
            $resultado = app('App\Http\Controllers\HistoricoArchivoController')->store($request);
        }

        //Archivo Dictamen
        if ($request->hasFile('fileDictamen')) {
            $file = $request->File('fileDictamen');
            $name = $file->getClientOriginalName();

            //Moviendo el archivo a la carpeta publica
            $file->move(public_path().'/'.$Escuela[0]->Directorio.'/'.$Escuela[0]->DNI.'/Documento_de_Dictamen/',$Escuela[0]->DNI.'_'.$Escuela[0]->Grado.'_dictamen.pdf');

            //Actualizando el campo de ActaDictamen en la tesis
            $Tesis->ActaDictamen=$Escuela[0]->Directorio.'/'.$Escuela[0]->DNI.'/Documento_de_Dictamen/'.$Escuela[0]->DNI.'_'.$Escuela[0]->Grado.'_dictamen.pdf';
        }




        //Guardando Tesis
        $Tesis->save();

        //Obteniendo el nuevo estado de la Tesis
        $EstadoNuevo = $Tesis->IdEstado;

        //Consulta de Jurados existen en la tesis
        $juradosTesis = Jurado::select('IdEstudiante')->where('IdTesis',$Tesis->IdTesis)->get();

        //$arrayIdsJurados => IdEstudiante de todos los jurados en un array
        for ($i=0; $i < count($arrayIdsJurados); $i++) {
            if ($arrayIdsJurados[$i]!=null) {

                //Guardando el Jurado
                $request->replace(['IdTesis' => $id,'IdEstudiante' => $arrayIdsJurados[$i],'IdTipoJurado' => $i+1]);
                $resultado = app('App\Http\Controllers\JuradoController')->store($request);

                //Obteniendo el Correo del nuevo Jurado
                $nuevoJurado = Estudiante::select('Correo')->where('id',$arrayIdsJurados[$i])->get();

                //Verifico si tiene jurados existe la tesis
                if(isset($juradosTesis[$i]->IdEstudiante)){
                    //Verifico si existe el jurado ingresado en la tesis
                    if(strcmp($arrayIdsJurados[$i],$juradosTesis[$i]->IdEstudiante) === 0){
                        //son iguales
                    }else{
                        //Envio de Correo al Jurado
                        $this->sendEmailJuradosTesista($nuevoJurado[0]->Correo,$Escuela[0]->Nombre,$Tesis->ResolucionJurado);
                    }
                }else{
                    //Envio de Correo al Jurado
                    $this->sendEmailJuradosTesista($nuevoJurado[0]->Correo,$Escuela[0]->Nombre,$Tesis->ResolucionJurado);
                }
            }
        }

        //verificamos que la edicion se haya cambiado el estado, de hacerlo verificamos si hay que enviar algun correo
        if(strcmp($EstadoActual,$EstadoNuevo) === 0){
            //en caso de ser iguales no se hace nada y continua con el metodo
        }else{
            //si son distintos quiere decir que hubo un cambio de estado en la tesis
            $this->CambioEstadoTesis($Tesis->IdTesis);
        }
        return $this->show($id);
    }

//Este metodo es usado para actualizar el archivo de Tesis e un estudiante, se requiere IDTesis
    public function updateFile(Request $request, $id)
    {

        $Tamano = $request->Tamano;
        $Tesis = Tesis::find($id);

        if ($request->hasFile('file')) {
            //extraer id escuela

        // return response()->json(['ENTRO FILE'=>'SE']);
            $Escuela = EstudianteEscuela::distinct()->select('Escuela.Directorio','Estudiante.DNI','Grado.Grado')->join('Escuela','Escuela.IdEscuela','=','EstudianteEscuela.IdEscuela')->join('Grado','Grado.IdGrado','=','EstudianteEscuela.IdGrado')->where('EstudianteEscuela.IdEstudianteEscuela',$Tesis->IdEstudianteEscuela)->join('Estudiante','Estudiante.id','=','EstudianteEscuela.IdEstudiante')->get();

            $file = $request->File('file');
            $name = $file->getClientOriginalName();
            $file->move(public_path().'/'.$Escuela[0]->Directorio.'/'.$Escuela[0]->DNI.'/Tesis/',$Escuela[0]->DNI.'_'.$Escuela[0]->Grado.'_tesis.pdf');
            //$Tesis->update(['RutaArchivo' =>$Escuela[0]->Directorio.'/'.$Escuela[0]->DNI.'/Tesis/',$name]);
            $Tesis->RutaArchivo=$Escuela[0]->Directorio.'/'.$Escuela[0]->DNI.'/Tesis/'.$Escuela[0]->DNI.'_'.$Escuela[0]->Grado.'_tesis.pdf';
           // $Tesis->RutaArchivo=$Escuela[0]->Directorio.'/'.$name;
            date_default_timezone_set("America/Lima");
            $fecha = new DateTime();
            $Tesis->FechaActualizacion = $fecha;
        }
        $Tesis->save();
        $correoTesista = EstudianteEscuela::select('Estudiante.Correo')->join('Estudiante','Estudiante.id','=','EstudianteEscuela.IdEstudiante')->where('EstudianteEscuela.IdEstudianteEscuela',$Tesis->IdEstudianteEscuela)->get();

        $correo =$correoTesista[0]->Correo;

        $request->replace(['IdTesis' =>$id,'NombreArchivo' => $Escuela[0]->DNI.'_'.$Escuela[0]->Grado.'_tesis.pdf', 'Tamano' => $Tamano ]);
        //return response()->json($request, 200);
        $resultado = app('App\Http\Controllers\HistoricoArchivoController')->store($request);


         $this->sendEmailUploadTesis($correo);
        return $this->show($id);
    }



//Este metodo es usado para mostrar datos una tesis de la base de datos, se requiere de IdTesis
    public function show($id)
    {
        $Tesis = Tesis::find($id);
        return response()->json($Tesis,200);
    }

    public function verTesis($id){
        $Tesis = Tesis::select('Tesis.TituloTesis','Tesis.Resumen','Tesis.PalabrasClave','Tesis.FechaFinAsesoria','Tesis.FechaFinRevision','TipoTesis.IdTipoTesis','TipoTesis.TipoTesis','Estado.IdEstado','Estado.Estado','Dictamen.IdDictamen','Dictamen.Dictamen','Tesis.FechaRegistro','Tesis.FechaActualizacion','Tesis.FechaFinAsesoria','Tesis.FechaFinRevision','Tesis.FechaDictamen','Tesis.FechaGraduacion','Tesis.ResolucionAsesor','Tesis.ResolucionJurado','Tesis.RutaArchivo','Tesis.ActaDictamen')->join('TipoTesis','TipoTesis.IdTipoTesis','=','Tesis.IdTipoTesis')->join('Dictamen','Dictamen.IdDictamen','=','Tesis.IdDictamen')->join('Estado','Estado.IdEstado','=','Tesis.IdEstado')->where('Tesis.IdTesis',$id)->get();

        $Jurados = Jurado::select('Estudiante.id','Jurado.IdJurado','Estudiante.Nombre','Jurado.IdTipoJurado','TipoJurado.TipoJurado')->join('Estudiante','Estudiante.id','=','Jurado.IdEstudiante')->join('TipoJurado','TipoJurado.IdTipoJurado','=','Jurado.IdTipoJurado')->where('Jurado.IdTesis',$id)->get();
       // return response()->json($Tesis[0]->ResolucionAsesor, 200);


        if ($Tesis[0]->ResolucionAsesor) {
            $file = File($Tesis[0]->ResolucionAsesor);
            $archivo=file_get_contents($Tesis[0]->ResolucionAsesor);
            $arcAsesor=base64_encode($archivo);
            $valores=explode("/", $Tesis[0]->ResolucionAsesor);
            $Tesis[0]->ResolucionAsesor = $valores[count($valores)-1];
        }else{
            $arcAsesor=null;
        }

        if ($Tesis[0]->ResolucionJurado) {
            $file = File($Tesis[0]->ResolucionJurado);
            $archivo=file_get_contents($Tesis[0]->ResolucionJurado);
            $arcJurado=base64_encode($archivo);
            $valores=explode("/", $Tesis[0]->ResolucionJurado);
            $Tesis[0]->ResolucionJurado = $valores[count($valores)-1];
        }else{
            $arcJurado=null;
        }

        if ($Tesis[0]->RutaArchivo) {
            $file = File($Tesis[0]->RutaArchivo);
            $archivo=file_get_contents($Tesis[0]->RutaArchivo);
            $arcTesis=base64_encode($archivo);
            $valores=explode("/", $Tesis[0]->RutaArchivo);
            $Tesis[0]->RutaArchivo = $valores[count($valores)-1];
        }else{
            $arcTesis=null;
        }

        if ($Tesis[0]->ActaDictamen) {
            $file = File($Tesis[0]->ActaDictamen);
            $archivo=file_get_contents($Tesis[0]->ActaDictamen);
            $actTesis=base64_encode($archivo);
            $valores=explode("/", $Tesis[0]->ActaDictamen);
            $Tesis[0]->ActaDictamen = $valores[count($valores)-1];
        }else{
            $actTesis=null;
        }
        return response()->json(['Tesis' => $Tesis,'Jurados' => $Jurados, 'ArchivoBase64_ResolucionAsesor'=>$arcAsesor,'ArchivoBase64_ResolucionJurado'=>$arcJurado,'ArchivoBase64_Tesis'=>$arcTesis,'ArchivoBase64_Dictamen'=>$actTesis]);


    }

    //funcion de prueba
    public function pruebaestado(Request $request){
        $id = $request->id;
        $tesis = Tesis::find($id);
        $tesis->IdEstado = 4;
        $tesis->save();

        $this->CambioEstadoTesis($id);
        return response()->json($tesis);
    }

    //verificar el estado de tesis, para enviar mensajes correspondientes, enviar id de la tesis
    public function CambioEstadoTesis($id){
        $tesis = Tesis::find($id);      //Buscamos la tesis segun su id
        //Si su estado ha sido cambiado a 'En revision' realiza el envio de correos.
        if(strcmp($tesis->IdEstado, "4") === 0){
            //Obtenemos los jurados de la tesis
            $Jurados = Jurado::select('Jurado.IdJurado','Jurado.IdTipoJurado','Estudiante.Correo')->join('Estudiante','Estudiante.id','=','Jurado.IdEstudiante')->where('Jurado.IdTesis','=',$id)->get();
            //Obtenemos los datos del estudiante dueno de la tesis
            $Estudiante = Estudiante::select('Estudiante.id','Estudiante.Correo','Estudiante.Nombre')->join('EstudianteEscuela','Estudiante.id','=','EstudianteEscuela.IdEstudiante')->join('Tesis','Tesis.IdEstudianteEscuela','=','EstudianteEscuela.IdEstudianteEscuela')->where('Tesis.IdTesis','=',$id)->get();
            //Enviamos el correo al estudiante
            $this->sendEmail($Estudiante[0]->Correo, $Estudiante[0]->Nombre);
            //Enviamos el correo a cada uno de los jurados
            foreach ($Jurados as $JuradoIndividual) {
                $this->sendEmail($JuradoIndividual->Correo, $Estudiante[0]->Nombre);
            }
        }
    }

    public function sendEmail($Correo, $Estudiante){
        Mail::to($Correo)->send(new MessageEstadoRevision($Estudiante));
    }

    public function sendEmailDictamen($Correo, $TituloTesis,$Dictamen){
        Mail::to($Correo)->send(new MessageDictamen($TituloTesis,$Dictamen));
    }

    public function subirBanner(Request $request){
        $path = Storage::disk('public')->put('Banner', $request->file('file'));
        return $path;
    }


}
