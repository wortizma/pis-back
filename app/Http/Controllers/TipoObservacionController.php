<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TipoObservacion;
class TipoObservacionController extends Controller
{
	//Este metodo es usado para listar todas las tipos de observaciones de la base de datos
      public function index(){
        $TipoObservacion = TipoObservacion::select('IdTipoObservacion','TipoObservacion')->get();
        return response()->json($TipoObservacion, 200);
    }


}