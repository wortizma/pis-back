<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Imports\ExcelImport;
use App\Estado;
use Excel;
class EstadoController extends Controller
{
    //Este metodo es usado para listar todos los estados de una tesis
    public function index(){
    	$Estado = Estado::select('IdEstado','Estado','Modificable')->get();
        return response()->json($Estado, 200);
    }


    //Este metodo es usado para guardar datos correspondiente de un estado de documenta en la base de datos
    public function store(Request $request){

        request()->validate([
            'Estado' => 'required|min:0|max:45|regex:/^[a-zA-ZáéíóúüñÁÉÍÓÚÜÑ.,;:[:space:]]*$/',

          ],[
            'Estado.required' => 'El campo tiene caracteres no validos.'
          ]);
        if($this->duplicidadRegistro($request->Estado)){
            return response()->json(['Mensaje' => 'Los datos ingresados ya existen en el sistema.']);
         }
    	$Estado = new Estado;
        $Estado->Estado = $request->Estado;

  		$Estado->save();
        return response()->json(['Mensaje' => 'Se ha registrado el Estado de Tesis de manera correcta.']);
  		//return response()->json($Estado, 200);

    }

    //Este metodo es usado para  eliminar un estado de tesis, se requiere de IdEstado
	public function destroy($id){
    	$Estado = Estado::find($id);
        $Estado->delete();

        return response()->json(['Mensaje' => 'Se eliminó de manera correcta.']);
    }

    //Este metodo es usado para  actualizar daots correspondientes a un estado de tesis, se requiere del IdEstado
    public function update(Request $request, $id)
    {
         request()->validate([
            'Estado' => 'required|min:0|max:45|regex:/^[a-zA-ZáéíóúüñÁÉÍÓÚÜÑ.,;:[:space:]]*$/',

          ],[
            'Estado.required' => 'El campo tiene caracteres no validos.'
          ]);
         if($this->duplicidad($request->Estado,$id)){
            return response()->json(['Mensaje' => 'Los datos ingresados ya existen en el sistema.']);
         }
    	$Estado = Estado::find($id);
        $Estado->Estado = $request->Estado;
        $Estado->Modificable = $request->Modificable;
        $Estado->save();
         return response()->json(['Mensaje' => 'Actualizacion existosa.']);
    	//return $this->show($id);
    }

    //Este metodo es usado para mostrar un estado de tesis, se requiere el IdEstado
    public function show($id)
    {

        $Estado = Estado::select('IdEstado','Estado','Modificable')->find($id);
    	return response()->json($Estado,200);

    }

    //Este metodo es usado para  realizar una busqueda por filtrado de un estado de tesis
       public function getEstadoPerName(Request $request){

        if ($request->input('Estado')) {
            $campoBusqueda = 'Estado';
            $datoEnviado = $request->input('Estado');
        }else if ($request->input('Modificable')) {
            $campoBusqueda = 'Modificable';
            $datoEnviado = $request->input('Modificable');
        }else
            return response()->json(['message' => 'Debe Llena el Campo']);

        $Estado = Estado::where($campoBusqueda,'like','%'.$datoEnviado.'%')->select('IdEstado','Estado','Modificable')->get();
                return response()->json($Estado,200);
    }



    //Esta funcion se encarga de verificar la duplicidad para un registro nuevo
    public function duplicidadRegistro($dato){
        return Estado::where('Estado', $dato)->exists();;
    }

    //Este metodo es usado para verificar que el dato no exista en la BD en un registro nuevo
    public function duplicidad($Estado,$id){
        return Estado::where('Estado', $Estado)->where('IdEstado','!=',$id)->exists();;
    }

    //Funcion que verifica que la cadena tenga datos validos
    public function verificarCadena($dato){
        if (preg_match("/^[a-zA-Z0-9áéíóúüñÁÉÍÓÚÜÑ \-_]{5,40}+$/", $dato)) {
            return true;
        } else {
            return false;
           }

    }

    //Este metodo es usado para  cargar un excel con los datos correspondiente a un estado
    public function subirExcelEstado(Request $request){
        $import = new ExcelImport();
        $data = Excel::toArray($import, $request->file('file'));
        $end = $data[0];
        $NoGuardados = "";
        $DatosInvalidos = "";
        $MensajeFinal="";
        $contador = 2;
        $contadorNoGuardado = 0;
        foreach ($end as $Estado) {
            if($Estado['estado'] !== '' &&  $Estado['estado'] !== NULL){
                if($this->duplicidadRegistro($Estado['estado'])){
                    $NoGuardados = $NoGuardados."El registro: ".$contador." ya existe en la base de datos.\n";
                    $contadorNoGuardado++;
                    $contador++;
                    continue;
                }else{
                    if(!$this->verificarCadena($Estado['estado'])){
                        $DatosInvalidos = $DatosInvalidos."El registro: ".$contador." tiene caracteres no válidos.\n";
                        $contador++;
                        continue;
                    }
                    $request->replace(['Estado' => $Estado['estado']]);
                    $this->store($request);
                    $contador++;
                }
            }
        }
        if($contadorNoGuardado == 0){
                $MensajeFinal = $MensajeFinal."Todos los registros se han guardado con éxito!\n";
            }else{
                $MensajeFinal = $MensajeFinal."Los siguientes registros no se han guardado:\n".$NoGuardados.$DatosInvalidos;
            }
      return response()->json(['Mensaje' => $MensajeFinal]);
    }


}
