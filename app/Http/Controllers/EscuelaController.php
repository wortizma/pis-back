<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Escuela;

use Excel;
use App\Imports\EscuelaImport;
class EscuelaController extends Controller
{
    //Este metodo es usado para listar todas las escuelas existentes en la base de datos
     public function index(){
        $Escuela = Escuela::select('Escuela.idEscuela','Escuela.IdFacultad','Escuela.Escuela','Escuela.Directorio','Facultad.Facultad')->join('Facultad','Facultad.IdFacultad','=','Escuela.IdFacultad')->get();
        return response()->json($Escuela, 200);
    }

    //Este metodo es usado para  guardar todos los datos correspondientes a la tabla escuela en la base de datos
    public function store(Request $request){

        request()->validate([
            'Escuela' => 'required|min:0|max:45|regex:/^[a-zA-ZáéíóúüñÁÉÍÓÚÜÑ.,;:[:space:]]*$/',
            //'IdFacultad' => 'required|number'
          ],[
            'Escuela.required' => 'El campo tiene caracteres no validos.'
          ]);
        if($this->duplicidadRegistro($request->IdFacultad, $request->Escuela)){
            return response()->json(['Mensaje' => 'Los datos ingresados ya existen en el sistema.']);
        }
        $Escuela = new Escuela;
        $Escuela->IdFacultad = $request->IdFacultad;
        $Escuela->Escuela = $request->Escuela;
        $Escuela->Directorio ="";
        $Escuela->save();
        $Escuela->Directorio="Directorio".$Escuela->idEscuela;
        $Escuela->save();

        //return response()->json(['Mensaje' => $Escuela]);

       return response()->json(['Mensaje' => 'Se ha registrado la escuela de manera correcta.']);
    }

    //Este metodo es usado para  eliminar una escuela en la base de datos, se requiere de IdEscuela
    public function destroy($id){
        $Escuela = Escuela::find($id);
        $Escuela->delete();
        return response()->json(['Mensaje' => 'Se eliminó de manera correcta.']);
    }

    //Este metodo es usado para  actualizar datos correspondiente a una escuerla, se requiere de IdEscuela
    public function update(Request $request, $id)
    {
       request()->validate([
            'Escuela' => 'required|min:0|max:45|regex:/^[a-zA-ZáéíóúüñÁÉÍÓÚÜÑ.,;:[:space:]]*$/',
           //'IdFacultad' => 'required|number'
          ],[
            'Escuela.required' => 'El campo tiene caracteres no validos.'
          ]);
       if($this->duplicidad($request->IdFacultad, $request->Escuela,$id)){
            return response()->json(['Mensaje' => 'Los datos ingresados ya existen en el sistema.']);
        }
        $Escuela = Escuela::find($id);
        $Escuela->IdFacultad = $request->IdFacultad;
        $Escuela->Escuela = $request->Escuela;
        $Escuela->Directorio = $Escuela->Directorio;
        $Escuela->save();
        return response()->json(['Mensaje' => "Actualizacion exitosa."]);
    }

    //Este metodo es usado para mostrar datos correspodientes de una escuela, se requiere de IdEscuela
    public function show($id)
    {
        $Escuela = Escuela::select('Escuela.idEscuela','Escuela.IdFacultad','Facultad.Facultad','Escuela.Escuela','Escuela.Directorio')->join('Facultad','Facultad.IdFacultad','=','Escuela.IdFacultad')->where('Escuela.idEscuela','=',$id)->get();
        return  response()->json(['idEscuela' => $Escuela[0]->idEscuela, 'IdFacultad' => $Escuela[0]->IdFacultad, 'Facultad' =>$Escuela[0]->Facultad, 'Escuela' =>$Escuela[0]->Escuela]);
    }
    //Este metodo es usado para verificar que el dato no exista en la BD
    public function duplicidadRegistro($Facultad, $Escuela){
        return Escuela::where('IdFacultad', $Facultad)->where('Escuela', $Escuela)->exists();;
    }
    //Este metodo es usado para verificar que el dato no exista en la BD en un registro nuevo
    public function duplicidad($Facultad, $Escuela,$id){
        return Escuela::where('IdFacultad', $Facultad)->where('Escuela', $Escuela)->where('IdEscuela','!=',$id)->exists();;
    }
    //Este metodo es usado para una busqueda filtrada de una escuela en la base de datos
     public function getEscuelaPerName(Request $request){

        if ($request->input('IdFacultad')) {
            $campoBusqueda = 'IdFacultad';
            $datoEnviado = $request->input('IdFacultad');
        }else if ($request->input('Escuela')) {
            $campoBusqueda = 'Escuela';
            $datoEnviado = $request->input('Escuela');
        }else if ($request->input('Directorio')) {
            $campoBusqueda = 'Directorio';
            $datoEnviado = $request->input('Directorio');
        }else
            return response()->json(['Mensaje' => 'Debe Llena el Campo']);

        $Escuela = Escuela::where($campoBusqueda,'like','%'.$datoEnviado.'%')->select('idEscuela','IdFacultad','Escuela','Directorio')->get();
                return response()->json($Escuela,200);
    }







    //Funcion que verifica que la cadena tenga datos validos
    public function verificarCadena($dato){
        if (preg_match("/^[a-zA-Z0-9áéíóúüñÁÉÍÓÚÜÑ .:;,()]{3,99}+$/", $dato)) {
            return true;
        } else {
            return false;
           }

    }
        //Este metodo es usado para  cargar un excel con los datos correspondiente a una facultad
    public function ExcelEscuela(Request $request){
        $import = new EscuelaImport();
        $data = Excel::toArray($import, $request->file('file'));
        $end = $data[0];
        $NoGuardados = "";
        $DatosInvalidos = "";
        $MensajeFinal="";
        $contador = 2;
        $contadorNoGuardado = 0;
        foreach ($end as $Escuela) {
            //return response()->json(['message' => $Escuela]);
            if($Escuela['escuela'] !== '' &&  $Escuela['escuela'] !== NULL){

                if($this->duplicidadRegistro($Escuela['idfacultad'],$Escuela['escuela'])){
                    $NoGuardados = $NoGuardados."El registro: ".$contador." ya existe en la base de datos.\n";
                    $contadorNoGuardado++;
                    $contador++;
                    continue;
                }else{
                    if(!$this->verificarCadena($Escuela['escuela'])){
                        $DatosInvalidos = $DatosInvalidos."El registro: ".$contador." tiene caracteres no validos.\n";
                        $contador++;
                        continue;
                    }
                    $request->replace(['Escuela' => $Escuela['escuela'], 'IdFacultad'=> $Escuela['idfacultad']]);
                    $this->store($request);
                    $contador++;
                }
            }
        }
        if($contadorNoGuardado == 0){
                $MensajeFinal = $MensajeFinal."Todos los registros se han guardado con exito\n";
            }else{
                $MensajeFinal = $MensajeFinal."Los siguientes registros no se han guardado:\n".$NoGuardados.$DatosInvalidos;
            }
      return response()->json(['Mensaje' => $MensajeFinal]);
    }

}
