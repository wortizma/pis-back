<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Estudiante;
use App\Tesis;
class ReporteController extends Controller
{
    //Funcion encargada de hacer el reporte.
    public function GenerarReporte(Request $request){
        DB::statement(DB::raw('SET @rownum = 0'));

    	/*$verificador = Estudiante::select(DB::raw('@rownum:=@rownum+1 as id'),'Estudiante.Nombre', 'Estudiante.DNI', 'Facultad.Facultad','Escuela.Escuela', 'Grado.Grado', 'Tesis.FechaRegistro','Tesis.FechaActualizacion', 'Tesis.TituloTesis','Tesis.FechaFinAsesoria', 'Tesis.FechaFinRevision', 'Tesis.FechaGraduacion', 'Estado.Estado','TipoTesis.TipoTesis')->join('EstudianteEscuela','EstudianteEscuela.IdEstudiante','=','Estudiante.id')->join('Escuela','Escuela.idEscuela','=','EstudianteEscuela.IdEscuela')->join('Facultad','Facultad.IdFacultad','=','Escuela.IdFacultad')->join('Grado','Grado.IdGrado','=','EstudianteEscuela.IdGrado')->join('Tesis','Tesis.IdEstudianteEscuela','=','EstudianteEscuela.IdEstudianteEscuela')->join('Estado','Estado.IdEstado','=','Tesis.IdEstado')->join('TipoTesis','Tesis.IdTipoTesis','=','TipoTesis.IdTipoTesis');*/

        $verificador = Tesis::select(DB::raw('@rownum:=@rownum+1 as id'),'Tesis.IdTesis','Estudiante.Nombre', 'Estudiante.DNI', 'Facultad.Facultad','Escuela.Escuela', 'Grado.Grado', 'Tesis.FechaRegistro','Tesis.FechaActualizacion', 'Tesis.TituloTesis','Tesis.FechaFinAsesoria', 'Tesis.FechaFinRevision', 'Tesis.FechaGraduacion', 'Estado.Estado','TipoTesis.TipoTesis')->join('EstudianteEscuela','EstudianteEscuela.IdEstudianteEscuela','=','Tesis.IdEstudianteEscuela')->join('Escuela','Escuela.idEscuela','=','EstudianteEscuela.IdEscuela')->join('Facultad','Facultad.IdFacultad','=','Escuela.IdFacultad')->join('Grado','Grado.IdGrado','=','EstudianteEscuela.IdGrado')->join('Estudiante','Estudiante.id','=','EstudianteEscuela.IdEstudiante')->join('Estado','Estado.IdEstado','=','Tesis.IdEstado')->join('TipoTesis','Tesis.IdTipoTesis','=','TipoTesis.IdTipoTesis')->with('jurados');

        //En caso de que el request contenga un nombre de asesor, este se anida a la consulta.
        if($request->Asesor){
            $dato = $request->Asesor;

            $verificador = $verificador->whereHas('jurados', function($q) use ($dato)  {$q->join('Estudiante','Estudiante.id','=','Jurado.IdEstudiante')->where('IdTipoJurado',1)->where('Estudiante.Nombre','like','%'.$dato.'%');});
        }
        //En caso de que el request contenga un nombre de un jurado sin distincion, este se anida a la consulta.
        if($request->Jurado){
            $dato = $request->Jurado;
            $verificador = $verificador->whereHas('jurados', function($q) use ($dato)  {$q->join('Estudiante','Estudiante.id','=','Jurado.IdEstudiante')->where('Estudiante.Nombre','like','%'.$dato.'%');});
        }
        //En caso de que el request contenga un nombre, este se anida a la consulta.
    	if($request->Nombre){
    		$verificador = $verificador->where('Estudiante.Nombre','like','%'.$request->Nombre.'%');
    	}
        //En caso de que el request contenga un DNI, este se anida a la consulta.
    	if($request->DNI){
    		$verificador = $verificador->where('Estudiante.DNI','like','%'.$request->DNI.'%');
    	}
        //En caso de que el request contenga un Tipo de tesis, este se anida a la consulta.
    	if($request->IdTipoTesis){
    		$verificador = $verificador->where('TipoTesis.IdTipoTesis','=',$request->IdTipoTesis);
    	}
        //En caso de que el request contenga una facultad, este se anida a la consulta.
    	if($request->IdFacultad){
    		$verificador = $verificador->where('Facultad.IdFacultad','=',$request->IdFacultad);
    	}
        //En caso de que el request contenga una escuela, este se anida a la consulta.
    	if($request->idEscuela){
    		$verificador = $verificador->where('Escuela.idEscuela','=',$request->idEscuela);
    	}
        //En caso de que el request contenga un grado, este se anida a la consulta.
    	if($request->IdGrado){
    		$verificador = $verificador->where('Grado.IdGrado','=',$request->IdGrado);
    	}
        //En caso de que el request contenga una fecha de inicio, este se anida a la consulta.
    	if($request->FechaDesde){
    		$verificador = $verificador->where('Tesis.FechaRegistro','>=',$request->FechaDesde);
    	}
        //En caso de que el request contenga una fecha de fin, este se anida a la consulta.
    	if($request->FechaFin){
    		$verificador = $verificador->where('Tesis.FechaRegistro','<=',$request->FechaFin);
    	}
        //En caso de que el request contenga una fecha de actualizacion, este se anida a la consulta.
        if($request->FechaInicioActualizacion){
            $verificador = $verificador->where('Tesis.FechaActualizacion','>=',$request->FechaInicioActualizacion);
        }
        //En caso de que el request contenga una fecha de actualizacion, este se anida a la consulta.
        if($request->FechaFinActualizacion){
            $verificador = $verificador->where('Tesis.FechaActualizacion','<=',$request->FechaFinActualizacion);
        }
        //En caso de que el request contenga una fecha de asesoria, este se anida a la consulta.
        if($request->FechaInicioAsesoria){
            $verificador = $verificador->where('Tesis.FechaFinAsesoria','>=',$request->FechaInicioAsesoria);
        }
        //En caso de que el request contenga una fecha de asesoria, este se anida a la consulta.
        if($request->FechaFinAsesoria){
            $verificador = $verificador->where('Tesis.FechaFinAsesoria','<=',$request->FechaFinAsesoria);
        }
        //En caso de que el request contenga una fecha de graduacion, este se anida a la consulta.
        if($request->FechaInicioGraduacion){
            $verificador = $verificador->where('Tesis.FechaGraduacion','>=',$request->FechaInicioGraduacion);
        }
        //En caso de que el request contenga una fecha de graduacion, este se anida a la consulta.
        if($request->FechaFinGraduacion){
            $verificador = $verificador->where('Tesis.FechaGraduacion','<=',$request->FechaFinGraduacion);
        }

        //Se agrega el orden de los resultados
    	$verificador = $verificador->orderBy('Estudiante.Nombre','DESC');
    	$verificador = $verificador->orderBy('Grado.Grado','DESC');
    	$verificador = $verificador->orderBy('Tesis.FechaRegistro','DESC');
    	$verificador = $verificador->get();
        //Se envia los datos al front.
    	return response()->json($verificador);
    }
}
