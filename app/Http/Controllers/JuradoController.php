<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jurado;
use App\Observacion;
use App\Estudiante;
use App\Tesis;
use App\EstudianteEscuela;
use DateTime;
use Illuminate\Support\Facades\Mail;
use App\Mail\MessageEstadoEsperaSustentacion;

use App\Mail\MessageToJuradosTesista;
use App\Mail\MessageEstadoEsperaJurado;
class JuradoController extends Controller
{
    //Este metodo es usado para listar todos los jurados de la base de datos
    public function index(){
    	$Jurado = Jurado::select('IdJurado','IdTesis','IdEstudiante','IdTipoJurado')->get();
        return response()->json($Jurado, 200);
    }

    public function sendEmailJuradosTesista($Correo,$NombreEstudiante,$file){
        Mail::to($Correo)->send(new MessageToJuradosTesista($NombreEstudiante,$file));
    }

    //Este metodo es usado para guardar datos correspondientes a un jurado en la base de datos
    public function store(Request $request){

        if(Jurado::where('IdTesis', $request->IdTesis)->where("IdTipoJurado","=",$request->IdTipoJurado)->exists()){
            $Jurado = Jurado::select('IdJurado','IdTesis','IdEstudiante','IdTipoJurado')->where('IdTesis', $request->IdTesis)->where("IdTipoJurado",$request->IdTipoJurado)->get();
            $JuradoC = Jurado::find($Jurado[0]->IdJurado);
            $JuradoC->IdEstudiante = $request->IdEstudiante;
            $JuradoC->save();
            return $JuradoC;

        }else{
            $JuradoN = new Jurado();
            $JuradoN->IdTesis = $request->IdTesis;
            $JuradoN->IdEstudiante = $request->IdEstudiante;
            $JuradoN->IdTipoJurado = $request->IdTipoJurado;
            $JuradoN->save();
            return $JuradoN;
        }

    }

    //Este metodo es usado para marcar el fin de observaciones del jurado
    public function FinObservaciones(Request $request){
        //Primero se verifica que la tesis este asignada al jurado (por seguridad).
        $ObtenerJurado = Jurado::select('IdJurado')->where([['IdEstudiante',$request->id],['IdTesis',$request->IdTesis]])->get();
        $EsAsesor = 0;
        //Si no esta  acargo de esta tesis, se informa al front.
        if(count($ObtenerJurado) == 0){
            return response()->json(["Mensaje" => "No tiene ningun cargo en esta tesis."]);
        }
        //Si el jurado tiene observaciones que el aun no aprobó, no puede finalizar este proceso.
        $Seleccion = Observacion::select("IdObservacion")->where([['IdJurado',$ObtenerJurado[0]->IdJurado],['Aprobado','0']])->get();
        if(count($Seleccion) > 0){
            return response()->json(["Mensaje" => "No se puede finalizar el proceso de observacion, aun quedan observaciones que usted no aprobó."]);
        }else{
            //Cambiamos el estado del jurado, poniendo 1 en el campo 'FinObservaciones'
            $ActualizarJurado = Jurado::find($ObtenerJurado[0]->IdJurado);
            $ActualizarJurado->FinObservaciones = 1;
            date_default_timezone_set("America/Lima");
            $fecha = new DateTime();
            //Si le pertenece a un asesor, entonces guardamos la variable
            if(strcmp($ActualizarJurado->IdTipoJurado,"1") === 0){
                $EsAsesor = 1;
            }
            $ActualizarJurado->FechaFin = $fecha;
            $ActualizarJurado->save();
            //Realizamos el cambio de estado de la tesis, verificando que sea un asesor o jurado el que finaliza la observacion.
            $this->CambiarEstadoCorreo($request->IdTesis,$fecha,$EsAsesor);
            return response()->json(["Mensaje" => "Se ha culminado su revision de forma exitosa!"]);
        }
        //aca estara lo de verificar pa enviar correo

    }
    //Funcion que realiza el cambio de estado y envio de correos.
    public function CambiarEstadoCorreo($idTesis, $fecha,$EsAsesor){
        //verifica que todos los jurados de la tesis hayan culminado sus observaciones, de no hacerlo finaliza la observacion.
        $Verificar = Jurado::select('IdJurado','FinObservaciones')->where([['IdTesis',$idTesis],['FinObservaciones','0']])->get();
        if(count($Verificar) > 0){
            return;
        }else{
            $tesis = Tesis::find($idTesis);
            //verificamos, si es un asesor el que finalizo la observacion y ademas la tesis se encuentra en "En asesoría", cambia el estado a 'Esperando designación de jurado'.
            if(strcmp($EsAsesor, "1") === 0){
                if(strcmp($tesis->IdEstado, "2") === 0){
                    $tesis->IdEstado = 3;
                    $tesis->FechaFinAsesoria = $fecha;
                    $tesis->save();
                    $this->EnviarCorreos($idTesis, $EsAsesor);
                }else{
                    return;
                }
            }
            //Si es otro tipo de jurado y ademas la tesis se encuentra en 'En revisión', cambia el estado a 'Esperando sustentación'
            if(strcmp($tesis->IdEstado, "4") === 0){
                $tesis->IdEstado = 5;
                $tesis->FechaFinRevision = $fecha;
                $tesis->save();
                $this->EnviarCorreos($idTesis, $EsAsesor);
            }else{
                return;
            }
        }
    }
    //Funcion encargada de enviar correos.
    public function EnviarCorreos($id, $EsAsesor){
        //Obtener operadores
        $Escuela = EstudianteEscuela::select('EstudianteEscuela.IdEscuela')->join('Tesis','Tesis.IdEstudianteEscuela','EstudianteEscuela.IdEstudianteEscuela')->where('Tesis.IdTesis','=',$id)->get();
        $Operadores = Estudiante::select('Estudiante.id','Estudiante.Correo','Estudiante.Nombre')->join('EstudianteEscuela','EstudianteEscuela.IdEstudiante','=','Estudiante.id')->where([['EstudianteEscuela.IdEscuela',$Escuela[0]->IdEscuela],['Estudiante.Operador','1']])->get();
        //Obtenemos los correos y nombres de los jurados y estudiantes.
        $Jurados = Jurado::select('Jurado.IdJurado','Jurado.IdTipoJurado','Estudiante.Correo')->join('Estudiante','Estudiante.id','=','Jurado.IdEstudiante')->where([['Jurado.IdTipoJurado', '1'],['Jurado.IdTesis', $id]])->get();
        $Estudiante = Estudiante::select('Estudiante.id','Estudiante.Correo','Estudiante.Nombre')->join('EstudianteEscuela','Estudiante.id','=','EstudianteEscuela.IdEstudiante')->join('Tesis','Tesis.IdEstudianteEscuela','=','EstudianteEscuela.IdEstudianteEscuela')->where('Tesis.IdTesis','=',$id)->get();
        //En caso de ser asesor, ejecutamos 'sendEmailAsesor'.
        if(strcmp($EsAsesor, "1") === 0){
            $this->sendEmailAsesor($Jurados[0]->Correo, $Estudiante[0]->Nombre);
            $this->sendEmailAsesor($Estudiante[0]->Correo, $Estudiante[0]->Nombre);
            //en ambos casos, enviamos correo a los operadores
            foreach ($Operadores as $operador) {
                $this->sendEmailAsesor($operador->Correo, $Estudiante[0]->Nombre);
            }
        }else{
            //En caso de ser otro tipo de jurado, ejecutamos 'sendEmail'.
            $JuradosOtros = Jurado::select('Jurado.IdJurado','Jurado.IdTipoJurado','Estudiante.Correo')->join('Estudiante','Estudiante.id','=','Jurado.IdEstudiante')->where([['Jurado.IdTesis', $id]])->get();
            foreach ($JuradosOtros as $jurado) {
                $this->sendEmail($jurado->Correo, $Estudiante[0]->Nombre);
            }
            $this->sendEmail($Estudiante[0]->Correo, $Estudiante[0]->Nombre);
            //en ambos casos, enviamos correo a los operadores
            foreach ($Operadores as $operador) {
                $this->sendEmail($operador->Correo, $Estudiante[0]->Nombre);
            }
        }

    }
    //Envia correo en caso de que la tesis este en 'En asesoría'.
    public function sendEmail($Correo, $Estudiante){
        Mail::to($Correo)->send(new MessageEstadoEsperaSustentacion($Estudiante));
    }
    //Envia correo en caso de que la tesis este en 'En revisión'.
    public function sendEmailAsesor($Correo, $Estudiante){
        Mail::to($Correo)->send(new MessageEstadoEsperaJurado($Estudiante));
    }
}
