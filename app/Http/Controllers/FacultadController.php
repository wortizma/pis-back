<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Facultad;
use App\Escuela;
use Excel;
use App\Imports\ExcelImport;
class FacultadController extends Controller
{
    //Este metodo es usado para listar todas las facultades de la base de datos
      public function index(){
        $Facultad = Facultad::select('IdFacultad','Facultad')->get();
        return response()->json($Facultad, 200);
    }

//Este metodo es usado para guardatos datos correspondientes a una facultad
    public function store(Request $request){
        request()->validate([
            'Facultad' => 'required|min:3|max:99|regex:/^[a-zA-ZáéíóúüñÁÉÍÓÚÜÑ.,;:[:space:]]*$/',

          ],[
            'Facultad.required' => 'El campo tiene caracteres no validos.'
          ]);
        if($this->duplicidadRegistro($request->Facultad)){
            return response()->json(['Mensaje' => "Los datos ingresados ya existen en el sistema"]);
        }

        $Facultad = new Facultad;
        $Facultad->Facultad = $request->Facultad;
        $Facultad->save();
        return response()->json(['Mensaje' => "Se ha registrado la facultad de manera correcta"]);
    }

//Este metodo es usado para  eliminar una facultad de la base de datos, se requiere IdFacultad
    public function destroy($id){
        $Facultad = Facultad::find($id);
        $Facultad->delete();
        return response()->json(['Eliminacion' => true]);
    }

//Este metodo es usado para actualizar datos correspondiente a una facultad, se requiere IdFacultad
    public function update(Request $request, $id)
    {
        request()->validate([
            'Facultad' => 'required|min:3|max:99|regex:/^[a-zA-ZáéíóúüñÁÉÍÓÚÜÑ.,;:[:space:]]*$/',
        ],[
            'Facultad.required' => 'El campo tiene caracteres no validos.'
          ]);

        if($this->duplicidad($request->Facultad, $id)){
            return response()->json(['Mensaje' => "Los datos ingresados ya existen en el sistema"]);
        }

        $Facultad = Facultad::find($id);
        $Facultad->Facultad = $request->Facultad;
        $Facultad->save();
        return response()->json(['Mensaje' => "Actualizacion exitosa."]);
    }

//Este metodo es usado para  mostrar una facultad de la base de datos, se requiere del IdFacultad
    public function show($id)
    {

        $Facultad = Facultad::select('IdFacultad','Facultad')->find($id);
        return response()->json($Facultad,200);

    }
//Este metodo losta las escuelas que existe en una facultad
    public function ListarEscuelaPorFacultad($id){
        $Escuelas = Escuela::where('IdFacultad','=',$id)->get();
        return response()->json($Escuelas);
    }


//Este metodo es usado para realizar una busqueda por filtrado de una facultad en la base de datos
     public function getFacultadPerName(Request $request){

        if ($request->input('Facultad')) {
            $campoBusqueda = 'Facultad';
            $datoEnviado = $request->input('Facultad');
        }else
            return response()->json(['message' => 'Debe Llena el Campo']);

        $Facultad = Facultad::where($campoBusqueda,'like','%'.$datoEnviado.'%')->select('IdFacultad','Facultad')->get();
                return response()->json($Facultad,200);
    }
    //Esta funcion se encarga de ver que los datos ingresados no existan en la BD.
    public function duplicidad($dato,$id){
        return Facultad::where('Facultad', $dato)->where('IdFacultad','!=',$id)->exists();;
    }
    //Esta funcion se encarga de verificar la duplicidad para un registro nuevo
    public function duplicidadRegistro($dato){
        return Facultad::where('Facultad', $dato)->exists();;
    }
    //Funcion que verifica que la cadena tenga datos validos
    public function verificarCadena($dato){
        if (preg_match("/^[a-zA-ZáéíóúüñÁÉÍÓÚÜÑ .,;:()-]{3,99}+$/", $dato)) {
            return true;
        } else {
            return false;
           }

    }

    //Este metodo es usado para  cargar un excel con los datos correspondiente a una facultad
    public function excelFacultad(Request $request){
        $import = new ExcelImport();
        $data = Excel::toArray($import, $request->file('file'));
        $end = $data[0];
        $NoGuardados = "";
        $DatosInvalidos = "";
        $MensajeFinal="";
        $contador = 2;
        $contadorNoGuardado = 0;
        foreach ($end as $Facultad) {
            if($Facultad['facultad'] !== '' &&  $Facultad['facultad'] !== NULL){
                if($this->duplicidadRegistro($Facultad['facultad'])){
                    $NoGuardados = $NoGuardados."El registro: ".$contador." ya existe en la base de datos.\n";
                    $contadorNoGuardado++;
                    $contador++;
                    continue;
                }else{
                    if(!$this->verificarCadena($Facultad['facultad'])){
                        $DatosInvalidos = $DatosInvalidos."El registro: ".$contador." tiene caracteres no validos.\n";
                        $contador++;
                        continue;
                    }
                    $request->replace(['Facultad' => $Facultad['facultad']]);
                    $this->store($request);
                    $contador++;
                }
            }
        }
        if($contadorNoGuardado == 0){
                $MensajeFinal = $MensajeFinal."Todos los registros se han guardado con exito\n";
            }else{
                $MensajeFinal = $MensajeFinal."Los siguientes registros no se han guardado:\n".$NoGuardados.$DatosInvalidos;
            }
      return response()->json(['Mensaje' => $MensajeFinal]);
    }
}
