<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
class PlantillaController extends Controller
{
    public function getPlantillaDocente(){
    	$file = File('Plantillas/Plantilla_DOCENTE.xlsx');
        $archivo=file_get_contents('Plantillas/Plantilla_DOCENTE.xlsx');
        $arc=base64_encode($archivo);
        return response()->json($arc, 200);
    }

    public function getPlantillaEstudiante(){
    	$file = File('Plantillas/Plantilla_ESTUDIANTE.xlsx');
        $archivo=file_get_contents('Plantillas/Plantilla_ESTUDIANTE.xlsx');
        $arc=base64_encode($archivo);
        return response()->json($arc, 200);
    }
    public function getPlantillaTipoJurado(){
        $file = File('Plantillas/Plantilla_TIPO_JURADO.xlsx');
        $archivo=file_get_contents('Plantillas/Plantilla_TIPO_JURADO.xlsx');
        $arc=base64_encode($archivo);
        return response()->json($arc, 200);
    }
    public function getPlantillaDictamen(){
        $file = File('Plantillas/Plantilla_DICTAMEN.xlsx');
        $archivo=file_get_contents('Plantillas/Plantilla_DICTAMEN.xlsx');
        $arc=base64_encode($archivo);
        return response()->json($arc, 200);
    }
    public function getPlantillaTipoDocumento(){
        $file = File('Plantillas/Plantilla_TIPO_DOCUMENTO.xlsx');
        $archivo=file_get_contents('Plantillas/Plantilla_TIPO_DOCUMENTO.xlsx');
        $arc=base64_encode($archivo);
        return response()->json($arc, 200);
    }
    public function getPlantillaFacultad(){
        $file = File('Plantillas/Plantilla_FACULTAD.xlsx');
        $archivo=file_get_contents('Plantillas/Plantilla_FACULTAD.xlsx');
        $arc=base64_encode($archivo);
        return response()->json($arc, 200);
    }

}