<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RolActividad;
use App\RolProceso;
use App\ProcesoActividad;

class RolActividadController extends Controller
{
    //
    //Este metodo es usado para listar datos de la tabla RolActividad, se requiere de IdRolProceso
	public function show($idRolProceso){
		$data = RolActividad::select('RolActividad.IdRolActividad','ProcesoActividad.Nombre','RolActividad.Permiso')->join('ProcesoActividad','ProcesoActividad.IdProcesoActividad' ,'=','RolActividad.IdProcesoActividad')->where('RolActividad.IdRolProceso','=',$idRolProceso)->get();
        return response()->json($data,200);
	}

//Este metodo es usado para actualizar datos correspondiente a la tabla de RolActividad, se requiere de IdRolActividad
	public function update(Request $request, $id){
    	request()->validate([
            'Permiso' => 'required|max:1',
          ]);
    	$RolActividad = RolActividad::find($id);
        $RolActividad->Permiso = $request->Permiso;
        $RolActividad->save();
        return response()->json(['Actualizacion de permiso' => true]);
    }


}