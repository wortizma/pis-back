<?php

namespace App\Http\Controllers;
use Excel;
use Illuminate\Http\Request;
use App\Tipodocumento;
use App\Imports\ExcelImport;
class TipodocumentoController extends Controller
{
    //Este metodo es usado para listar todos los Tipodocumento de la base de datos
    public function index(){
    	$Documentos = Tipodocumento::select('IdDNITipo','TipoDNI')->get();
        return response()->json($Documentos, 200);
    }

    //Este metodo es usado para guardart datos correspondiente a la tabla Tipodocumento en la base de datos
    public function store(Request $request){
    	request()->validate([
            'TipoDNI' => 'required|min:2|max:45|regex:/^[a-zA-ZáéíóúüñÁÉÍÓÚÜÑ.[:space:]]*$/',

        ],[
            'TipoDNI.required' => 'El campo tiene caracteres no validos.'
          ]);

        if($this->duplicidadRegistro($request->TipoDNI)){
            return response()->json(['Mensaje' => "El registro ya existe en el sistema"]);
        }
    	$Tipodocumento = new Tipodocumento;
        $Tipodocumento->TipoDNI = $request->TipoDNI;
    	$Tipodocumento->save();

    	return response()->json(['Mensaje' => "Se ha registrado el tipo de documento de manera correcta"]);
    }

//Este metodo es usado para mostrar un tipodocumento, se requiere de IdTipodocumento
    public function show($id)
    {

        $Tipodocumento = Tipodocumento::select('IdDNITipo','TipoDNI')->find($id);
    	return response()->json($Tipodocumento,200);

    }
    //Este metodo es usado para actualizar datos correspondientes el tipo de documento, se requiere id del tipo de documento
    public function update(Request $request, $id)
    {
        request()->validate([
            'TipoDNI' => 'required|min:2|max:45|regex:/^[a-zA-Z0-9áéíóúüñÁÉÍÓÚÜÑ[:space:]]*$/',
        ],[
            'TipoDNI.required' => 'El campo tiene caracteres no validos.'
          ]);
        if($this->duplicidad($request->TipoDNI, $id)){
            return response()->json(['Mensaje' => "Los datos ingresados ya existen en el sistema"]);
        }
        $Tipodocumento = Tipodocumento::find($id);
        $Tipodocumento->TipoDNI = $request->TipoDNI;
        $Tipodocumento->save();
        return response()->json(['Mensaje' => "Se ha actualizado el tipo de documento de manera correcta"]);
    }

//Este metodo es usado para eliminar un tipodocumento de la base de datos, se requiere de Idtipodocumento
    public function destroy($id){
    	$Tipodocumento = Tipodocumento::find($id);
        $Tipodocumento->delete();

        return response()->json(['Eliminacion' => true]);
    }
    //Funcion encargada de verificar que no haya duplicidad en la base de datos
    public function duplicidad($dato, $id){
        return Tipodocumento::where('TipoDNI', $dato)->where('IdDNITipo','!=',$id)->exists();
    }
    //Funcion encargada de verificar que no haya duplicidad en la base de datos
    public function duplicidadRegistro($dato){
        return Tipodocumento::where('TipoDNI', $dato)->exists();
    }

    //Funcion que verifica que la cadena tenga datos validos
    public function verificarCadena($dato){
        if (preg_match("/^[a-zA-Z0-9áéíóúüñÁÉÍÓÚÜÑ \-_]{2,45}+$/", $dato)) {
            return true;
        } else {
            return false;
           }

    }
    //Funcion encargada de registrar un excel de los tipos de documento.
    public function excelTipoDocumento(Request $request){
        $import = new ExcelImport();
        $data = Excel::toArray($import, $request->file('file'));
        $end = $data[0];
        $NoGuardados = "";
        $MensajeFinal="";
        $DatosInvalidos = "";
        $contador = 2;
        $contadorNoGuardado = 0;
        foreach ($end as $Documento) {
            if($Documento['tipodni'] !== '' &&  $Documento['tipodni'] !== NULL){
                if($this->duplicidadRegistro($Documento['tipodni'])){
                    $NoGuardados = $NoGuardados."El registro: ".$contador." ya existe en la base de datos.\n";
                    $contadorNoGuardado++;
                    $contador++;
                    continue;
                }else{
                    if(!$this->verificarCadena($Documento['tipodni'])){
                        $DatosInvalidos = $DatosInvalidos."El registro: ".$contador." tiene caracteres no validos.\n";
                        $contador++;
                        continue;
                    }
                    $request->replace(['TipoDNI' => $Documento['tipodni']]);
                    $this->store($request);
                    $contador++;
                }
            }
        }
        if($contadorNoGuardado == 0){
                $MensajeFinal = $MensajeFinal."Todos los registros se han guardado con exito\n";
            }else{
                $MensajeFinal = $MensajeFinal."Los siguientes registros no se han guardado:\n".$NoGuardados.$DatosInvalidos;
            }
      return response()->json(['Mensaje' => $MensajeFinal]);
    }

}
