<?php
namespace App\Http\Controllers;
use App\Exports\DataExportEstudiante;
use App\Exports\DataExportDocente;
use App\Exports\DataExportEstado;
use App\Exports\DataExportTipoTesis;
use App\Exports\DataExportGrado;
use App\Exports\DataExportEscuela;

use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;

class DataExportController extends Controller {
	public function exportExcelEstudiante(){
		//return Excel::download(New DataExport,'data.xlsx');
		$dataExport = new DataExportEstudiante;
		$dataExport->store('Plantilla_ESTUDIANTE_POSGRADO.xlsx','public');
		$archivo=file_get_contents('../storage/app/public/Plantilla_ESTUDIANTE_POSGRADO.xlsx');
		$arc=base64_encode($archivo);
		return response()->json($arc, 200);
	}

	public function exportExcelDocente(){
		//return Excel::download(New DataExport,'data.xlsx');
		$dataExport = new DataExportDocente;
		$dataExport->store('Plantilla_DOCENTE_POSGRADO.xlsx','public');
		$archivo=file_get_contents('../storage/app/public/Plantilla_DOCENTE_POSGRADO.xlsx');
		$arc=base64_encode($archivo);
		return response()->json($arc, 200);
	}
	public function exportExcelEstado(){
		//return Excel::download(New DataExport,'data.xlsx');
		$dataExport = new DataExportEstado;
		$dataExport->store('Plantilla_ESTADO_POSGRADO.xlsx','public');
		$archivo=file_get_contents('../storage/app/public/Plantilla_ESTADO_POSGRADO.xlsx');
		$arc=base64_encode($archivo);
		return response()->json($arc, 200);
	}

	public function exportExcelTipoTesis(){
		//return Excel::download(New DataExport,'data.xlsx');
		$dataExport = new DataExportTipoTesis;
		$dataExport->store('Plantilla_TIPOTESIS_POSGRADO.xlsx','public');
		$archivo=file_get_contents('../storage/app/public/Plantilla_TIPOTESIS_POSGRADO.xlsx');
		$arc=base64_encode($archivo);
		return response()->json($arc, 200);
	}

	public function exportExcelGrado(){
		//return Excel::download(New DataExport,'data.xlsx');
		$dataExport = new DataExportGrado;
		$dataExport->store('Plantilla_GRADO_POSGRADO.xlsx','public');
		$archivo=file_get_contents('../storage/app/public/Plantilla_GRADO_POSGRADO.xlsx');
		$arc=base64_encode($archivo);
		return response()->json($arc, 200);
	}
	public function exportExcelEscuela(){
		//return Excel::download(New DataExport,'data.xlsx');
		$dataExport = new DataExportEscuela;
		$dataExport->store('Plantilla_ESCUELA_POSGRADO.xlsx','public');
		$archivo=file_get_contents('../storage/app/public/Plantilla_ESCUELA_POSGRADO.xlsx');
		$arc=base64_encode($archivo);
		return response()->json($arc, 200);
	}


}