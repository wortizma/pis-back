<?php
namespace App\Http\Traits;
use Exception;
use Illuminate\Http\Request;
use App\User;
use App\EstudianteEscuela;
trait CrearEstudianteEscuela{
	public function StoreEstudianteEscuela(Request $request){
        $id = $request->IdEstudiante;
		$atributos = User::find($id);

        if(strcmp($atributos->Operador, "1") === 0 && strcmp($atributos->Estudiante, "1") === 0){


            if($request->input('IdGrado')){
                if($request->IdGrado !== NULL && $request->IdGrado !== ''){
                    if(EstudianteEscuela::where('IdEscuela', $request->IdEscuela)->where([["IdEstudiante", $request->IdEstudiante],["IdGrado",  $request->IdGrado]])->exists()){
                        return "Este registro ya existe";
                    }
                    $EstudianteEscuela = new EstudianteEscuela;
                    $EstudianteEscuela->IdEscuela = $request->IdEscuela;
                    $EstudianteEscuela->IdEstudiante = $request->IdEstudiante;
                    $EstudianteEscuela->IdGrado = $request->IdGrado;
                    $EstudianteEscuela->Promocion = $request->Promocion;
                    $EstudianteEscuela->save();
                    return "Registro Guardado.";
                }

            }
            if(EstudianteEscuela::where('IdEscuela', $request->IdEscuela)->where([["IdEstudiante", $request->IdEstudiante]])->exists()){
                return "Este registro ya existe";
                }

            $EstudianteEscuela = new EstudianteEscuela;
            $EstudianteEscuela->IdEscuela = $request->IdEscuela;
            $EstudianteEscuela->IdEstudiante = $request->IdEstudiante;
            $EstudianteEscuela->save();
            return "Registro Guardado.";

        }

        if(strcmp($atributos->Estudiante, "1") === 0){
             if(EstudianteEscuela::where('IdEscuela', $request->IdEscuela)->where([["IdEstudiante", $request->IdEstudiante],["IdGrado",  $request->IdGrado]])->exists()){
                return "Este registro ya existe";
                }
            $EstudianteEscuela = new EstudianteEscuela;
            $EstudianteEscuela->IdEscuela = $request->IdEscuela;
            $EstudianteEscuela->IdEstudiante = $request->IdEstudiante;
            $EstudianteEscuela->IdGrado = $request->IdGrado;
            $EstudianteEscuela->Promocion = $request->Promocion;
            $EstudianteEscuela->save();
            return "Registro Guardado.";
        }

        if(strcmp($atributos->Operador, "1") === 0){
             if(EstudianteEscuela::where('IdEscuela', $request->IdEscuela)->where([["IdEstudiante", $request->IdEstudiante]])->exists()){
                return "Este registro ya existe";
                }
            $EstudianteEscuela = new EstudianteEscuela;
            $EstudianteEscuela->IdEscuela = $request->IdEscuela;
            $EstudianteEscuela->IdEstudiante = $request->IdEstudiante;
            $EstudianteEscuela->save();
            return "Registro Guardado.";
        }
		}

	}