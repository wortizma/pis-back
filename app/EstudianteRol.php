<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstudianteRol extends Model
{
	// Datos correspondiente a la tabla EstudianteRol
    protected $table="EstudianteRol";
    protected $primarykey = "IdEstudianteRol";
    protected $fillable = array('IdEstudiante','IdRol');
    public $timestamps = false;
}
