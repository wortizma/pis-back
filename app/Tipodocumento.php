<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tipodocumento extends Model
{
	// Datos correspondiente a la tabla Tipodocumetn
    protected $table = "DNITipo";
    protected $primaryKey = "IdDNITipo";
    protected $fillable = array('TipoDNI');
    public $timestamps = false;
}
