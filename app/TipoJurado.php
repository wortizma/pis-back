<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoJurado extends Model
{
    // Datos correspondiente a la tabla TipoJurado
    protected $table = "TipoJurado";
    protected $primaryKey = "IdTipoJurado";
    protected $fillable = array('TipoJurado');
    public $timestamps = false;
}
