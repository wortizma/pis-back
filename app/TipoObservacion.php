<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoObservacion extends Model
{
	// Datos correspondiente a la tabla Observacion
    protected $table = "TipoObservacion";
    protected $primaryKey = "IdTipoObservacion";
    protected $fillable = array('TipoObservacion');
    public $timestamps = false;
}
