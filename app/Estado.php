<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estado extends Model
{
    // Datos correspondiente a la tabla Estado
    protected $table = "Estado";
    protected $primaryKey = "IdEstado";
    protected $fillable = array('Estado','Modificable');
    public $timestamps = false;
}
