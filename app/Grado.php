<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grado extends Model
{
    protected $table = "Grado";
    protected $primaryKey = "IdGrado";
    protected $fillable = array('IdEscuela','Grado');
    public $timestamps = false;
}
