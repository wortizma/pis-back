<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;

use Maatwebsite\Excel\Concerns\WithTitle;

use App\Escuela;
class EscuelaVDataExportSheet implements FromCollection, WithTitle
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        //
         //return Estudiante::all();
         $collection = collect([
    ['Escuela' => "Escuela", 'IdFacultad' => 'IdFacultad','Facultad'=>'Facultad']
]);
         return $collection;
    }

    public function title():string{
    	return 'ESCUELAS';
    }
}
