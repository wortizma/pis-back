<?php

namespace App\Exports;

use App\Tipodocumento;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class DataExportEscuela implements WithMultipleSheets
{
	use Exportable;
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Tipodocumento::all();
    }

    public function sheets(): array{
    	$sheets = [];
    		$sheets[]= new EscuelaVDataExportSheet();
    		$sheets[]= new FacultadDataExportSheet();


    //*	var_dump($shees)
    	return $sheets;
    }



}
