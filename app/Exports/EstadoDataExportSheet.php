<?php

namespace App\Exports;


use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\FromCollection;

use Maatwebsite\Excel\Concerns\WithTitle;

use App\Estado;
class EstadoDataExportSheet implements FromCollection, WithTitle
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        //
       // $Estado = Estado::select('Estado')->get();

      $collection = collect([
    ['Estado' => "Estado"]
]);
      //  $concatenar = $collection->concat($Estado);

         return $collection;
         //return $Escuela[0];
    }

    public function title():string{
        return 'Estado';
    }
}
