<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;

use Maatwebsite\Excel\Concerns\WithTitle;

use App\Estudiante;
class DocenteDataExportSheet implements FromCollection, WithTitle
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        //
         //return Estudiante::all();
         $collection = collect([
    ['Nombre' => "NOMBRE", 'IdDNITipo' => 'IDTIPODOCUMENTO','DNI'=>'DNI','Correo'=>'CORREO','Telefono'=>'TELEFONO','Direccion'=>'DIRECCION']
]);
         return $collection;
    }

    public function title():string{
    	return 'Docente';
    }
}
