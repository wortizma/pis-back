<?php

namespace App\Exports;


use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\FromCollection;

use Maatwebsite\Excel\Concerns\WithTitle;

use App\Facultad;
class FacultadDataExportSheet implements FromCollection, WithTitle
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        //
        $Facultad = Facultad::select('IdFacultad','Facultad')->get();

      $collection = collect([
    ['IdFacultad' => "IdFacultad", 'Facultad' => 'Facultad']
]);
		$concatenar = $collection->concat($Facultad);

         return $concatenar;
         //return $Escuela[0];
    }

    public function title():string{
    	return 'FACULTAD';
    }
}
