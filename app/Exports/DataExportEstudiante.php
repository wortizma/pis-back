<?php

namespace App\Exports;

use App\Tipodocumento;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class DataExportEstudiante implements WithMultipleSheets
{
	use Exportable;
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Tipodocumento::all();
    }

    public function sheets(): array{
    	$sheets = [];
    		$sheets[]= new EstudianteDataExportSheet();
    		$sheets[]= new DNITipoDataExportSheet();
    		$sheets[]= new EscuelaDataExportSheet();


    //*	var_dump($shees)
    	return $sheets;
    }



}
