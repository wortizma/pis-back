<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;

use Maatwebsite\Excel\Concerns\WithTitle;

use App\Estudiante;
class GradoDataExportSheet implements FromCollection, WithTitle
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        //
         //return Estudiante::all();
         $collection = collect([
    ['Grado' => "Grado", 'IdEscuela' => 'IdEscuela','Escuela'=>'Escuela']
]);
         return $collection;
    }

    public function title():string{
    	return 'Grado';
    }
}
