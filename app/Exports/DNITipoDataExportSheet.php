<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;

use Maatwebsite\Excel\Concerns\WithTitle;

use App\Tipodocumento;
class DNITipoDataExportSheet implements FromCollection, WithTitle
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        //

         $tipoDocumento = Tipodocumento::all();

         $collection = collect([
    ['IdDNITipo' => "ID", 'TipoDNI' => 'TIPO DOCUMENTO']
]);
		$concatenar = $collection->concat($tipoDocumento);

         return $concatenar;
    }

    public function title():string{
    	return 'TIPOS DE DOCUMENTO';
    }


}
