<?php

namespace App\Exports;


use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\FromCollection;

use Maatwebsite\Excel\Concerns\WithTitle;

use App\TipoTesis;
class TipoTesisDataExportSheet implements FromCollection, WithTitle
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        //
        //$TipoTesis = TipoTesis::select('TipoTesis')->get();

      $collection = collect([
    ['TipoTesis' => "TipoTesis"]
]);
        //$concatenar = $collection->concat($TipoTesis);

         return $collection;
         //return $Escuela[0];
    }

    public function title():string{
        return 'TipoTesis';
    }
}
