<?php

namespace App\Exports;


use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\FromCollection;

use Maatwebsite\Excel\Concerns\WithTitle;

use App\Escuela;
class EscuelaDataExportSheet implements FromCollection, WithTitle
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        //
        $Escuela = Escuela::select('IdEscuela','Escuela')->get();

      $collection = collect([
    ['IdEscuela' => "ID ESCUELA", 'Escuela' => 'ESCUELA']
]);
		$concatenar = $collection->concat($Escuela);

         return $concatenar;
         //return $Escuela[0];
    }

    public function title():string{
    	return 'ESCUELAS';
    }
}
