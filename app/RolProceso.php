<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RolProceso extends Model
{
	// Datos correspondiente a la tabla RolProceso
    protected $table = "RolProceso";
    protected $primaryKey = "IdRolProceso";
    protected $fillable = array('IdRol','IdProceso');
    public $timestamps = false;
}
