<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proceso extends Model
{
	// Datos correspondiente a la tabla Proceso
    protected $table = "Proceso";
    protected $primaryKey = "IdProceso";
    protected $fillable = array('NombreProceso');
    public $timestamps = false;
}
