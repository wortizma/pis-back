<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistoricoArchivo extends Model
{
    // Datos correspondiente a la tabla Facultad
    protected $table = "HistoricoArchivo";
    protected $primaryKey = "IdHistoricoArchivo";
    protected $fillable = array('IdTesis','NombreArchivo','Fecha','Tamano','Numero');
    public $timestamps = false;
}
