<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class messageDictamen extends Mailable
{
    use Queueable, SerializesModels;
    public $subject = 'Cambio de Dictamen en tesis.';
    public $TituloTesis;
    public $Dictamen;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($TituloTesis,$Dictamen)
    {
        $this->TituloTesis = $TituloTesis;
        $this->Dictamen = $Dictamen;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.messageDictamen');
    }
}