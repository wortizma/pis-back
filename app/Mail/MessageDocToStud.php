<?php

namespace App\Mail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
class MessageDocToStud extends Mailable
{
    use Queueable, SerializesModels;
    public $subject = 'El docente a cargo de tu tesis te ha hecho observaciones';
    public $Encabezados;
    public $Generales;
    public $Especificas;
    public $contadorGeneral = 1;
    public $contadorEspecifico = 1;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($datosEncabezadoCorreo, $Generales, $Especificas)
    {
        $this->Encabezados = $datosEncabezadoCorreo;
        $this->Generales = $Generales;
        $this->Especificas = $Especificas;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.messageMailDocToEst');
    }
}