<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendMailTesistaToJurado extends Mailable
{
    use Queueable, SerializesModels;
    public $subject = 'Correcciones Enviadas por el Tesista';
    public $TituloTesis;
    public $Tesista;
    public $Jurado;
    public $Fecha;
    public $ObservacionesGenerales;
    public $ObservacionesEspecificas=array();



    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($TituloTesis,$Tesista,$Jurado,$Fecha,$ObservacionesGenerales,$ObservacionesEspecificas)
    {
        $this->TituloTesis=$TituloTesis;
        $this->Tesista=$Tesista;
        $this->Jurado=$Jurado;
        $this->Fecha=$Fecha;
        $this->ObservacionesGenerales=$ObservacionesGenerales;
        $this->ObservacionesEspecificas=$ObservacionesEspecificas;
    }



    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.messageTesistaToJurado');
    }
}
