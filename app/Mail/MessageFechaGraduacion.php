<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class messageFechaGraduacion extends Mailable
{
    use Queueable, SerializesModels;
    public $subject = 'ASIGNACION DE FECHA DE GRADUACION';
    public $NombreTesista;
    public $FechaGraduacion;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($NombreTesista,$FechaGraduacion)
    {
        $this->NombreTesista = $NombreTesista;
        $this->FechaGraduacion = $FechaGraduacion;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.messageFechaGraduacion');
    }
}