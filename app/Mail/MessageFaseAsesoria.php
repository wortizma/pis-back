<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MessageFaseAsesoria extends Mailable
{
    use Queueable, SerializesModels;
    public $subject = 'Cambio de estado en tesis.';
    public $NombreEstudiante;
    public $FileResolucionAsesor;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($NombreEstudiante,$FileResolucionAsesor)
    {
        $this->NombreEstudiante = $NombreEstudiante;
        $this->FileResolucionAsesor = $FileResolucionAsesor;


    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
       // $file="Directorio1/44444444/Resolucion Asesor/44444444_Grado Prueba_resolucionAsesor.pdf";

        $email=$this->view('emails.messageAsesoria');
        //$arc = fopen($this->FileResolucionJurado,"rb");
        //$archivo=file_get_contents("Directorio1/44444444/Resolucion Asesor/44444444_Grado Prueba_resolucionAsesor.pdf");

        //$email->attach(public_path($file));
        $email->attach(public_path($this->FileResolucionAsesor));
/*
        $archivosadjuntos= [
    // primer archivo adjunto
    'path/to/file1' => [
        'as' => 'file1.pdf',
        'mime' => 'application/pdf',
    ],

    // Segundo archivo adjunto
    'path/to/file12' => [
        'as' => 'file2.pdf',
        'mime' => 'application/pdf',
    ],

    ...
];
*/

        return $email;
    }
}