<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MessageToOperator extends Mailable
{
    use Queueable, SerializesModels;
    public $subject = 'Cambio de estado - Sistema de la escuela de Postgrado.';
    public $Tesis;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($Tesis)
    {
        $this->Tesis = $Tesis;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.messageToOperator');
    }
}
