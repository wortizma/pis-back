<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MessageEstadoEsperaJurado extends Mailable
{
    use Queueable, SerializesModels;
    public $subject = 'Cambio de estado en la tesis.';
    public $estudiante;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($Estudiante)
    {
        $this->estudiante = $Estudiante;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.messageEsperaJurado');
    }
}