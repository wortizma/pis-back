<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MessageEstadoRevision extends Mailable
{
    use Queueable, SerializesModels;
    public $subject = 'Cambio de estado en tesis.';
    public $estudiante;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($Estudiante)
    {
        $this->estudiante = $Estudiante;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.messageRevision');
    }
}