<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tesis extends Model
{
    // Datos correspondiente a la tabla Tesis
    protected $table = "Tesis";
    protected $primaryKey = "IdTesis";
    protected $fillable = array('IdEstudianteEscuela','IdTipoTesis','IdEstado','IdDictamen','TituloTesis','RutaArchivo');
    public $timestamps = false;

    public function jurados()
    {
        return $this->hasMany('App\Jurado','IdTesis')->join('Estudiante','Estudiante.id','=','Jurado.IdEstudiante')->select('Jurado.*', 'Estudiante.Nombre')->orderBy('IdTipoJurado', 'asc');
    }
    public function jurados2()
    {
        return $this->hasMany('App\Jurado','IdTesis');
    }
}
