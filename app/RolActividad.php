<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RolActividad extends Model
{
	// Datos correspondiente a la tabla RolActividad
    protected $table = "RolActividad";
    protected $primaryKey = "IdRolActividad";
    protected $fillable = array('IdRolProceso','IdProcesoActividad','Permiso');
    public $timestamps = false;
}
