<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jurado extends Model
{
    // Datos correspondiente a la tabla Jurado
    protected $table = "Jurado";
    protected $primaryKey = "IdJurado";
    protected $fillable = array('IdTesis','IdEstudiante','IdTipoJurado','FinObservaciones','FechaFin');
    public $timestamps = false;

}
